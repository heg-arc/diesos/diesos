Development
======================================================================

Documentation
----------------------------------------------------------------------

Main documentation sources:

Django
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- Django 4.1: https://docs.djangoproject.com/en/4.1/
- REST Django Rest Framework (DRF): https://www.django-rest-framework.org/
- Agiliq Books: https://books.agiliq.com/en/latest/README.html
  - Django ORM Cookbook: https://books.agiliq.com/projects/django-orm-cookbook/
  - Django Admin Cookbook: https://books.agiliq.com/projects/django-admin-cookbook/
- Two Scoops of Django: https://gitlab.com/heg-arc/ressources/two-scoops-of-django  

Bootstrap
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- Bootstrap 5.3: https://getbootstrap.com/docs/5.3/getting-started/introduction/
- Bootstrap Icons (1.10.0): https://icons.getbootstrap.com/

HTMX
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- django-htmx: https://django-htmx.readthedocs.io/en/latest/
- htmx: https://htmx.org/


