# DIESOS

Integrated system for recording and tracking objects in the event of a disaster

[![Built with Cookiecutter Django](https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg?logo=cookiecutter)](https://github.com/cookiecutter/cookiecutter-django/)
[![Black code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)
[![Licence: GPLv3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Issues](https://img.shields.io/gitlab/issues/open-raw/heg-arc/diesos/diesos?gitlab_url=https%3A%2F%2Fgitlab.com)](https://gitlab.com/heg-arc/diesos/diesos/-/issues)
[![Release](https://img.shields.io/gitlab/v/release/heg-arc/diesos/diesos?date_order_by=created_at&sort=date)](https://gitlab.com/heg-arc/diesos/diesos/-/releases)

## Getting started

1. Install [Docker](https://docs.docker.com/install/) and [Docker-Compose](https://docs.docker.com/compose/).

     **Windows tip:** You need to [install WSL2](https://learn.microsoft.com/en-us/windows/wsl/install) in order to use Docker on Windows. You also need to put the source code
       in a directory that is accessible from WSL2. For example, you can put it in your home directory ie. `/home/username` (but not on the windows partition `/mnt/c/Users/...`).


2. Clone the repository

       git clone https://gitlab.com/heg-arc/diesos/diesos.git

    You need to [create a SSH key and add it to your Gitlab account](https://docs.gitlab.com/ee/user/ssh.html) in order to be able to push to the repository.


3. Set COMPOSE_FILE environment variable (or add `-f local.yml` to all docker-compose commands)

       export COMPOSE_FILE=local.yml

4. Build your virtual machines with the following shell command (issue this command from the root of the repository ie. `diesos/`).
   This can take a while the first time you run it.

       docker-compose build

5. Download the latest database dump from the development server (https://dev.diesos.ch/pgbackups/)
   You can also start with an empty database by skipping the steps 5 to 8 and [loading the demo data fixtures](https://gitlab.com/heg-arc/diesos/diesos/-/wikis/Demo-Setup).


6. Start your postgresql server with the following shell command

       docker-compose up -d postgres

7. Copy the database dump to the postgresql container

       docker-compose cp backup_xxxx_xx_xxTxx_xx_xx.sql.gz postgres:/backups

8. Restore the database dump in the postgresql container

       docker-compose exec postgres restore backup_xxxx_xx_xxTxx_xx_xx.sql.gz

9. Start your virtual machines with the following shell command

       docker-compose up -d

10. Check everything is fine for example

        docker-compose ps

        docker-compose logs django

11. You can now access the application at the following URLs:

    - DIESOS website: http://localhost:8000
    - DIESOS admin website: http://localhost:8000/admin
    - Mailhog (webmail): http://localhost:8025
    - Flower (celery monitoring): http://localhost:5555
    - Documentation: http://localhost:9000
    - Postgres database: [postgresql://localhost:5432/diesos](postgresql://localhost:5432/lcba)

    You can use the [default credentials](https://gitlab.com/heg-arc/diesos/diesos/-/wikis/Demo-Setup) to log in.

## Useful commands

**Windows/git bash tip:**
      `docker-compose exec django /entrypoint`  works from command.exe or powershell but fails from git bash.
      As git bash tries to convert any absolute linux path to its windows install path ("C:/Program Files/Git/...)
      under git bash use ../entrypoint instead, also prefix docker-compose commands with winpty. For example, to create a new
      admin account, use the following: `winpty docker-compose exec django ../entrypoint python manage.py createsuperuser`

### Create a new admin account

    docker-compose exec django /entrypoint python manage.py createsuperuser

### Load fixtures (default / master data)

    docker-compose exec django /entrypoint python manage.py loaddata artefactType

Available fixtures are located in the `./fixtures` directories of each app.

### Access the Django shell

    docker-compose exec django /entrypoint python manage.py shell

### Generate database model graph from postgres database

    docker-compose exec django /entrypoint python manage.py graph_models -a -g -o diesos.png

You can also restrict the graph to a specific app (for example: `artefacts`)

    docker-compose exec django /entrypoint python manage.py graph_models artefacts treatments utils -g -o diesos_only.png

For more information, see [django-extensions](https://django-extensions.readthedocs.io/en/latest/graph_models.html).

## Migrations

1. Create new migration file based on the model's changes

        docker-compose -f local.yml exec django /entrypoint python manage.py makemigrations

   You can also create files for a specific app only

        docker-compose -f local.yml exec django /entrypoint python manage.py makemigrations artefacts

2. Apply the migrations file  (Apply all changes up to the last migration file)

        docker-compose -f local.yml exec django /entrypoint python manage.py migrate

    You can also apply migrations files for a specific app only

        docker-compose -f local.yml exec django /entrypoint python manage.py migrate artefacts

    It still possible to unapply new changes by specifying a migration file's older version

        docker-compose -f local.yml exec django /entrypoint python manage.py migrate artefacts 0001

3. Show all migration state for each project's app

        docker-compose -f local.yml exec django /entrypoint python manage.py showmigrations

## Tests

1. Run all tests

       docker-compose exec django /entrypoint coverage run -m pytest

   You can also run tests for a specific app only (faster)

       docker-compose exec django /entrypoint coverage run -m pytest diesos/artefacts

2. Generate coverage report

       docker-compose exec django /entrypoint coverage report

   You can also generate a nice HTML report (created in `htmlcov/` directory)

       docker-compose exec django /entrypoint coverage html

   **Windows tip:** You can then access the report from your browser (for example: file://wsl.localhost/Ubuntu/home/xxxxxx/diesos/htmlcov/index.html)

## Style Guide Enforcement

The best way to ensure that your code is compliant with the style guide is to use **[pre-commit](https://pre-commit.com/)** hooks.
_**Failing to comply with the style guide will prevent you from merging your code!**_

1. Install pre-commit

       pip install pre-commit

2. Install pre-commit hooks

       pre-commit install

3. (Optional) Run pre-commit hooks manually

       pre-commit run --all-files

When pre-commit hooks are installed, they will be run automatically before each commit. If the code is not compliant with the style guide, the commit will fail.
Some hooks can also **automatically fix the code** for you (for example, isort will reorder the imports). In this case, you will need to **add the fixed files to the commit and re-commit** (in some cases, multiple times).

## Style Guide Enforcement (alternative)

1. Run style check

       docker-compose exec django flake8

2. More detailed output with pylint

       docker-compose exec django /entrypoint pylint --load-plugins pylint_django /app/diesos

3. If you have issues with the order of the imports, you can use *isort* to reorder them

       docker-compose -f local.yml exec django isort diesos

## Support
The main place of support is the [issue tracker](https://gitlab.com/heg-arc/diesos/diesos/-/issues). You can also reach the support desk at [help@diesos.ch](mailto:help@diesos.ch).

## Roadmap
Check the [current roadmap](https://gitlab.com/heg-arc/diesos/diesos/-/wikis/Roadmap) to follow the development of the project. You can also contribute to the roadmap by creating [issues](https://gitlab.com/heg-arc/diesos/diesos/-/issues) related to expected future functionality.

## Contributing
Always happy to get issues identified and merge requests! See the [contribution guide](https://gitlab.com/heg-arc/diesos/diesos/-/blob/main/CONTRIBUTING.md) for more information.

## Authors and acknowledgment
This project is the result of collaboration between the HE Arc's [conservation-restoration](https://www.he-arc.ch/conservation-restauration/recherche/) and [management information systems](https://www.he-arc.ch/gestion/institut-de-digitalisation-des-organisations-ido/) (IDO) research teams.
The project was partly funded by the HE Arc and the Hasler Foundation.

Full list of contributors: [CONTRIBUTORS.md](https://gitlab.com/heg-arc/diesos/diesos/-/blob/main/CONTRIBUTORS.md)

## License
This project is licensed under the terms of the [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.en.html).
