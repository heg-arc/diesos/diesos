# Contributors

## Academic team

* [Dr Cédric Gaspoz](https://people.hes-so.ch/en/profile/3575238180-cedric-gaspoz) - Full Professor UAS, Haute école de gestion Arc – University of Applied Sciences and Arts Western Switzerland (HES-SO), Switzerland
* [Dr Agnès Gilbert-Miermon](https://people.hes-so.ch/fr/profile/75081673-agnes-gelbert-miermon) - Full Professor UAS, Haute Ecole Arc Conservation-restauration – University of Applied Sciences and Arts Western Switzerland (HES-SO), Switzerland
* [Thierry Jacot](https://people.hes-so.ch/fr/profile/112980295-thierry-jacot) - Senior Lecturer UAS, Haute Ecole Arc Conservation-restauration – University of Applied Sciences and Arts Western Switzerland (HES-SO), Switzerland
* [Dr Ulysse Rosselet](https://people.hes-so.ch/fr/profile/3230977856-ulysse-rosselet) - Assistant Professor UAS, Haute école de gestion Arc – University of Applied Sciences and Arts Western Switzerland (HES-SO), Switzerland

## Developers

Listed in alphabetical order.

| Name            | Gitlab                                       |
|:----------------|:---------------------------------------------|
| Davide Baffa    | [@DBaffa](https://gitlab.com/DBaffa)         |
| Cédric Gaspoz   | [@cgaspoz](https://gitlab.com/cgaspoz)       |
| Robin Rolle     | [@RobinRolle](https://gitlab.com/RobinRolle) |
| Ulysse Rosselet | [@urosselet](https://gitlab.com/urosselet)   |
| Nicolas Rosset  | [@Qtan](https://gitlab.com/Qtan)             |
