from django.urls import path

from diesos.barcodes.views import scan_barcode, search_barcode, set_scanner

app_name = "barcodes"
urlpatterns = [
    path("", view=scan_barcode, name="read"),
    path("search/", view=search_barcode, name="search"),
    path("set/<str:scanner>", view=set_scanner, name="set"),
]
