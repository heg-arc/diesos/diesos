from urllib.parse import urlparse

from django.db.models import Q
from django.urls import Resolver404, resolve

from diesos.artefacts.models import Artefact, ExistingCollection
from diesos.utils.models import UniqueShortId

TYPE_ICONS = {
    "artefact": "bi-egg",
    "box": "bi-box-seam",
    "processingline": "bi-repeat",
    "locationtype": "bi-geo-alt-fill",
}


def resolve_barcode(barcode):
    """
    This function receives a barcode from the scanner or manual input and tries to find corresponding objects in the
    database.

    Most of the time, this should only return a list of one object. In the case of multiple objects with a similar
    barcode, we return a list of objects. If no objects are found, we return None.

    {'object_type': 'artefact', 'icon': 'bi-box-seam', 'name': 'Gold plate', 'url': object.get_url(), 'o}

    :param barcode: a scanned barcode
    :return: None or an object or a list of objects
    """
    # TODO: add the other models

    # We check if the barcode is a Diesos URL
    try:
        path = urlparse(barcode).path
        url = resolve(path)
    except Resolver404:
        url = None
    if url:
        return [{"url": barcode}]

    objects = []
    artefacts = Artefact.objects.filter(
        Q(rescue_number=barcode) | Q(inventory_number=barcode)
    )
    if artefacts:
        for artefact in artefacts:
            if artefact.inventory_number:
                name = f"{artefact.name} ({artefact.inventory_number})"
            else:
                name = artefact.name
            objects.append(
                {
                    "object_type": "artefact",
                    "icon": TYPE_ICONS["artefact"],
                    "code": artefact.rescue_number,
                    "description": artefact.description,
                    "name": name,
                    "url": artefact.get_absolute_url(),
                    "object": artefact,
                }
            )
    shortuuids = UniqueShortId.objects.filter(short_id=barcode)
    if shortuuids:
        for shortuuid in shortuuids:
            objects.append(
                {
                    "object_type": shortuuid.content_type.model,
                    "icon": TYPE_ICONS[shortuuid.content_type.model],
                    "code": shortuuid.short_id,
                    "name": shortuuid.content_object.name,
                    "url": shortuuid.content_object.get_scan_url(),
                    "object": shortuuid.content_object,
                }
            )
    existing_collections = ExistingCollection.objects.filter(
        Q(inventory_number=barcode)
    )
    if existing_collections:
        for existing_collection in existing_collections:
            objects.append(
                {
                    "object_type": "existing_collection",
                    "data": existing_collection,
                    "url": existing_collection.get_absolute_url(),
                }
            )
    return objects
