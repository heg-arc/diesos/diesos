from actstream import action
from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from diesos.barcodes.barcode_resolver import resolve_barcode


def set_scanner(request, scanner):
    # We set the user's preference and redirect to the appropriate page
    if scanner == "reader":
        request.session["scanner"] = "reader"
    else:
        request.session["scanner"] = "mobile"

    if request.GET.get("parent_id") and request.GET.get("option"):
        url = (
            reverse("barcodes:read")
            + f'?parent_id={request.GET.get("parent_id")}&option={request.GET.get("option")}'
        )

        return redirect(url)

    return redirect("barcodes:read")


@permission_required("artefacts.view_artefact")
def scan_barcode(request):
    # We check if the user has already scanned a barcode
    scanner = request.session.get("scanner", False)
    if scanner:
        if scanner == "mobile":
            # We display the mobile scanner page
            return render(request, "barcodes/scanner.html")
        else:
            # We display the desktop scanner page
            return render(request, "barcodes/form.html")
    else:
        request.session["scanner"] = "mobile"
        return render(request, "barcodes/scanner.html")


@permission_required("artefacts.view_artefact")
def search_barcode(request):
    barcode = request.GET.get("barcode")
    parent_id = request.GET.get("parent_id")
    option = request.GET.get("option")
    if barcode:
        # Objects matching scan data
        objects = resolve_barcode(barcode)
        # Objects matching scan data excluding existing collection
        not_existing_collection_objects = [
            obj for obj in objects if obj["object_type"] != "existing_collection"
        ]
        if not objects:
            # This is a new barcode, we can create an object (or it is a scanning error)
            # TODO: add link to buttons as soon as the models are available
            return render(
                request,
                "barcodes/create.html",
                {"barcode": barcode, "parent_id": parent_id, "option": option},
            )
        elif len(objects) == 1 and objects[0]["object_type"] != "existing_collection":
            # This is a single artefact or shortID, we redirect to the object's url
            # TODO: add location (target) to the action
            if "object" in objects[0] and objects[0]["object_type"] == "artefact":
                object_id = objects[0]["object"].id
                return redirect("artefacts:scan", pk=object_id)
            else:
                if "object" in objects[0]:
                    action.send(
                        request.user,
                        verb=_("scanned"),
                        action_object=objects[0]["object"],
                        target=request.processing_line,
                    )
                return redirect(objects[0]["url"])

        elif len(objects) == 1 and objects[0]["object_type"] == "existing_collection":
            # We found only an existingCollection object, we want to create a new artefact from existing info
            messages.success(request, "Existing collection has been found !")
            return render(
                request,
                "barcodes/import.html",
                {"barcode": barcode, "existing_collection": objects[0]["data"]},
            )

        elif len(objects) == 2 and len(not_existing_collection_objects) == 1:
            # We found one object and its existing collection, the user is redirected to the object
            return redirect(not_existing_collection_objects[0]["url"])
        else:
            # We have more than one object, the user have to select the right one
            # We exclude existing_collection objects
            return render(
                request,
                "barcodes/results.html",
                {"barcode": barcode, "objects": not_existing_collection_objects},
            )
    else:
        # We display the form to input a barcode manually or with an external reader
        request.session["scanner"] = "reader"
        return render(request, "barcodes/form.html")
