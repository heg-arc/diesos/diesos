import pytest
from django.contrib.auth.models import AnonymousUser, Permission
from django.core.exceptions import PermissionDenied
from django.test import Client, TestCase
from django.urls import reverse

from diesos.artefacts.tests.factories import ArtefactFactory, ExistingCollectionFactory
from diesos.treatments.tests.factories import ProcessingLineFactory
from diesos.users.tests.factories import UserFactory

pytestmark = pytest.mark.django_db


class TestSetScanner(TestCase):
    def setUp(self):
        self.client = Client()

    def test_set_scanner_reader(self):
        scanner = "reader"
        response = self.client.get(reverse("barcodes:set", args=(scanner,)))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("barcodes:read"))
        self.assertEqual(self.client.session["scanner"], scanner)

    def test_set_scanner_mobile(self):
        scanner = "mobile"
        response = self.client.get(reverse("barcodes:set", args=(scanner,)))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("barcodes:read"))
        self.assertEqual(self.client.session["scanner"], scanner)

    def test_set_scanner_with_options(self):
        scanner = "reader"
        parent_id = "123"
        option = "compose"
        response = self.client.get(
            reverse("barcodes:set", args=(scanner,)),
            {"parent_id": parent_id, "option": option},
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url,
            reverse("barcodes:read") + f"?parent_id={parent_id}&option={option}",
        )
        self.assertEqual(self.client.session["scanner"], scanner)


class TestScanBarcode(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(Permission.objects.get(codename="view_artefact"))
        self.client.force_login(self.user)

    def test_get_mobile(self):
        session = self.client.session
        session["scanner"] = "mobile"
        session.save()
        response = self.client.get(reverse("barcodes:read"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "barcodes/scanner.html")

    def test_get_reader(self):
        session = self.client.session
        session["scanner"] = "reader"
        session.save()
        response = self.client.get(reverse("barcodes:read"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "barcodes/form.html")

    def test_get_false(self):
        session = self.client.session
        session["scanner"] = False
        session.save()
        response = self.client.get(reverse("barcodes:read"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "barcodes/scanner.html")
        self.assertEqual(self.client.session["scanner"], "mobile")


class TestSearchBarcode(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(Permission.objects.get(codename="view_artefact"))
        self.user.user_permissions.add(
            Permission.objects.get(codename="view_processingline")
        )
        self.client.force_login(self.user)

    def test_find_none(self):
        barcode = "123456ABC"
        ArtefactFactory.create(rescue_number="ABC123456")
        response = self.client.get(
            "{}?barcode={}".format(reverse("barcodes:search"), barcode)
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["barcode"], barcode)
        self.assertTemplateUsed(response, "barcodes/create.html")

    def test_find_one_artefact(self):
        barcode = "123456ABC"
        artefact = ArtefactFactory.create(rescue_number=barcode)
        response = self.client.get(
            "{}?barcode={}".format(reverse("barcodes:search"), barcode)
        )
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            reverse("artefacts:scan", kwargs={"pk": artefact.pk}),
            target_status_code=302,
            fetch_redirect_response=True,
        )

    def test_find_one_non_artefact(self):
        line = ProcessingLineFactory.create()
        line.name = "Test Line"
        line.save()
        barcode = line.short_id
        response = self.client.get(
            "{}?barcode={}".format(reverse("barcodes:search"), barcode)
        )
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            reverse("treatments:scan-processing-line", kwargs={"pk": line.pk}),
            fetch_redirect_response=True,
        )

    def test_find_multiple_artefacts(self):
        barcode = "654321ABC"
        artefact1 = ArtefactFactory.create(rescue_number=barcode)
        artefact2 = ArtefactFactory.create(inventory_number=barcode)
        response = self.client.get(
            "{}?barcode={}".format(reverse("barcodes:search"), barcode)
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["barcode"], barcode)
        self.assertTemplateUsed(response, "barcodes/results.html")
        self.assertEqual(
            response.context["objects"][0]["code"], artefact1.rescue_number
        )
        self.assertEqual(
            response.context["objects"][1]["code"], artefact2.rescue_number
        )

    def test_submit_empty_barcode(self):
        barcode = "123456ABC"
        ArtefactFactory.create(rescue_number=barcode)
        response = self.client.get("{}?barcode=".format(reverse("barcodes:search")))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "barcodes/form.html")

    def test_submit_no_barcode(self):
        barcode = "123456ABC"
        ArtefactFactory.create(rescue_number=barcode)
        response = self.client.get(reverse("barcodes:search"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "barcodes/form.html")

    def test_anonymous_user(self):
        barcode = "123456ABC"
        ArtefactFactory.create(rescue_number="ABC123456")
        self.user = AnonymousUser()
        self.client.get("{}?barcode={}".format(reverse("barcodes:search"), barcode))
        self.assertRaises(PermissionDenied)

    def test_find_existing_collection_only(self):
        barcode = "654321ABC"
        existing_collection = ExistingCollectionFactory.create(inventory_number=barcode)
        response = self.client.get(
            "{}?barcode={}".format(reverse("barcodes:search"), barcode)
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["barcode"], barcode)
        self.assertTemplateUsed(response, "barcodes/import.html")
        self.assertEqual(response.context["existing_collection"], existing_collection)

    def test_find_artefact_and_existing_collection(self):
        barcode = "654321ABC"
        artefact = ArtefactFactory.create(inventory_number=barcode)
        existing_collection = ExistingCollectionFactory.create(inventory_number=barcode)
        self.assertIsNotNone(existing_collection.pk)  # add this line
        response = self.client.get(
            "{}?barcode={}".format(reverse("barcodes:search"), barcode)
        )
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, reverse("artefacts:view", kwargs={"pk": artefact.pk})
        )
