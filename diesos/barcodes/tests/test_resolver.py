import pytest
from django.test import TestCase

from diesos.artefacts.tests.factories import ArtefactFactory
from diesos.barcodes.barcode_resolver import resolve_barcode

pytestmark = pytest.mark.django_db


class TestBarcodeResolver(TestCase):
    def test_not_found(self):
        """Test the response when no objects are found with the barcode."""
        barcode = "123456ABC"
        ArtefactFactory.create(rescue_number="ABC123456")
        response = resolve_barcode(barcode)
        assert not response

    def test_found_one(self):
        """Test the response when exactly one object is found with the barcode."""
        barcode = "123456ABC"
        ArtefactFactory.create(rescue_number=barcode)
        response = resolve_barcode(barcode)
        assert len(response) == 1
        assert response[0]["code"] == barcode

    def test_found_multiple(self):
        """Test the response when multiple objects are found with the barcode."""
        barcode = "123456ABC"
        ArtefactFactory.create(rescue_number=barcode)
        artefact = ArtefactFactory.create(inventory_number=barcode)
        response = resolve_barcode(barcode)
        assert len(response) == 2
        assert response[0]["code"] in [barcode, artefact.rescue_number]
        assert response[1]["code"] in [barcode, artefact.rescue_number]

    def test_name_with_inventory_number(self):
        """Test the response when the artefact has an inventory number."""
        barcode = "123456ABC"
        artefact = ArtefactFactory.create(rescue_number=barcode)
        response = resolve_barcode(barcode)
        assert len(response) == 1
        assert response[0]["name"] == f"{artefact.name} ({artefact.inventory_number})"

    def test_name_without_inventory_number(self):
        """Test the response when the artefact has no inventory number."""
        barcode = "123456ABC"
        artefact = ArtefactFactory.create(rescue_number=barcode)
        artefact.inventory_number = ""
        artefact.save()
        response = resolve_barcode(barcode)
        assert len(response) == 1
        assert response[0]["name"] == artefact.name

    def test_random_url(self):
        """Test the response when we sumbit a random url."""
        barcode = "https://www.diesos.ch/xyz/"
        response = resolve_barcode(barcode)
        assert len(response) == 0

    def test_diesos_url(self):
        """Test the response when we sumbit a Diesos url."""
        artefact = ArtefactFactory.create(rescue_number="123456ABC")
        barcode = artefact.get_absolute_url()
        response = resolve_barcode(barcode)
        assert len(response) == 1
        assert response[0]["url"] == barcode
