from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from safedelete.models import SafeDeleteModel
from treebeard.mp_tree import MP_Node


class LocationType(SafeDeleteModel):
    name = models.CharField(verbose_name=_("name"), max_length=225)
    description = models.TextField(verbose_name=_("description"), null=True, blank=True)

    class Meta:
        verbose_name = _("location type")
        verbose_name_plural = _("location types")
        ordering = ["name"]

    def __str__(self):
        return f"{self.name}"

    def get_absolute_url(self):
        return reverse("locations:view-type", kwargs={"pk": self.pk})


class Location(MP_Node, SafeDeleteModel):
    name = models.CharField(
        verbose_name=_("location name"), max_length=225, unique=True
    )
    description = models.TextField(verbose_name=_("description"), null=True, blank=True)
    color = models.CharField(max_length=7, null=True, blank=True)

    location_type = models.ForeignKey(
        LocationType,
        verbose_name=_("location type"),
        related_name="locations",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    elevation = models.IntegerField(
        verbose_name=_("elevation"),
        null=True,
        blank=True,
    )
    latitude = models.DecimalField(
        verbose_name=_("latitude"),
        max_digits=9,
        decimal_places=6,
        null=True,
        blank=True,
    )
    longitude = models.DecimalField(
        verbose_name=_("longitude"),
        max_digits=9,
        decimal_places=6,
        null=True,
        blank=True,
    )

    node_order_by = ["name"]

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = _("location")
        verbose_name_plural = _("locations")
        ordering = ["name"]

    def get_absolute_url(self):
        return reverse("locations:view", kwargs={"pk": self.pk})
