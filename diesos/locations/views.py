from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    ListView,
    UpdateView,
)
from treebeard.forms import movenodeform_factory

from .models import Location, LocationType


class LocationTypesListView(PermissionRequiredMixin, ListView):
    permission_required = "locations.view_locationtype"
    model = LocationType


class LocationTypeDetailView(PermissionRequiredMixin, DetailView):
    permission_required = "locations.view_locationtype"
    model = LocationType


class LocationTypeCreateView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    permission_required = "locations.add_locationtype"
    model = LocationType
    fields = ["name", "description"]
    success_url = reverse_lazy("locations:list-types")
    success_message = _("The location type <strong>%(name)s</strong> has been created.")


class LocationTypeUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    permission_required = "locations.change_locationtype"
    model = LocationType
    fields = ["name", "description"]
    success_url = reverse_lazy("locations:list-types")
    success_message = _(
        "The  location type <strong>%(name)s</strong> has been updated."
    )


class LocationTypeDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = "locations.delete_locationtype"
    model = LocationType

    def get_success_url(self):
        return reverse("locations:list-types")

    def form_valid(self, form):
        response = super().form_valid(form)
        obj = self.object
        url = reverse("locations:undelete-type", kwargs={"pk": obj.pk})
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                f'The location type <strong>{obj.name}</strong> has been \
                    deleted. If it was a mistake, you can <a href="{url}">restore it</a>.'
            ),
        )
        return response


@permission_required("locations.delete_locationtype")
def undelete_location_type(request, pk):
    location_type = LocationType.all_objects.get(pk=pk)
    location_type.undelete()
    messages.add_message(
        request,
        messages.SUCCESS,
        _(
            f"The location type <strong>{location_type.name}</strong> has been restored."
        ),
    )
    return redirect("locations:list-types")


class LocationListView(PermissionRequiredMixin, ListView):
    permission_required = "locations.view_location"
    model = Location


class LocationDetailView(PermissionRequiredMixin, DetailView):
    permission_required = "locations.view_location"
    model = Location


class LocationCreateView(PermissionRequiredMixin, CreateView):
    permission_required = "locations.add_location"
    model = Location
    form_class = movenodeform_factory(Location)


class LocationUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = "locations.change_location"
    model = Location
    form_class = movenodeform_factory(Location)


class LocationDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = "locations.delete_location"
    model = Location


@permission_required("locations.delete_location")
def undelete_location(request, pk):
    location = Location.all_objects.get(pk=pk)
    location.undelete()
    messages.add_message(
        request,
        messages.SUCCESS,
        _(f"The location <strong>{location.name}</strong> has been restored."),
    )
    return redirect("locations:list")
