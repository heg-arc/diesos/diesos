from factory import Faker, Sequence, SubFactory
from factory.django import DjangoModelFactory

from diesos.locations.models import Location, LocationType


class LocationTypeFactory(DjangoModelFactory):
    name = Faker("text", max_nb_chars=20)
    description = Faker("paragraph", nb_sentences=2, variable_nb_sentences=True)

    class Meta:
        model = LocationType
        django_get_or_create = ["name"]


class LocationFactory(DjangoModelFactory):
    class Meta:
        model = Location

    name = Sequence(lambda n: "Location %d" % n)
    description = Faker("paragraph", nb_sentences=2, variable_nb_sentences=True)
    color = Faker("hex_color")
    location_type = SubFactory(LocationTypeFactory)
    elevation = Faker("pyint")
    latitude = Faker("pydecimal", left_digits=3, right_digits=6)
    longitude = Faker("pydecimal", left_digits=3, right_digits=6)

    # Very naive handling of treebeard node fields. Works though!
    depth = 1
    path = Sequence(lambda n: "%04d" % n)
