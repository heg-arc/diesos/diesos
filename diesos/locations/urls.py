from django.urls import path

from diesos.locations import views

app_name = "locations"

urlpatterns = [
    path("", views.LocationListView.as_view(), name="list"),
    path("add", views.LocationCreateView.as_view(), name="add"),
    path("<int:pk>", views.LocationDetailView.as_view(), name="view"),
    path("<int:pk>/change", views.LocationUpdateView.as_view(), name="change"),
    path("<int:pk>/delete", views.LocationDetailView.as_view(), name="delete"),
    path("<int:pk>/undelete", views.undelete_location, name="undelete-type"),
    path("types", views.LocationTypesListView.as_view(), name="list-types"),
    path("types/add", views.LocationTypeCreateView.as_view(), name="add-type"),
    path("types/<int:pk>", views.LocationTypeDetailView.as_view(), name="view-type"),
    path(
        "types/<int:pk>/change",
        views.LocationTypeUpdateView.as_view(),
        name="change-type",
    ),
    path(
        "types/<int:pk>/delete",
        views.LocationTypeDeleteView.as_view(),
        name="delete-type",
    ),
    path("types/<int:pk>/undelete", views.undelete_location_type, name="undelete-type"),
]
