from datetime import timedelta

from django import forms
from django.utils.translation import gettext_lazy as _


class DurationWidget(forms.MultiWidget):
    def __init__(self, attrs=None):
        widgets = (
            forms.NumberInput(
                attrs={
                    "min": 0,
                    "class": "me-2",
                    "label": _("hours"),
                }
            ),
            forms.NumberInput(
                attrs={
                    "min": 0,
                    "label": _("minutes"),
                }
            ),
        )
        super().__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return [value.seconds // 3600, (value.seconds // 60) % 60]
        return [0, 0]

    def value_from_datadict(self, data, files, name):
        hours, minutes = super().value_from_datadict(data, files, name)
        if hours is None:
            hours = 0
        if minutes is None:
            minutes = 0

        days = 0
        hours = int(hours)
        minutes = int(minutes)

        if hours >= 24:
            days = hours // 24
            hours = hours % 24

        return timedelta(days=days, hours=hours, minutes=minutes)

    def render(self, name, value, attrs=None, renderer=None):
        decompressed_value = self.decompress(value)

        widgets = []
        for i, widget in enumerate(self.widgets):
            widget_value = decompressed_value[i] if decompressed_value else None
            rendered_widget = widget.render(
                name + "_%s" % i, widget_value, attrs, renderer
            )
            label = widget.attrs.get("label", None)
            if label:
                rendered_widget = f'<div class="flex-fill"><label>{label}</label>{rendered_widget}</div>'
            widgets.append(rendered_widget)

        return f'<div class="border rounded p-3"><div class="d-flex">{"".join(widgets)}</div></div>'
