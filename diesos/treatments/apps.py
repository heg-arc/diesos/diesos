from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class TreatmentsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "diesos.treatments"
    verbose_name = _("Treatments")

    def ready(self):
        from actstream import registry

        registry.register(self.get_model("ProcessingLine"))
        registry.register(self.get_model("TreatmentType"))
