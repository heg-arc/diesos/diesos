from diesos.treatments.models import ProcessingLine


class ProcessingLineMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        processing_line = request.session.get("processing_line", False)
        if processing_line:
            request.processing_line = ProcessingLine.objects.get(pk=processing_line)
        else:
            request.processing_line = None
        response = self.get_response(request)
        # Code to be executed for each request/response after
        # the view is called.
        return response
