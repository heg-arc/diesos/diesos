from actstream import action
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    ListView,
    UpdateView,
)
from wkhtmltopdf.views import PDFTemplateView

from diesos.artefacts.models import StabilizationMeasure

from .forms import TreatmentTypeForm
from .models import ProcessingLine, TreatmentType


class ProcessingLinesSelectView(PermissionRequiredMixin, ListView):
    permission_required = "treatments.view_processingline"
    template_name = "treatments/processingline_select_list.html"
    model = ProcessingLine

    def dispatch(self, request, *args, **kwargs):
        pk = self.kwargs.get("pk", None)
        if pk:
            if request.processing_line:
                # The user is already in a processing line
                action.send(
                    self.request.user,
                    verb=_("left"),
                    action_object=request.processing_line,
                )
            # We set the processing line in the session
            processing_line = get_object_or_404(ProcessingLine, pk=pk)
            if processing_line.running:
                request.session["processing_line"] = pk
                action.send(
                    self.request.user, verb=_("joined"), action_object=processing_line
                )
            else:
                messages.add_message(
                    request,
                    messages.ERROR,
                    _("You cannot select a closed processing line!"),
                )
                return redirect("treatments:select-location")
            return redirect("treatments:view-processing-line", pk=processing_line.id)
        elif request.resolver_match.url_name == "select-location-clear":
            # We clear the processing line from the session
            if request.processing_line:
                # The user is already in a processing line
                action.send(
                    self.request.user,
                    verb=_("left"),
                    action_object=request.processing_line,
                )
            request.session.pop("processing_line", None)
            return redirect("home")
        return super().dispatch(request, *args, **kwargs)


class ProcessingLineDetailView(PermissionRequiredMixin, DetailView):
    permission_required = "treatments.view_processingline"
    model = ProcessingLine

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        processing_line = ProcessingLine.objects.get(pk=self.kwargs["pk"])
        stabilization_measures = StabilizationMeasure.objects.filter(
            treatment_type=processing_line.treatment_type,
            processing_line=processing_line.pk,
        )

        stabilization_in_treatment = stabilization_measures.filter(
            status=StabilizationMeasure.IN_TREATMENT,
        )
        nb_stabilization_finish = stabilization_measures.filter(
            status=StabilizationMeasure.FINISH,
        ).count()

        nb_stabilization_awaiting = 0
        stabilization_awaiting = StabilizationMeasure.objects.filter(
            treatment_type=processing_line.treatment_type,
            status=None,
        )
        for value in stabilization_awaiting:
            previous_measure = value.artefact.stabilization_measures.filter(
                priority_order=value.priority_order - 1
            ).first()
            if (
                previous_measure
                and previous_measure.status == StabilizationMeasure.FINISH
            ):
                nb_stabilization_awaiting += 1

        context["stabilization_measures"] = stabilization_in_treatment
        context["nb_stabilization_finish"] = nb_stabilization_finish
        context["nb_stabilization_awaiting"] = nb_stabilization_awaiting

        action.send(self.request.user, verb=_("viewed"), action_object=self.object)

        return context


class ProcessingLineListView(PermissionRequiredMixin, ListView):
    permission_required = "treatments.view_processingline"
    model = ProcessingLine


class ProcessingLineCreateView(
    PermissionRequiredMixin, SuccessMessageMixin, CreateView
):
    permission_required = "treatments.add_processingline"
    model = ProcessingLine
    fields = ["name", "treatment_type", "capacity", "location"]
    success_url = reverse_lazy("treatments:list-processing-line")
    success_message = _(
        "The processing line <strong>%(name)s</strong> has been created."
    )


class ProcessingLineUpdateView(
    PermissionRequiredMixin, SuccessMessageMixin, UpdateView
):
    permission_required = "treatments.change_processingline"
    model = ProcessingLine
    fields = ["name", "treatment_type", "capacity", "location"]
    success_url = reverse_lazy("treatments:list-processing-line")
    success_message = _(
        "The processing line <strong>%(name)s</strong> has been updated."
    )


class ProcessingLineScanView(PermissionRequiredMixin, DetailView):
    permission_required = "treatments.view_processingline"
    template_name = "treatments/processingline_scan.html"
    model = ProcessingLine


class ProcessingLineDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = "treatments.delete_processingline"
    model = ProcessingLine
    success_url = reverse_lazy("treatments:list-processing-line")

    def form_valid(self, form):
        response = super().form_valid(form)
        obj = self.object
        url = reverse("treatments:undelete-processing-line", kwargs={"pk": obj.pk})
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                f'The processing line <strong>{obj.name}</strong> has been \
                deleted. If it was a mistake, you can <a href="{url}">restore it</a>.'
            ),
        )
        return response


class ProcessingLinePrintLabel(PermissionRequiredMixin, PDFTemplateView):
    permission_required = "treatments.view_processingline"
    template_name = "treatments/pdf/processingline_label.html"
    header_template = "pdf_header.html"
    footer_template = "pdf_footer.html"
    filename = "processing_line_label.pdf"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["object"] = ProcessingLine.objects.get(pk=self.kwargs["pk"])
        context["base_url"] = f"{self.request.build_absolute_uri('/')[:-1]}"
        context[
            "qr_code"
        ] = f"{context['base_url']}{reverse('treatments:scan-processing-line', kwargs={'pk': context['object'].pk})}"  # noqa E501
        return context


def undelete_processing_line(request, pk):
    processing_line = ProcessingLine.all_objects.get(pk=pk)
    processing_line.undelete()
    messages.add_message(
        request,
        messages.SUCCESS,
        _(
            f"The processing line <strong>{processing_line.name}</strong> has been \
            restored."
        ),
    )
    return redirect("treatments:list-processing-line")


def toggle_operation_processing_line(request, pk):
    processing_line = ProcessingLine.objects.get(pk=pk)
    if processing_line.running:
        processing_line.stop()
        action.send(request.user, verb=_("closed"), action_object=processing_line)
        messages.add_message(
            request,
            messages.SUCCESS,
            _(
                f"The processing line <strong>{processing_line.name}</strong> has been \
                    closed."
            ),
        )
    else:
        processing_line.start()
        action.send(request.user, verb=_("opened"), action_object=processing_line)
        messages.add_message(
            request,
            messages.SUCCESS,
            _(
                f"The processing line <strong>{processing_line.name}</strong> has been \
                    opened."
            ),
        )
    return redirect("treatments:view-processing-line", pk=pk)


class TreatmentTypeDetailView(PermissionRequiredMixin, DetailView):
    permission_required = "treatments.view_treatmenttype"
    model = TreatmentType

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        treatment_type = TreatmentType.objects.get(pk=self.kwargs["pk"])

        stabilization_measures = StabilizationMeasure.objects.filter(
            treatment_type=treatment_type,
        )

        stabilization_measures_with_awaiting = []
        for value in stabilization_measures:
            if value.status is None:
                previous_measure = value.artefact.stabilization_measures.filter(
                    priority_order=value.priority_order - 1
                ).first()
                if (
                    previous_measure
                    and previous_measure.status == StabilizationMeasure.FINISH
                ):
                    stabilization_measures_with_awaiting.append(value)
            else:
                stabilization_measures_with_awaiting.append(value)

        context["treatment_type"] = treatment_type
        context["stabilization_measures"] = stabilization_measures_with_awaiting

        action.send(self.request.user, verb=_("viewed"), action_object=self.object)

        return context


class TreatmentTypeDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = "treatments.delete_treatmenttype"
    model = TreatmentType
    success_url = reverse_lazy("treatments:list-treatment-type")

    def form_valid(self, form):
        response = super().form_valid(form)
        obj = self.object
        url = reverse("treatments:undelete-treatment-type", kwargs={"pk": obj.pk})
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                f'The treatment type <strong>{obj.name}</strong> has been \
                    deleted. If it was a mistake, you can <a href="{url}">restore it</a>.'
            ),
        )
        return response


def undelete_treatment_type(request, pk):
    treatment_type = TreatmentType.all_objects.get(pk=pk)
    treatment_type.undelete()
    messages.add_message(
        request,
        messages.SUCCESS,
        _(
            f"The treatment type <strong>{treatment_type.name}</strong> has been \
                restored."
        ),
    )
    return redirect("treatments:list-treatment-type")


class TreatmentTypeListView(PermissionRequiredMixin, ListView):
    permission_required = "treatments.view_treatmenttype"
    model = TreatmentType


class TreatmentTypeUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    permission_required = "treatments.change_treatmenttype"
    model = TreatmentType
    # TODO add icon select list
    # TODO add color select list
    form_class = TreatmentTypeForm

    success_url = reverse_lazy("treatments:list-treatment-type")
    success_message = _("The treatment <strong>%(name)s</strong> has been updated.")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = _("update")
        return context


class TreatmentTypeCreateView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    permission_required = "treatments.add_treatmenttype"
    model = TreatmentType
    # TODO add icon select list
    # TODO add color select list
    form_class = TreatmentTypeForm
    success_url = reverse_lazy("treatments:list-treatment-type")
    success_message = _("The treatment <strong>%(name)s</strong> has been created.")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = _("create")
        return context
