from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from safedelete.models import SafeDeleteModel

from diesos.locations.models import Location
from diesos.utils.models import UniqueShortId


class TreatmentType(SafeDeleteModel):
    name = models.CharField(verbose_name=_("name"), max_length=225, unique=True)
    description = models.TextField(verbose_name=_("description"), null=True, blank=True)
    icon_class = models.CharField(
        verbose_name=_("icon"), max_length=255, null=True, blank=True
    )
    color = models.CharField(
        verbose_name=_("color"), max_length=7, null=True, blank=True
    )
    duration = models.DurationField(verbose_name=_("duration"), null=True, blank=True)

    # TODO: Add a M2M field with TypeOfDamage and a priority_order

    class Meta:
        verbose_name = _("treatment type")
        verbose_name_plural = _("treatment types")
        ordering = ["name"]

    def __str__(self):
        return f"{self.name}"

    def get_absolute_url(self):
        return reverse("treatments:view-treatment-type", kwargs={"pk": self.pk})


class ProcessingLine(SafeDeleteModel):
    name = models.CharField(verbose_name=_("name"), max_length=225)
    treatment_type = models.ForeignKey(
        TreatmentType,
        verbose_name=_("treatment type"),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )
    capacity = models.IntegerField(
        verbose_name=_("capacity"),
        blank=True,
        null=True,
        default=0,
        help_text=_(
            "The number of units that can be processed at the same time or 0 if there is no limit."
        ),
    )
    location = models.ForeignKey(
        Location,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="processing_lines",
        verbose_name=_("location"),
    )
    running = models.BooleanField(
        verbose_name=_("running"),
        default=False,
        help_text=_("Set to true if the processing line is currently running."),
    )
    short_id = models.CharField(
        max_length=5, unique=True, null=True, verbose_name=_("short id")
    )

    class Meta:
        verbose_name = _("processing line")
        verbose_name_plural = _("processing lines")
        ordering = ["name"]
        permissions = (("can_run_processing_line", "Can run processing line"),)

    def __str__(self):
        return f"{self.name}"

    def start(self):
        # TODO: Add more business logic here
        self.running = True
        self.save()

    def stop(self):
        # TODO: Add more business logic here
        self.running = False
        self.save()

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # Generate and store unique short id after saving
        if not self.short_id:
            unique_short_id = UniqueShortId.create_unique_short_id_instance(
                content_object=self
            )
            self.short_id = unique_short_id.short_id
            self.save()

    def get_absolute_url(self):
        return reverse("treatments:view-processing-line", kwargs={"pk": self.pk})

    def get_scan_url(self):
        return reverse("treatments:scan-processing-line", kwargs={"pk": self.pk})
