import re

from django import forms
from django.core.exceptions import ValidationError

from .models import TreatmentType
from .widgets import DurationWidget


class TreatmentTypeForm(forms.ModelForm):
    duration = forms.CharField(
        widget=DurationWidget(),
        required=False,
    )

    class Meta:
        model = TreatmentType
        fields = [
            "name",
            "icon_class",
            "color",
            "duration",
            "description",
        ]

    def clean_color(self):
        color = self.cleaned_data.get("color")
        if not color:
            return color

        # Example of possible value : #f01f3e / #f01
        if not re.match("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$", color):
            raise ValidationError("The hexadecimal color code is not valid")
        return color
