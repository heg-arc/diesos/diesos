from django.test import TestCase
from django.urls import resolve, reverse

from diesos.treatments.tests.factories import TreatmentTypeFactory


class TestUrlsTestCase(TestCase):
    def test_add(self):
        assert reverse("treatments:add-treatment-type") == "/treatments/type/add"
        assert (
            resolve("/treatments/type/add").view_name == "treatments:add-treatment-type"
        )

    def test_list(self):
        assert reverse("treatments:list-treatment-type") == "/treatments/types"
        assert (
            resolve("/treatments/types").view_name == "treatments:list-treatment-type"
        )

    def test_change(self):
        treatment_type = TreatmentTypeFactory.create()
        assert (
            reverse(
                "treatments:change-treatment-type",
                kwargs={"pk": treatment_type.pk},
            )
            == f"/treatments/type/{treatment_type.pk}/change"
        )
        assert (
            resolve(f"/treatments/type/{treatment_type.pk}/change").view_name
            == "treatments:change-treatment-type"
        )

    def test_delete(self):
        treatment_type = TreatmentTypeFactory.create()
        assert (
            reverse(
                "treatments:delete-treatment-type",
                kwargs={"pk": treatment_type.pk},
            )
            == f"/treatments/type/{treatment_type.pk}/delete"
        )
        assert (
            resolve(f"/treatments/type/{treatment_type.pk}/delete").view_name
            == "treatments:delete-treatment-type"
        )
