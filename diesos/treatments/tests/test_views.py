import pytest
from django.contrib import messages
from django.contrib.auth.models import Permission
from django.contrib.messages import get_messages
from django.test import Client, TestCase
from django.urls import reverse

from diesos.artefacts.models import StabilizationMeasure
from diesos.artefacts.tests.factories import (
    ArtefactFactory,
    StabilizationMeasureFactory,
)
from diesos.treatments.models import ProcessingLine, TreatmentType
from diesos.treatments.tests.factories import (
    ProcessingLineFactory,
    TreatmentTypeFactory,
)
from diesos.users.tests.factories import UserFactory

pytestmark = pytest.mark.django_db


class TestProcessingLineMiddleware(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="view_processingline")
        )
        self.client.force_login(self.user)
        self.processing_line = ProcessingLineFactory.create()

    def test_session_processing_line_request(self):
        session = self.client.session
        session["processing_line"] = self.processing_line.pk
        session.save()
        response = self.client.get(reverse("home"))
        request = response.wsgi_request
        self.assertEqual(request.processing_line, self.processing_line)
        html = response.content.decode("utf8")
        self.assertInHTML(self.processing_line.name, html)

    def test_no_session_processing_line_request(self):
        response = self.client.get(reverse("home"))
        request = response.wsgi_request
        self.assertEqual(request.processing_line, None)
        html = response.content.decode("utf8")
        self.assertInHTML("You are currently not registered at a processing line", html)


class TestProcessingLinesSelectView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="view_processingline")
        )
        self.client.force_login(self.user)
        self.processing_line = ProcessingLineFactory.create(running=True)

    def test_list_view_no_processing_line(self):
        response = self.client.get(reverse("treatments:select-location"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["processing_line"], None)
        self.assertEqual(response.context["object_list"][0], self.processing_line)
        self.assertTemplateUsed(response, "treatments/processingline_select_list.html")
        html = response.content.decode("utf8")
        self.assertInHTML("You are currently not registered at a processing line", html)

    def test_list_view_with_processing_line(self):
        session = self.client.session
        session["processing_line"] = self.processing_line.pk
        session.save()
        response = self.client.get(reverse("treatments:select-location"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context["processing_line"].id, self.processing_line.pk
        )
        self.assertEqual(response.context["object_list"][0], self.processing_line)
        self.assertTemplateUsed(response, "treatments/processingline_select_list.html")
        html = response.content.decode("utf8")
        self.assertInHTML(self.processing_line.name, html)
        print(html)
        self.assertInHTML(
            '<a href="/treatments/select-location/clear" class="btn btn-danger"><i class="bi bi-box-arrow-right"></i> Check out</a>',  # noqa: E501
            html,
        )

    def test_set_processing_line_running(self):
        response = self.client.get(
            reverse(
                "treatments:select-location-choice",
                kwargs={"pk": self.processing_line.pk},
            )
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url,
            reverse(
                "treatments:view-processing-line",
                kwargs={"pk": self.processing_line.pk},
            ),
        )
        self.assertEqual(
            self.client.session["processing_line"], self.processing_line.pk
        )
        self.assertEqual(self.processing_line.action_object_actions.count(), 1)

    def test_set_processing_line_closed(self):
        self.processing_line.running = False
        self.processing_line.save()
        response = self.client.get(
            reverse(
                "treatments:select-location-choice",
                kwargs={"pk": self.processing_line.pk},
            )
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("treatments:select-location"))
        self.assertNotIn("processing_line", self.client.session)

        messages_list = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages_list), 1)
        self.assertEqual(messages_list[0].level, messages.ERROR)
        self.assertEqual(self.processing_line.action_object_actions.count(), 0)

    def test_clear_processing_line(self):
        session = self.client.session
        session["processing_line"] = self.processing_line.pk
        session.save()
        self.assertIn("processing_line", self.client.session)
        response = self.client.get(reverse("treatments:select-location-clear"))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("home"))
        self.assertNotIn("processing_line", self.client.session)
        self.assertEqual(self.processing_line.action_object_actions.count(), 1)

    def test_switch_processing_line_running(self):
        processing_line_2 = ProcessingLineFactory.create(running=True)
        session = self.client.session
        session["processing_line"] = processing_line_2.pk
        session.save()
        response = self.client.get(
            reverse(
                "treatments:select-location-choice",
                kwargs={"pk": self.processing_line.pk},
            )
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url,
            reverse(
                "treatments:view-processing-line",
                kwargs={"pk": self.processing_line.pk},
            ),
        )
        self.assertEqual(
            self.client.session["processing_line"], self.processing_line.pk
        )
        self.assertEqual(self.processing_line.action_object_actions.count(), 1)
        self.assertEqual(processing_line_2.action_object_actions.count(), 1)


class TestProcessingLinesDetailView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.permission = Permission.objects.get(codename="view_processingline")
        self.user.user_permissions.add(self.permission)
        self.client.force_login(self.user)
        self.treatment_type = TreatmentTypeFactory()
        self.processing_line = ProcessingLineFactory(
            treatment_type=self.treatment_type, running=True
        )

    def test_authenticated_with_permissions(self):
        response = self.client.get(
            reverse(
                "treatments:view-processing-line",
                kwargs={"pk": self.processing_line.pk},
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "treatments/processingline_detail.html")

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(self.permission)
        response = self.client.get(
            reverse(
                "treatments:view-processing-line",
                kwargs={"pk": self.processing_line.pk},
            )
        )
        self.assertEqual(response.status_code, 403)

    def test_count_stabilization_awaiting(self):
        self.artefact_1 = ArtefactFactory()
        self.stabilization_measure_1 = StabilizationMeasureFactory(
            artefact=self.artefact_1,
            priority_order=1,
            status=StabilizationMeasure.FINISH,
            processing_line=self.processing_line,
            treatment_type=self.treatment_type,
        )

        self.treatment_type_2 = TreatmentTypeFactory()
        self.processing_line_2 = ProcessingLineFactory(
            treatment_type=self.treatment_type_2, running=True
        )
        self.stabilization_measure_2 = StabilizationMeasureFactory(
            artefact=self.artefact_1,
            priority_order=2,
            status=None,
            treatment_type=self.treatment_type_2,
        )

        response = self.client.get(
            reverse(
                "treatments:view-processing-line",
                kwargs={"pk": self.processing_line_2.pk},
            )
        )
        self.assertEqual(response.status_code, 200)

        context = response.context

        # Check number of stabilization awaiting
        nb_stabilization_awaiting = context["nb_stabilization_awaiting"]
        self.assertEqual(nb_stabilization_awaiting, 1)

    def test_count_stabilization_finished(self):
        self.artefact_1 = ArtefactFactory()
        self.stabilization_measure_1 = StabilizationMeasureFactory(
            artefact=self.artefact_1,
            priority_order=1,
            status=StabilizationMeasure.FINISH,
            processing_line=self.processing_line,
            treatment_type=self.treatment_type,
        )

        self.treatment_type_2 = TreatmentTypeFactory()
        self.artefact_2 = ArtefactFactory()
        self.stabilization_measure_2 = StabilizationMeasureFactory(
            artefact=self.artefact_2,
            priority_order=1,
            status=StabilizationMeasure.FINISH,
            processing_line=self.processing_line,
            treatment_type=self.treatment_type,
        )
        self.stabilization_measure_3 = StabilizationMeasureFactory(
            artefact=self.artefact_2,
            priority_order=2,
            status=StabilizationMeasure.FINISH,
            treatment_type=self.treatment_type_2,
        )

        response = self.client.get(
            reverse(
                "treatments:view-processing-line",
                kwargs={"pk": self.processing_line.pk},
            )
        )
        self.assertEqual(response.status_code, 200)

        context = response.context
        # Check number of stabilization finished
        nb_stabilization_finish = context["nb_stabilization_finish"]
        self.assertEqual(nb_stabilization_finish, 2)

    def test_count_stabilization_in_treatment(self):
        self.artefact_1 = ArtefactFactory()
        self.stabilization_measure_1 = StabilizationMeasureFactory(
            artefact=self.artefact_1,
            priority_order=1,
            status=StabilizationMeasure.FINISH,
            processing_line=self.processing_line,
            treatment_type=self.treatment_type,
        )

        self.artefact_2 = ArtefactFactory()
        self.stabilization_measure_2 = StabilizationMeasureFactory(
            artefact=self.artefact_2,
            priority_order=1,
            status=StabilizationMeasure.IN_TREATMENT,
            processing_line=self.processing_line,
            treatment_type=self.treatment_type,
        )

        response = self.client.get(
            reverse(
                "treatments:view-processing-line",
                kwargs={"pk": self.processing_line.pk},
            )
        )
        self.assertEqual(response.status_code, 200)

        context = response.context

        # Check number of stabilization in treatment
        stabilization_in_treatment = context["stabilization_measures"]
        self.assertEqual(stabilization_in_treatment.count(), 1)


class DeleteTreatmentTypeTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="delete_treatmenttype")
        )
        self.client.force_login(self.user)
        self.type = TreatmentTypeFactory.create(name="Type 1")

    def test_delete_artefact_type(self):
        self.assertEqual(TreatmentType.objects.count(), 1)

        response = self.client.post(
            reverse("treatments:delete-treatment-type", args=[self.type.id])
        )

        # Check if the view has returned a redirect to 'treatments:list-treatment-type'
        self.assertRedirects(
            response,
            reverse("treatments:list-treatment-type"),
            fetch_redirect_response=False,
        )

        self.assertEqual(TreatmentType.objects.count(), 0)
        self.assertEqual(TreatmentType.all_objects.count(), 1)

        messages_list = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages_list), 1)
        self.assertEqual(messages_list[0].level, messages.SUCCESS)

    def test_undelete_artefact_type(self):
        self.assertEqual(TreatmentType.objects.count(), 1)
        self.type.delete()
        self.assertEqual(TreatmentType.objects.count(), 0)

        response = self.client.get(
            reverse("treatments:undelete-treatment-type", args=[self.type.id])
        )

        # Check if the view has returned a redirect to 'treatments:list-treatment-type'
        self.assertRedirects(
            response,
            reverse("treatments:list-treatment-type"),
            fetch_redirect_response=False,
        )

        self.assertEqual(TreatmentType.objects.count(), 1)
        self.assertEqual(TreatmentType.objects.first().name, "Type 1")


class DeleteProcessingLineTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="delete_processingline")
        )
        self.client.force_login(self.user)
        self.processing_line = ProcessingLineFactory.create(name="Type 1")

    def test_delete_processing_line(self):
        self.assertEqual(ProcessingLine.objects.count(), 1)

        response = self.client.post(
            reverse("treatments:delete-processing-line", args=[self.processing_line.id])
        )

        # Check if the view has returned a redirect to 'treatments:list-processing-line'
        self.assertRedirects(
            response,
            reverse("treatments:list-processing-line"),
            fetch_redirect_response=False,
        )

        self.assertEqual(ProcessingLine.objects.count(), 0)
        self.assertEqual(ProcessingLine.all_objects.count(), 1)

        # Check if the success message has been added
        messages_list = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages_list), 1)
        self.assertEqual(messages_list[0].level, messages.SUCCESS)

    def test_undelete_processing_line(self):
        self.assertEqual(ProcessingLine.objects.count(), 1)
        self.processing_line.delete()
        self.assertEqual(ProcessingLine.objects.count(), 0)

        response = self.client.get(
            reverse(
                "treatments:undelete-processing-line", args=[self.processing_line.id]
            )
        )

        self.assertRedirects(
            response,
            reverse("treatments:list-processing-line"),
            fetch_redirect_response=False,
        )

        self.assertEqual(ProcessingLine.objects.count(), 1)
        self.assertEqual(ProcessingLine.objects.first().name, "Type 1")


class ToggleProcessingLineTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="delete_processingline")
        )
        self.client.force_login(self.user)
        self.processing_line = ProcessingLineFactory.create(name="Type 1")

    def test_start_processing_line(self):
        self.processing_line.running = False
        self.processing_line.save()
        self.assertEqual(self.processing_line.running, False)

        response = self.client.get(
            reverse(
                "treatments:toggle-operation-processing-line",
                args=[self.processing_line.id],
            )
        )

        self.assertRedirects(
            response,
            reverse("treatments:view-processing-line", args=[self.processing_line.id]),
            fetch_redirect_response=False,
        )

        self.processing_line.refresh_from_db()
        self.assertEqual(self.processing_line.running, True)

    def test_stop_processing_line(self):
        self.processing_line.running = True
        self.processing_line.save()
        self.assertEqual(self.processing_line.running, True)

        response = self.client.get(
            reverse(
                "treatments:toggle-operation-processing-line",
                args=[self.processing_line.id],
            )
        )

        self.assertRedirects(
            response,
            reverse("treatments:view-processing-line", args=[self.processing_line.id]),
            fetch_redirect_response=False,
        )

        self.processing_line.refresh_from_db()
        self.assertEqual(self.processing_line.running, False)


class StabilizationMeasureListViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.permission = Permission.objects.get(codename="view_treatmenttype")
        self.user.user_permissions.add(self.permission)
        self.client.force_login(self.user)
        self.treatment_type = TreatmentTypeFactory()

    def test_authenticated_with_permissions(self):
        response = self.client.get(reverse("treatments:list-treatment-type"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "treatments/treatmenttype_list.html")

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(self.permission)
        response = self.client.get(reverse("treatments:list-treatment-type"))
        self.assertEqual(response.status_code, 403)

    def test_anonymous_user(self):
        self.client.logout()
        response = self.client.get(reverse("treatments:list-treatment-type"))
        self.assertEqual(response.status_code, 302)


class StabilizationMeasureCreateView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="add_treatmenttype")
        )
        self.client.force_login(self.user)

    def test_get(self):
        response = self.client.get(reverse("treatments:add-treatment-type"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "treatments/treatmenttype_form.html")

    def test_post(self):
        response = self.client.post(
            reverse("treatments:add-treatment-type"),
            data={
                "name": "Test name",
                "description": "Test desc",
                "icon_class": "bi-exclamation-square",
                "color": "#FA1919",
                "duration": "0 days, 6:00:00",
            },
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("treatments:list-treatment-type"))

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="add_treatmenttype")
        )
        response = self.client.get(reverse("treatments:add-treatment-type"))
        self.assertEqual(response.status_code, 403)

    def test_authenticated_with_permissions(self):
        response = self.client.get(reverse("treatments:add-treatment-type"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "treatments/treatmenttype_form.html")

    def test_not_authenticated(self):
        self.client.logout()
        response = self.client.get(reverse("treatments:add-treatment-type"))
        self.assertEqual(response.status_code, 302)


class TreatmentTypeUpdateViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.permission = Permission.objects.get(codename="change_treatmenttype")
        self.user.user_permissions.add(self.permission)
        self.client.force_login(self.user)
        self.treatment_type = TreatmentTypeFactory()

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="change_treatmenttype")
        )
        response = self.client.get(
            reverse(
                "treatments:change-treatment-type",
                kwargs={"pk": self.treatment_type.pk},
            )
        )
        self.assertEqual(response.status_code, 403)

    def test_authenticated_with_permissions(self):
        response = self.client.get(
            reverse(
                "treatments:change-treatment-type",
                kwargs={"pk": self.treatment_type.pk},
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "treatments/treatmenttype_form.html")

    def test_not_authenticated(self):
        self.client.logout()
        response = self.client.get(
            reverse(
                "treatments:change-treatment-type",
                kwargs={"pk": self.treatment_type.pk},
            )
        )
        self.assertEqual(response.status_code, 302)

    def test_get(self):
        response = self.client.get(
            reverse(
                "treatments:change-treatment-type",
                kwargs={"pk": self.treatment_type.pk},
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "treatments/treatmenttype_form.html")

    def test_post(self):
        new_name = "Updated name"
        response = self.client.post(
            reverse(
                "treatments:change-treatment-type",
                kwargs={"pk": self.treatment_type.pk},
            ),
            data={
                "name": new_name,
                "description": self.treatment_type.description,
                "icon_class": self.treatment_type.icon_class,
                "color": self.treatment_type.color,
                "duration": self.treatment_type.duration,
            },
        )
        self.treatment_type.refresh_from_db()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("treatments:list-treatment-type"))
        self.assertEqual(self.treatment_type.name, new_name)


class TreatmentTypeDeleteViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="delete_treatmenttype")
        )
        self.client.force_login(self.user)
        self.treatment_type = TreatmentTypeFactory.create()

    def test_get(self):
        response = self.client.get(
            reverse(
                "treatments:delete-treatment-type",
                kwargs={"pk": self.treatment_type.pk},
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "treatments/treatmenttype_confirm_delete.html"
        )

    def test_post(self):
        response = self.client.post(
            reverse(
                "treatments:delete-treatment-type",
                kwargs={"pk": self.treatment_type.pk},
            )
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("treatments:list-treatment-type"))
        self.assertEqual(TreatmentType.objects.count(), 0)

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="delete_treatmenttype")
        )
        response = self.client.get(
            reverse(
                "treatments:delete-treatment-type",
                kwargs={"pk": self.treatment_type.pk},
            )
        )
        self.assertEqual(response.status_code, 403)

    def test_authenticated_with_permissions(self):
        response = self.client.get(
            reverse(
                "treatments:delete-treatment-type",
                kwargs={"pk": self.treatment_type.pk},
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "treatments/treatmenttype_confirm_delete.html"
        )

    def test_not_authenticated(self):
        self.client.logout()
        response = self.client.get(
            reverse(
                "treatments:delete-treatment-type",
                kwargs={"pk": self.treatment_type.pk},
            )
        )
        self.assertEqual(response.status_code, 302)


class UndeleteTreatmentTypeTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.treatment_type = TreatmentTypeFactory.create()
        self.treatment_type.delete()

    def test_undelete_treatment_type(self):
        self.assertEqual(TreatmentType.objects.count(), 0)

        response = self.client.get(
            reverse(
                "treatments:undelete-treatment-type",
                args=[self.treatment_type.id],
            )
        )

        # Check if the view has returned a redirect to 'treatments:list-stabilization-measure'
        self.assertRedirects(
            response,
            reverse("treatments:list-treatment-type"),
            fetch_redirect_response=False,
        )

        self.assertEqual(TreatmentType.objects.count(), 1)
        self.assertEqual(TreatmentType.objects.first().name, self.treatment_type.name)
