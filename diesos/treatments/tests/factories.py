import datetime

from factory import Faker, SubFactory
from factory.django import DjangoModelFactory

from diesos.locations.tests.factories import LocationFactory
from diesos.treatments.models import ProcessingLine, TreatmentType


class TreatmentTypeFactory(DjangoModelFactory):
    name = Faker("text", max_nb_chars=20)
    description = Faker("paragraph", nb_sentences=2, variable_nb_sentences=True)
    icon_class = Faker("text", max_nb_chars=255)
    color = Faker("hex_color")
    duration = datetime.timedelta(hours=1)

    class Meta:
        model = TreatmentType
        django_get_or_create = ["name"]


class ProcessingLineFactory(DjangoModelFactory):
    name = Faker("pystr")
    treatment_type = SubFactory(TreatmentTypeFactory)
    capacity = Faker("pyint")
    location = SubFactory(LocationFactory)

    class Meta:
        model = ProcessingLine
