from django.test import TestCase

from diesos.treatments.tests.factories import (
    ProcessingLineFactory,
    TreatmentTypeFactory,
)


class TestProcessingLineModel(TestCase):
    def setUp(self):
        self.processing_line = ProcessingLineFactory.create()

    def test_str(self):
        self.assertEqual(str(self.processing_line), f"{self.processing_line.name}")

    def test_get_absolute_url(self):
        assert (
            self.processing_line.get_absolute_url()
            == f"/treatments/processing-line/{self.processing_line.pk}"
        )

    def test_start(self):
        self.processing_line.running = False
        self.processing_line.save()
        self.assertEqual(self.processing_line.running, False)

        self.processing_line.start()
        self.processing_line.refresh_from_db()

        self.assertEqual(self.processing_line.running, True)

    def test_stop(self):
        self.processing_line.running = True
        self.processing_line.save()
        self.assertEqual(self.processing_line.running, True)

        self.processing_line.stop()
        self.processing_line.refresh_from_db()

        self.assertEqual(self.processing_line.running, False)


class TestTreatmentTypeModel(TestCase):
    def setUp(self):
        self.treatment_type = TreatmentTypeFactory.create()

    def test_str(self):
        self.assertEqual(str(self.treatment_type), f"{self.treatment_type.name}")

    # def test_get_absolute_url(self):
    #    assert (
    #        self.treatment_type.get_absolute_url()
    #       == f"/treatments/stabilization-measure/{self.treatment_type.pk}"
    #    )
