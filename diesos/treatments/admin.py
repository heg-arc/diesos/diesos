from django.contrib import admin

from .models import ProcessingLine, TreatmentType


class TreatmentTypeAdmin(admin.ModelAdmin):
    model = ProcessingLine
    list_display = ("name", "description")


class ProcessingLineAdmin(admin.ModelAdmin):
    model = ProcessingLine
    list_display = ("name", "treatment_type", "capacity", "running", "location")


admin.site.register(ProcessingLine, ProcessingLineAdmin)
admin.site.register(TreatmentType, TreatmentTypeAdmin)
