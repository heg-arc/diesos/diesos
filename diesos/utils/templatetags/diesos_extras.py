import datetime

from django import template
from django.utils.translation import gettext_lazy as _
from libgravatar import Gravatar

register = template.Library()


@register.filter(name="gravatar")
def gravatar_url(value):
    g = Gravatar(value)
    return g.get_image(default="retro")


@register.filter
def format_duration(value):
    if isinstance(value, datetime.timedelta):
        total_seconds = int(value.total_seconds())
        hours = total_seconds // 3600
        minutes = (total_seconds % 3600) // 60
        if hours == 0:
            return f"{minutes}{_('min')}"
        elif minutes == 0:
            return f"{hours}{_('h')}"
        else:
            return f"{hours}{_('h')} {minutes}{_('min')}"
    return value
