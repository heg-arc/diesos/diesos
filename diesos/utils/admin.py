from django.contrib import admin

from .models import Hazard, UniqueShortId


class HazardAdmin(admin.ModelAdmin):
    model = Hazard


class UniqueShortIdAdmin(admin.ModelAdmin):
    model = UniqueShortId
    list_display = ["short_id", "content_type"]
    search_fields = ["short_id"]
    list_filter = ["content_type"]


admin.site.register(Hazard, HazardAdmin)
admin.site.register(UniqueShortId, UniqueShortIdAdmin)
