import shortuuid
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import gettext_lazy as _

"""
STEPS TO USE SHORTID GENERATOR
    - add this import :
        from diesos.utils.models import UniqueShortId

    - add a short_id to your model :
        short_id = models.CharField(
            max_length=5,
            unique=True,
            null=True,
            verbose_name=_("short id")
        )

    - Redefine the save() method from the class that need a short id:
      (Example code to add in model)
                    def save(self, *args, **kwargs):
                    # Generate and store unique short id before saving
                    if not self.short_id:
                        unique_short_id = UniqueShortId.create_unique_short_id_instance(
                            content_object=self
                        )
                        self.short_id = unique_short_id.short_id
                    super().save(*args, **kwargs)
"""


class ShortIdGenerator:
    # Generate a short id
    @staticmethod
    def generate(length=5):
        """
        Génère un identifiant court.
        """
        short_id = shortuuid.ShortUUID().random(length=length)
        return short_id.upper()

    @staticmethod
    def generate_unique_short_id(length=5):
        """
        Generate a short id and check if it already exists.
        """
        unique = False
        while not unique:
            short_id = ShortIdGenerator.generate(length=length)
            if not UniqueShortId.objects.filter(short_id=short_id).exists():
                unique = True
        return short_id


# Store generated short id
class UniqueShortId(models.Model):
    short_id = models.CharField(max_length=5, unique=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True)
    object_id = models.PositiveIntegerField(null=True)
    content_object = GenericForeignKey("content_type", "object_id")

    def __str__(self):
        return self.short_id

    def create_unique_short_id_instance(content_object, length=5):
        """
        Generate a unique short id and create a UniqueShortId instance for a given content_object.
        """
        unique_short_id = ShortIdGenerator.generate_unique_short_id(length=length)
        instance = UniqueShortId(
            short_id=unique_short_id, content_object=content_object
        )
        instance.save()
        return instance


class TestModel(models.Model):
    name = models.CharField(max_length=255)
    short_id = models.CharField(
        max_length=5, unique=True, null=True, verbose_name=_("short id")
    )

    def save(self, *args, **kwargs):
        # Generate and store unique short id before saving
        if not self.short_id:
            unique_short_id = UniqueShortId.create_unique_short_id_instance(
                content_object=self
            )
            self.short_id = unique_short_id.short_id
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Hazard(models.Model):
    name = models.CharField(verbose_name=_("name"), max_length=50)
    acronym = models.CharField(verbose_name=_("acronym"), max_length=1)
    icon_class = models.CharField(
        verbose_name=_("icon class"), max_length=50, null=True, blank=True
    )
    warning = models.TextField(verbose_name=_("warning message"), null=True, blank=True)

    class Meta:
        verbose_name = _("hazard")
        verbose_name_plural = _("hazards")
        ordering = ["name"]

    def __str__(self):
        return f"{self.acronym} - {self.name}"
