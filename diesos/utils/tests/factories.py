from factory import Faker
from factory.django import DjangoModelFactory

from diesos.utils.models import Hazard


class HazardFactory(DjangoModelFactory):
    name = Faker("word")
    acronym = Faker("random_letter")
    icon_class = Faker("word")
    warning = Faker("paragraph")

    class Meta:
        model = Hazard
