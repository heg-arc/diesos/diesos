from django.contrib.contenttypes.models import ContentType
from django.test import TestCase

from ..models import Hazard, ShortIdGenerator, TestModel, UniqueShortId


class TestUniqueShortIdGenerator(TestCase):
    def setUp(self):
        self.test_model_1 = TestModel.objects.create(name="Test Model 1")
        self.test_model_2 = TestModel.objects.create(name="Test Model 2")

    def test_short_id_created_on_create(self):
        self.assertIsNotNone(self.test_model_1.short_id)

    def test_short_id_created_on_update(self):
        initial_short_id = self.test_model_1.short_id
        self.test_model_1.name = "New Test Model Name"
        self.test_model_1.save()
        self.assertEqual(self.test_model_1.short_id, initial_short_id)

    def test_unique_short_id_generated(self):
        self.assertNotEqual(self.test_model_1.short_id, self.test_model_2.short_id)

    def test_short_id_has_correct_length(self):
        self.assertEqual(len(self.test_model_1.short_id), 5)


class TestUniqueShortId(TestCase):
    def setUp(self):
        self.test_object = TestModel.objects.create(name="Test Object")
        self.content_type = ContentType.objects.get_for_model(self.test_object)

    def test_create_unique_short_id_instance(self):
        # Create a UniqueShortId instance for the test object
        instance = UniqueShortId.create_unique_short_id_instance(
            content_object=self.test_object
        )

        # Check that the short id is of the correct length
        self.assertEqual(len(instance.short_id), 5)

        # Check that the content type, object id and content object are set correctly
        self.assertEqual(instance.content_type, self.content_type)
        self.assertEqual(instance.object_id, self.test_object.id)
        self.assertEqual(instance.content_object, self.test_object)


class TestShortIdGenerator(TestCase):
    def test_generate_short_id(self):
        # Generate a short id and check that it has the correct length
        short_id = ShortIdGenerator.generate()
        self.assertEqual(len(short_id), 5)

    def test_generate_unique_short_id(self):
        # Generate 1000 unique short ids and check that they are all of the correct length and unique
        genRange = 1000
        for _ in range(genRange):
            short_id = ShortIdGenerator.generate_unique_short_id()
            self.assertEqual(len(short_id), 5)
            unique_short_id = UniqueShortId(short_id=short_id)
            unique_short_id.save()
            count = UniqueShortId.objects.count()
        self.assertEqual(count, genRange)


class TestHazardModel(TestCase):
    fixtures = ["hazards.json"]

    def test_hazard_str(self):
        hazard = Hazard.objects.get(pk=1)
        assert str(hazard) == f"{hazard.acronym} - {hazard.name}"
