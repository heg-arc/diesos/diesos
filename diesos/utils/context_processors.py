from django.conf import settings


def tag_settings(request):
    try:
        settings_module = settings.SETTINGS_MODULE.split(".")[-1]
    except AttributeError:
        settings_module = None
    return {
        "GIT_REVISION": settings.GIT_REVISION,
        "SETTINGS_MODULE": settings_module,
    }
