from django.urls import path

from diesos.artefacts import views

app_name = "artefacts"

urlpatterns = [
    path("", views.ArtefactListView.as_view(), name="list"),
    path("add", views.ArtefactCreateView.as_view(), name="add"),
    path("<int:pk>/change", views.ArtefactUpdateView.as_view(), name="change"),
    path("<int:pk>/delete", views.ArtefactDeleteView.as_view(), name="delete"),
    path("<int:pk>", views.ArtefactDetailView.as_view(), name="view"),
    path("<int:pk>/scan", views.ArtefactScanView.as_view(), name="scan"),
    path(
        "<int:pk>/scan/finish",
        views.finish_stabilization_measures,
        name="finish-stabilization",
    ),
    path(
        "<int:pk>/scan/start/<int:processing_line_id>",
        views.start_stabilization_measures,
        name="start-stabilization",
    ),
    path("type", views.ArtefactTypeListView.as_view(), name="list-type"),
    path("type/add", views.ArtefactTypeCreateView.as_view(), name="add-type"),
    path(
        "type/<int:pk>/change",
        views.ArtefactTypeUpdateView.as_view(),
        name="change-type",
    ),
    path(
        "type/<int:pk>/delete",
        views.ArtefactTypeDeleteView.as_view(),
        name="delete-type",
    ),
    path(
        "type/<int:pk>/undelete",
        views.undelete_artefact_type,
        name="undelete-type",
    ),
    path("type/save-type-ordering", views.save_new_ordering, name="save-type-ordering"),
    path(
        "<int:pk>/take-picture",
        views.ArtefactTakePicture.as_view(),
        name="take-picture",
    ),
    path("damage/group", views.DamageGroupListView.as_view(), name="list-damage-group"),
    path(
        "damage/group/add",
        views.DamageGroupCreateView.as_view(),
        name="add-damage-group",
    ),
    path(
        "damage/group/<int:pk>/delete",
        views.DamageGroupDeleteView.as_view(),
        name="delete-damage-group",
    ),
    path(
        "damage/group/<int:pk>/undelete",
        views.undelete_damage_group,
        name="undelete-damage-group",
    ),
    path(
        "damage/group/<int:pk>/change",
        views.DamageGroupUpdateView.as_view(),
        name="change-damage-group",
    ),
    path("damage/type", views.TypeOfDamageListView.as_view(), name="list-damage-type"),
    path(
        "damage/type/add",
        views.TypeOfDamageCreateView.as_view(),
        name="add-damage-type",
    ),
    path(
        "damage/type/<int:pk>/delete",
        views.TypeOfDamageDeleteView.as_view(),
        name="delete-damage-type",
    ),
    path(
        "damage/type/<int:pk>/undelete",
        views.undelete_type_of_damage,
        name="undelete-damage-type",
    ),
    path(
        "damage/type/<int:pk>/change",
        views.TypeOfDamageUpdateView.as_view(),
        name="change-damage-type",
    ),
    path(
        "<int:pk>/damage-assessment",
        views.DamageAssessmentUpdateView.as_view(),
        name="change-damage",
    ),
    path("priority", views.PriorityLevelListView.as_view(), name="priority-level"),
    path(
        "priority/<int:pk>/change",
        views.PriorityLevelUpdateView.as_view(),
        name="change-priority",
    ),
    path(
        "<int:pk>/stabilization",
        views.StabilizationMeasureUpdateView.as_view(),
        name="change-stabilization-measure",
    ),
    path(
        "<int:pk>/stabilization/delete/<int:treatment_type_id>",
        views.delete_treatment_type,
        name="delete-treatment-type",
    ),
    path(
        "<int:pk>/stabilization/save-treatment-type-ordering",
        views.save_treatment_type_ordering,
        name="save-treatment-type-ordering",
    ),
    path(
        "existing-collection/import", views.UploadCsvView.as_view(), name="import-file"
    ),
    path(
        "existing-collection/headers/edit",
        views.FileHeaderEditView.as_view(),
        name="header-edit-existing-collection",
    ),
    path(
        "existing-collection/map",
        views.CsvMapperView.as_view(),
        name="map-existing-collection",
    ),
    path(
        "existing-collection/map/complex",
        views.ComplexMapperView.as_view(),
        name="map-complex-existing-collection",
    ),
    path(
        "existing-collection/<int:pk>",
        views.ExistingCollectionDetailView.as_view(),
        name="view-existing-collection",
    ),
    path(
        "existing-collection",
        views.ExistingCollectionListView.as_view(),
        name="list-existing-collection",
    ),
    path(
        "existing-collection/<int:pk>/delete",
        views.ExistingCollectionDeleteView.as_view(),
        name="delete-existing-collection",
    ),
    path(
        "existing-collection/add",
        views.create_existing_collection,
        name="add-existing-collection",
    ),
]
