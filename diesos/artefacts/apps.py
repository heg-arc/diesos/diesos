from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ArtefactsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "diesos.artefacts"
    verbose_name = _("Artefacts")

    def ready(self):
        from actstream import registry

        registry.register(
            self.get_model("Artefact"), self.get_model("ArtefactImageAsset")
        )
