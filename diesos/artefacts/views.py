import csv
import io
import logging
import os
import re
from datetime import datetime

import chardet
import clevercsv
from actstream import action
from django.apps import apps
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    FormView,
    ListView,
    UpdateView,
)

from diesos.treatments.models import ProcessingLine, TreatmentType

from .forms import (
    ArtefactLevelOfDamageForm,
    CsvForm,
    DamageAssessmentForm,
    DynamicComplexMapForm,
    ExistingCollectionMapForm,
    FileHeaderForm,
    OrderingForm,
    PriorityLevelForm,
    StabilizationMeasureForm,
)
from .models import (
    Artefact,
    ArtefactImageAsset,
    ArtefactType,
    DamageAssessment,
    DamageGroup,
    ExistingCollection,
    PriorityLevel,
    StabilizationMeasure,
    TypeOfDamage,
)

logger = logging.getLogger(__name__)


class ArtefactListView(PermissionRequiredMixin, ListView):
    permission_required = "artefacts.view_artefact"
    model = Artefact

    # TODO: Add search, filter, pagination, etc.


class ArtefactDetailView(PermissionRequiredMixin, DetailView):
    permission_required = "artefacts.view_artefact"
    model = Artefact

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        action.send(self.request.user, verb=_("viewed"), action_object=self.object)
        context["artefact"] = self.object
        context["added_damages"] = self.object.damages.all()
        context["deleted_damages"] = DamageAssessment.objects.deleted_only().filter(
            artefact=self.object
        )
        return context


class ArtefactScanView(PermissionRequiredMixin, DetailView):
    permission_required = "artefacts.view_artefact"
    template_name = "artefacts/artefact_scan.html"
    model = Artefact

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        stabilization_measure = self.object.stabilization_measures.exclude(
            status=StabilizationMeasure.FINISH
        ).first()
        processing_line = self.request.processing_line

        context["measure"] = stabilization_measure
        context["processing_line"] = processing_line

        return context

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        # Check if artefact as Stabilization Measures
        if not self.object.stabilization_measures.exists():
            return redirect("artefacts:view", pk=self.object.pk)

        # Check if all Stabilization Measures are finished and User's processing line has the correct treatment_type
        processing_line = self.request.processing_line
        stabilization_measures = self.object.stabilization_measures.all()
        unfinished_measures = any(
            measure.status != measure.FINISH
            and processing_line.treatment_type == measure.treatment_type
            for measure in stabilization_measures
        )

        if not unfinished_measures:
            return redirect("artefacts:view", pk=self.object.pk)

        return super().get(request, *args, **kwargs)


def finish_stabilization_measures(request, pk):
    measure = StabilizationMeasure.objects.get(pk=pk)

    measure.status = measure.FINISH
    measure.save()

    messages.add_message(
        request,
        messages.SUCCESS,
        _(f"<strong>{measure.treatment_type.name}</strong> treatment completed."),
    )

    return redirect(measure.artefact.get_absolute_url())


def start_stabilization_measures(request, pk, processing_line_id):
    measure = StabilizationMeasure.objects.get(pk=pk)
    processing_line = ProcessingLine.objects.get(pk=processing_line_id)

    measure.status = measure.IN_TREATMENT
    measure.processing_line = processing_line
    measure.save()

    messages.add_message(
        request,
        messages.SUCCESS,
        _(f"<strong>{measure.treatment_type.name}</strong> treatment started."),
    )

    return redirect(measure.artefact.get_absolute_url())


class ArtefactCreateView(PermissionRequiredMixin, CreateView):
    permission_required = "artefacts.add_artefact"
    model = Artefact
    fields = [
        "parent",
        "rescue_number",
        "name",
        "artefact_type",
        "inventory_number",
        "priority",
        "hazard",
        "description",
        "existing_collection",
    ]

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields["description"].widget.attrs["rows"] = 7
        form.fields["parent"].queryset = Artefact.objects.filter(parent=None)
        parent_id = self.request.GET.get("parent_id")
        if parent_id:
            form.fields["parent"].disabled = True
        form.fields["existing_collection"].widget.attrs["hidden"] = "hidden"
        return form

    def form_valid(self, form):
        parent = form.cleaned_data["parent"]
        composed_object = self.request.GET.get("option")

        if parent:
            if composed_object:
                form.instance.composed_object = composed_object
                parent.composed_object = composed_object

                # Redefine composed_object for children that have been linked to parent before using 'Create Part'
                children = Artefact.objects.filter(parent=parent)
                for child in children:
                    child.composed_object = parent.composed_object
                    child.save()
            else:
                form.instance.composed_object = parent.composed_object

            parent.save()

        response = super().form_valid(form)
        action.send(self.request.user, verb=_("created"), action_object=self.object)
        return response

    def get_initial(self):
        initial = super().get_initial()
        parent_id = self.request.GET.get("parent_id")
        if parent_id:
            initial["parent"] = parent_id
        rescue_number = self.request.GET.get("rescue_number")
        if rescue_number:
            initial["rescue_number"] = rescue_number
            # Include the ID of existing_collection
        existing_collection_id = self.request.GET.get("existing_collection")
        if existing_collection_id:
            initial["existing_collection"] = existing_collection_id
        for field_name in self.fields:
            request_value = self.request.GET.get(field_name)
            if request_value:
                initial[field_name] = request_value

        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["parent_id"] = self.request.GET.get("parent_id")
        return context


class ArtefactTakePicture(PermissionRequiredMixin, CreateView):
    permission_required = "artefacts.view_artefact"
    model = ArtefactImageAsset
    fields = ["image", "description", "default"]

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields["description"].widget.attrs["rows"] = 2
        form.fields["image"].widget.attrs["capture"] = "environment"
        form.fields["image"].widget.attrs["accept"] = "image/*"
        form.fields["image"].widget.attrs["hidden"] = "hidden"
        form.fields["image"].label = mark_safe(
            '<span class="btn btn-outline-primary btn-lg"><i class="bi bi-camera me-2"></i>'
            + _("Take picture")
            + "</span>"
        )
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["artefact"] = Artefact.objects.get(pk=self.kwargs["pk"])
        return context

    def form_valid(self, form):
        artefact = Artefact.objects.get(pk=self.kwargs["pk"])
        form.instance.artefact = artefact
        messages.add_message(
            self.request, messages.SUCCESS, _("The image was added to the artefact.")
        )
        response = super().form_valid(form)
        action.send(
            self.request.user,
            verb=_("photographed"),
            action_object=artefact,
            target=self.object,
        )
        return response


class ArtefactDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = "artefacts.delete_artefact"
    model = Artefact
    success_url = reverse_lazy("artefacts:list")


class ArtefactUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = "artefacts.change_artefact"
    model = Artefact
    fields = [
        "rescue_number",
        "name",
        "artefact_type",
        "inventory_number",
        "damage_level",
        "priority",
        "description",
    ]

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields["description"].widget.attrs["rows"] = 7
        return form

    def get_succes_url(self):
        return reverse_lazy("artefacts:list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = _("update")
        return context

    def form_valid(self, form):
        response = super().form_valid(form)
        action.send(self.request.user, verb=_("updated"), action_object=self.object)
        return response


class ArtefactTypeCreateView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    permission_required = "artefacts.add_artefacttype"
    model = ArtefactType
    fields = ["type_name", "description"]
    success_url = reverse_lazy("artefacts:list-type")
    success_message = _(
        "The  type <strong>%(type_name)s</strong> has been created and placed at the bottom of the list."
    )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = _("create")
        return context


class ArtefactTypeListView(PermissionRequiredMixin, ListView):
    permission_required = "artefacts.view_artefacttype"
    model = ArtefactType


class ArtefactTypeUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    permission_required = "artefacts.change_artefacttype"
    model = ArtefactType
    fields = ["type_name", "description"]
    success_url = reverse_lazy("artefacts:list-type")
    success_message = _("The  type <strong>%(type_name)s</strong> has been updated.")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = _("update")
        return context


class ArtefactTypeDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = "artefacts.delete_artefacttype"
    model = ArtefactType

    def get_success_url(self):
        return reverse("artefacts:list-type")

    def form_valid(self, form):
        response = super().form_valid(form)
        obj = self.object
        url = reverse("artefacts:undelete-type", kwargs={"pk": obj.pk})
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                f'The type <strong>{obj.type_name}</strong> has been \
                    deleted. If it was a mistake, you can <a href="{url}">restore it</a>.'
            ),
        )
        return response


@permission_required("artefacts.change_artefacttype")
def save_new_ordering(request):
    form = OrderingForm(request.POST)

    if form.is_valid():
        ordered_ids = form.cleaned_data["ordering"].split(",")

        with transaction.atomic():
            current_order = 1
            for id in ordered_ids:
                type = ArtefactType.objects.get(pk=id)
                type.priority_order = current_order
                type.save()
                current_order += 1

        messages.add_message(
            request, messages.SUCCESS, _("The new order has been saved.")
        )
    else:
        messages.add_message(
            request, messages.ERROR, _("The new order could not be saved.")
        )

    return redirect("artefacts:list-type")


@permission_required("artefacts.delete_artefacttype")
def undelete_artefact_type(request, pk):
    artefact_type = ArtefactType.all_objects.get(pk=pk)
    artefact_type.undelete()
    messages.add_message(
        request,
        messages.SUCCESS,
        _(
            f"The type <strong>{artefact_type.type_name}</strong> has been \
                restored."
        ),
    )
    return redirect("artefacts:list-type")


class DamageGroupListView(PermissionRequiredMixin, ListView):
    permission_required = "artefacts.view_damagegroup"
    model = DamageGroup


class DamageGroupUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    permission_required = "artefacts.change_damagegroup"
    model = DamageGroup
    fields = [
        "name",
    ]
    success_url = reverse_lazy("artefacts:list-damage-group")
    success_message = _("The type <strong>%(name)s</strong> has been updated.")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = _("update")
        return context


class DamageGroupCreateView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    permission_required = "artefacts.add_damagegroup"
    model = DamageGroup
    fields = [
        "name",
    ]
    success_url = reverse_lazy("artefacts:list-damage-group")
    success_message = _("The type <strong>%(name)s</strong> has been created.")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = _("create")
        return context


class DamageGroupDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = "artefacts.delete_damagegroup"
    model = DamageGroup

    def get_success_url(self):
        return reverse("artefacts:list-damage-group")

    def form_valid(self, form):
        response = super().form_valid(form)
        url = reverse("artefacts:undelete-damage-group", kwargs={"pk": self.object.pk})
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                f'The type <strong>{self.object.name}</strong> has been \
                    deleted. If it was a mistake, you can <a href="{url}">restore it</a>.'
            ),
        )
        return response


@permission_required("artefacts.delete_damagegroup")
def undelete_damage_group(request, pk):
    damage_group = DamageGroup.all_objects.get(pk=pk)
    damage_group.undelete()
    messages.add_message(
        request,
        messages.SUCCESS,
        _(
            f"The type <strong>{damage_group.name}</strong> has been \
                restored."
        ),
    )
    return redirect("artefacts:list-damage-group")


class TypeOfDamageListView(PermissionRequiredMixin, ListView):
    permission_required = "artefacts.view_typeofdamage"
    model = TypeOfDamage


class TypeOfDamageUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    permission_required = "artefacts.change_typeofdamage"
    model = TypeOfDamage
    fields = [
        "name",
        "group",
        "description",
    ]
    success_url = reverse_lazy("artefacts:list-damage-type")
    success_message = _("The type <strong>%(name)s</strong> has been updated.")

    # TODO: Add utils class for icon selection.

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = _("update")
        return context


class TypeOfDamageCreateView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    permission_required = "artefacts.add_typeofdamage"
    model = TypeOfDamage
    fields = [
        "name",
        "group",
        "description",
    ]
    success_url = reverse_lazy("artefacts:list-damage-type")
    success_message = _("The type <strong>%(name)s</strong> has been created.")

    # TODO: Add utils class for icon selection.

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = _("create")
        return context


class TypeOfDamageDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = "artefacts.delete_typeofdamage"
    model = TypeOfDamage

    def get_success_url(self):
        return reverse("artefacts:list-damage-type")

    def form_valid(self, form):
        response = super().form_valid(form)
        url = reverse("artefacts:undelete-damage-type", kwargs={"pk": self.object.pk})
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                f'The type <strong>{self.object.name}</strong> has been \
                    deleted. If it was a mistake, you can <a href="{url}">restore it</a>.'
            ),
        )
        return response


@permission_required("artefacts.delete_typeofdamage")
def undelete_type_of_damage(request, pk):
    type_of_damage = TypeOfDamage.all_objects.get(pk=pk)
    type_of_damage.undelete()
    messages.add_message(
        request,
        messages.SUCCESS,
        _(
            f"The type <strong>{type_of_damage.name}</strong> has been \
                restored."
        ),
    )
    return redirect("artefacts:list-damage-type")


class DamageAssessmentUpdateView(PermissionRequiredMixin, FormView):
    permission_required = "artefacts.change_damageassessment"
    model = DamageAssessment
    form_class = DamageAssessmentForm
    artefact_form_class = ArtefactLevelOfDamageForm
    template_name = "artefacts/damageassessment_form.html"

    def get(self, request, *args, **kwargs):
        self.artefact = get_object_or_404(Artefact, pk=self.kwargs["pk"])
        self.initial_damage_types = self.artefact.damages.values_list(
            "damage_type", flat=True
        )
        form = self.form_class(initial={"damage_types": self.initial_damage_types})
        artefact_form = self.artefact_form_class(instance=self.artefact)
        return self.render_to_response(
            self.get_context_data(form=form, artefact_form=artefact_form)
        )

    def form_valid(self, form):
        self.artefact = get_object_or_404(Artefact, pk=self.kwargs["pk"])
        artefact_form = self.artefact_form_class(
            self.request.POST, instance=self.artefact
        )
        self.initial_damage_types = self.artefact.damages.values_list(
            "damage_type", flat=True
        )
        if artefact_form.is_valid():
            artefact_form.save()

        # Type de dommages cochés
        damage_types = form.cleaned_data.get("damage_types")

        # Vérifier si un nouveau type de dommage a été ajouté
        if self.initial_damage_types is None:
            new_damage_types = set(damage_types)
        else:
            new_damage_types = set(damage_types) - set(self.initial_damage_types)

        # Si un nouveau type de dommage est ajouté, vérifier si le niveau de dommage n'est pas nul
        if new_damage_types and self.artefact.damage_level is None:
            messages.add_message(
                self.request,
                messages.ERROR,
                _("A level of damage is required."),
            )
            return self.render_to_response(
                self.get_context_data(form=form, artefact_form=artefact_form)
            )

        # Suppression des types de dommages décochés
        unchecked_damage_assessments = self.artefact.damages.exclude(
            damage_type__in=damage_types
        )
        for unchecked_damage_assessment in unchecked_damage_assessments:
            unchecked_damage_assessment.user_remove = self.request.user
            unchecked_damage_assessment.timestamp_remove = timezone.now()
            unchecked_damage_assessment.save()
        unchecked_damage_assessments.delete()

        # Ajout et mise à jour des types de dommages cochés
        for damage_type in damage_types:
            damage_assessment, created = DamageAssessment.objects.get_or_create(
                artefact=self.artefact, damage_type=damage_type
            )
            if created:
                damage_assessment.user_add = self.request.user
                damage_assessment.timestamp_add = timezone.now()
                damage_assessment.save()
            else:
                # Réinitialisation des champs user_remove et timestamp_remove
                if (
                    damage_assessment.user_remove is not None
                    or damage_assessment.timestamp_remove is not None
                ):
                    damage_assessment.user_remove = None
                    damage_assessment.timestamp_remove = None
                    damage_assessment.save()

        action.send(
            self.request.user, verb=_("damage updated"), action_object=self.artefact
        )
        response = HttpResponseRedirect(
            reverse("artefacts:view", kwargs={"pk": self.artefact.pk})
        )
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                f"The damage assessment for artefact <strong>{self.artefact.rescue_number} / {self.artefact.name}"
                f"</strong> has been updated."
            ),
        )
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["artefact"] = self.artefact
        context["artefact_form"] = kwargs.get("artefact_form")
        context["added_damages"] = self.artefact.damages.all()
        context["deleted_damages"] = DamageAssessment.objects.deleted_only().filter(
            artefact=self.artefact
        )
        return context


class PriorityLevelListView(PermissionRequiredMixin, ListView):
    permission_required = "artefacts.view_prioritylevel"
    model = PriorityLevel
    fields = ["label", "color"]
    ordering = ["priority_order"]


class PriorityLevelUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = "artefacts.change_prioritylevel"
    model = PriorityLevel
    form_class = PriorityLevelForm
    success_url = reverse_lazy("artefacts:priority-level")
    success_message = _("The  type <strong>%(label)s</strong> has been updated.")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = _("update")
        return context


class StabilizationMeasureUpdateView(PermissionRequiredMixin, FormView):
    permission_required = "artefacts.change_stabilizationmeasure"
    model = StabilizationMeasure
    form_class = StabilizationMeasureForm
    template_name = "artefacts/stabilizationmeasure_form.html"

    def get(self, request, *args, **kwargs):
        self.artefact = get_object_or_404(Artefact, pk=self.kwargs["pk"])
        self.initial_treatment_types = self.artefact.stabilization_measures.values_list(
            "treatment_type", flat=True
        )
        form = self.form_class()
        form.fields["treatment_type"].queryset = TreatmentType.objects.exclude(
            id__in=self.initial_treatment_types
        )
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        self.artefact = get_object_or_404(Artefact, pk=self.kwargs["pk"])
        self.initial_treatment_types = self.artefact.stabilization_measures.values_list(
            "treatment_type", flat=True
        )
        form = self.form_class(request.POST)
        form.fields["treatment_type"].queryset = TreatmentType.objects.exclude(
            id__in=self.initial_treatment_types
        )
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_invalid(self, form):
        return super().form_invalid(form)

    def form_valid(self, form):
        self.artefact = get_object_or_404(Artefact, pk=self.kwargs["pk"])
        self.initial_treatment_types = self.artefact.stabilization_measures.values_list(
            "treatment_type", flat=True
        )
        treatment_type = form.cleaned_data.get("treatment_type")
        if treatment_type:
            StabilizationMeasure.objects.create(
                artefact=self.artefact,
                treatment_type=treatment_type,
                user_add=self.request.user,
                timestamp_add=timezone.now(),
            )

            messages.add_message(
                self.request,
                messages.SUCCESS,
                _(
                    f"The treatment <strong>{treatment_type.name}</strong> has been added."
                ),
            )
        else:
            messages.add_message(
                self.request,
                messages.ERROR,
                _("A treatment must be selected."),
            )

        return HttpResponseRedirect(
            reverse(
                "artefacts:change-stabilization-measure",
                kwargs={"pk": self.artefact.pk},
            )
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["artefact"] = self.artefact
        context[
            "added_stabilization_measures"
        ] = self.artefact.stabilization_measures.all()
        return context


@permission_required("artefacts.change_stabilizationmeasure")
def save_treatment_type_ordering(request, pk):
    form = OrderingForm(request.POST)
    if form.is_valid():
        ordered_ids = form.cleaned_data["ordering"].split(",")

        with transaction.atomic():
            current_order = 1
            for id in ordered_ids:
                stabilization_measure = StabilizationMeasure.objects.get(
                    treatment_type=id, artefact=pk
                )
                stabilization_measure.priority_order = current_order
                stabilization_measure.save()
                current_order += 1

        messages.add_message(
            request, messages.SUCCESS, _("The new order has been saved.")
        )
    else:
        messages.add_message(
            request, messages.ERROR, _("The new order could not be saved.")
        )
    if request.GET.get("in_place", None):
        return redirect("artefacts:view", pk=pk)
    else:
        return redirect("artefacts:change-stabilization-measure", pk=pk)


@permission_required("artefacts.delete_stabilizationmeasure")
def delete_treatment_type(request, pk, treatment_type_id):
    stabilization_measure = get_object_or_404(
        StabilizationMeasure, artefact__id=pk, treatment_type__id=treatment_type_id
    )
    stabilization_measure.user_remove = request.user
    stabilization_measure.timestamp_remove = timezone.now()
    stabilization_measure.save()
    treatment_type_name = stabilization_measure.treatment_type.name
    stabilization_measure.delete()

    remaining_measures = StabilizationMeasure.objects.filter(artefact=pk).order_by(
        "priority_order"
    )
    current_order = 1
    for measure in remaining_measures:
        measure.priority_order = current_order
        measure.save()
        current_order += 1

    messages.add_message(
        request,
        messages.SUCCESS,
        _(f"The treatment <strong>{treatment_type_name}</strong> has been deleted."),
    )

    if request.GET.get("in_place", None):
        return HttpResponseRedirect(reverse("artefacts:view", kwargs={"pk": pk}))
    else:
        return HttpResponseRedirect(
            reverse("artefacts:change-stabilization-measure", kwargs={"pk": pk})
        )


class UploadCsvView(PermissionRequiredMixin, FormView):
    permission_required = "artefacts.add_existingcollection"
    template_name = "artefacts/csv_form.html"
    form_class = CsvForm
    success_url = reverse_lazy("artefacts:header-edit-existing-collection")

    # Save the file only with valid lines
    def save_csv_file(self, original_filename, encoding, valid_lines):
        # Format name
        filename, extension = os.path.splitext(original_filename)
        timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
        new_filename = f"{filename}_{timestamp}{extension}"
        file_path = os.path.join(settings.MEDIA_ROOT, new_filename)

        with open(file_path, "w", encoding=encoding) as f:
            # write valid_lines to the file
            f.write("\n".join(valid_lines))

        # Save session
        self.request.session["csv_file_name"] = original_filename
        self.request.session["csv_file_path"] = file_path

    def csv_format(self, delimiter, row):
        # Format a row as a CSV string.
        csv_string = io.StringIO()
        writer = csv.writer(csv_string, delimiter=delimiter, quoting=csv.QUOTE_MINIMAL)
        writer.writerow(row)
        return csv_string.getvalue().strip()

    def form_valid(self, form):
        # Get uploaded file
        uploaded_file = form.cleaned_data["file_csv"]
        original_filename = uploaded_file.name  # Save the original filename
        try:
            # Detect encoging
            rawdata = uploaded_file.read()
            result = chardet.detect(rawdata)
            encoding = result["encoding"]

            # Read with correct encoding
            csv_file = io.StringIO(rawdata.decode(encoding))

            # Get dialect / ex: SimpleDialect(';', '"', '') as (delimiter,quotechar,escapechar)
            try:
                dialect = clevercsv.Sniffer().sniff(csv_file.read(1024))
            except clevercsv.Error:
                dialect = (
                    clevercsv.unix_dialect
                )  # Provide a default dialect if detection fails
            csv_file.seek(0)

            # Parse CSV
            reader = clevercsv.reader(csv_file, dialect=dialect)

            headers = []
            valid_lines = []
            ignored_lines = 0
            for i, row in enumerate(reader):
                if i == 0:
                    headers = [
                        {"index": i, "name": col if col else "Unnamed"}
                        for i, col in enumerate(row)
                    ]
                    valid_lines.append(
                        self.csv_format(row=row, delimiter=dialect.delimiter)
                    )
                elif len(row) == len(headers):
                    valid_lines.append(
                        self.csv_format(row=row, delimiter=dialect.delimiter)
                    )
                else:
                    logger.warning(f"Ignored line {i}: incorrect number of columns.")
                    ignored_lines += 1  # Increment ignored lines counter

            # Prepare example data for headers
            header_data = [{**header, "data_example": None} for header in headers]

            for line in valid_lines[1:]:  # start from the second line
                parts = list(csv.reader([line], delimiter=dialect.delimiter))[0]
                for header, part in zip(header_data, parts):
                    part = part.strip() if part else None
                    if part and header["data_example"] is None:
                        header["data_example"] = part
                        if all(h["data_example"] is not None for h in header_data):
                            break  # if all headers have a data_example, break the loop

                if all(h["data_example"] is not None for h in header_data):
                    break  # if all headers have a data_example, break the loop

        except Exception:
            logger.error("File import error", exc_info=True)
            form.add_error(None, _("Unsupported file format"))
            return super().form_invalid(form)

        # Save csv
        self.save_csv_file(original_filename, encoding, valid_lines)
        # Save Session data
        self.request.session["encoding"] = encoding
        self.request.session["delimiter"] = dialect.delimiter
        self.request.session["quotechar"] = dialect.quotechar
        self.request.session["headers"] = headers
        self.request.session["header_data"] = header_data

        # Add a warning message with the number of ignored lines if needed
        if ignored_lines > 0:
            messages.warning(
                self.request,
                f"Ignored {ignored_lines} lines due to incorrect number of columns.",
            )
        messages.success(
            self.request, f"Found {len(header_data)} columns in this file."
        )

        return redirect(self.get_success_url())

    def form_invalid(self, form):
        logger.warning("Import File form is invalid.")
        return super().form_invalid(form)


class FileHeaderEditView(PermissionRequiredMixin, FormView):
    permission_required = "artefacts.add_existingcollection"
    template_name = "artefacts/existingcollection_edit_file_headers.html"
    form_class = FileHeaderForm
    success_url = reverse_lazy("artefacts:map-existing-collection")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        header_data = self.request.session.get("header_data", {})
        kwargs.update({"header_data": header_data})
        return kwargs

    def form_valid(self, form):
        header_data = self.request.session.get("header_data", {})
        headers = self.request.session.get("headers", [])

        # update headers with the new values from form
        for i, header in enumerate(header_data):
            field_name = f"header_{i}"
            new_header_name = form.cleaned_data.get(field_name)
            header["name"] = new_header_name
            headers[i]["name"] = new_header_name

        self.request.session["header_data"] = header_data
        self.request.session["headers"] = headers

        # Rewrite CSV file with new headers
        csv_file_path = self.request.session.get("csv_file_path", "")
        if csv_file_path:
            encoding = self.request.session.get("encoding", "utf-8")
            with open(csv_file_path, "r+", encoding=encoding) as f:
                lines = f.readlines()
                lines[0] = (
                    ",".join([header["name"] for header in headers]) + "\n"
                )  # replace the first line (headers)
                f.seek(0)
                f.writelines(lines)

        return super().form_valid(form)


class CsvMapperView(PermissionRequiredMixin, FormView):
    permission_required = "artefacts.add_existingcollection"
    template_name = "artefacts/existingcollection_map.html"
    form_class = ExistingCollectionMapForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        header_data = self.request.session.get("header_data", {})
        kwargs.update({"headers": self.request.session.get("headers", [])})
        kwargs.update({"header_data": header_data})
        return kwargs

    def form_valid(self, form):
        # Get all headers from CSV
        all_headers = self.request.session.get("headers", [])

        # Set the wanted mapping for simple field
        simple_mapping = {
            field: form.cleaned_data.get(field)
            for field in [
                "record_id",
                "inventory_number",
                "name",
                "description",
            ]
            # check header not empty
            if form.cleaned_data.get(field) is not None
            and form.cleaned_data.get(field) != ""
        }
        # Get the wanted mapping for FK field
        complex_mapping = {
            field: form.cleaned_data.get(field)
            for field in [
                "priority",
                "artefact_type",
                "hazard",
            ]
            # check header not empty
            if form.cleaned_data.get(field) is not None
            and form.cleaned_data.get(field) != ""
        }

        # Will map a simple field to an index of the csv file
        simple_mapped_index = [int(index) for index in simple_mapping.values() if index]
        complex_mapped_index = [
            int(index) for index in complex_mapping.values() if index
        ]

        # Get all indices
        all_indices = [header["index"] for header in all_headers]

        # Get unmapped indices
        unmapped_headers_index = list(
            set(all_indices) - set(simple_mapped_index + complex_mapped_index)
        )

        self.request.session["unmapped_headers_index"] = unmapped_headers_index

        # redirect depending on the kind of mapping
        if complex_mapping:
            self.request.session["simple_mapping"] = simple_mapping
            self.request.session["complex_mapping"] = complex_mapping
            return redirect("artefacts:map-complex-existing-collection")
        else:
            self.request.session["simple_mapping"] = simple_mapping
            return redirect("artefacts:add-existing-collection")


# Handle Complex ExistingCollection Mapping
class ComplexMapperView(PermissionRequiredMixin, FormView):
    permission_required = "artefacts.add_existingcollection"
    template_name = "artefacts/existingcollection_complexmapper.html"
    form_class = DynamicComplexMapForm

    # Clean white space at the end of a value
    # but keep the value if the white space is alone because it will be eventually mapped
    def clean_value(self, value):
        if isinstance(value, str):
            return re.sub(r"\s+$", "", value)
        else:
            return value

    # Return distinct values from file for a given index
    def get_distinct_values(self, indices):
        csv_file_path = self.request.session.get("csv_file_path")
        encoding = self.request.session.get("encoding")
        delimiter = self.request.session.get("delimiter")
        quotechar = self.request.session.get("quotechar")

        with open(csv_file_path, encoding=encoding) as f:
            reader = clevercsv.reader(f, delimiter=delimiter, quotechar=quotechar)
            csv_distinct_values = {index: set() for index in indices}
            # Skip the header line
            skip_header = True
            for row in reader:
                if skip_header:
                    skip_header = False
                    continue
                for index in indices:
                    if index < len(row):  # ignore rows with not enough columns
                        value = self.clean_value(row[int(index)])
                        if (
                            value is not None
                        ):  # consider empty values as distinct values
                            csv_distinct_values[index].add(value)

        return csv_distinct_values

    # Get complex mapping and distinct values
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        complex_mapping = self.request.session.get("complex_mapping", {})
        headers = self.request.session.get("headers", [])
        # give a list of index and get distinct value for each index
        distinct_values = self.get_distinct_values(
            [int(index) for index in complex_mapping.values()]
        )

        # Check if there are more than 30 distinct values for an index
        warning_message = ""
        for index, values in distinct_values.items():
            if len(values) > 30:
                # Find the corresponding header
                header_name = next(
                    (header["name"] for header in headers if header["index"] == index),
                    "Unknown",
                )
                # Append to the warning message
                warning_message += (
                    f"The header '{header_name}' has {len(values)} distinct values. "
                )

        # Add a single warning message if any cases were found
        if warning_message:
            url = reverse("artefacts:map-existing-collection")
            warning_message += _(
                f'</br>If needed, you can <a href="{url}">change your choice</a>.'
            )
            messages.warning(self.request, warning_message.strip())

        kwargs.update(
            {
                "complex_mapping": complex_mapping,
                "csv_distinct_values": distinct_values,
            }
        )
        return kwargs

    # Save the choices for every mapping
    def form_valid(self, form):
        complex_mapping_choices = {}
        complex_mapping = self.request.session.get("complex_mapping", {})

        for k, v in form.cleaned_data.items():
            if v is not None:
                field, value = k.split(
                    "_:_"
                )  # split the form field name and the CSV value
                index = complex_mapping.get(field)  # get index
                if field not in complex_mapping_choices:
                    complex_mapping_choices[field] = {
                        "index": index,
                        "choices": {},
                    }
                complex_mapping_choices[field]["choices"][value] = (
                    v._meta.app_label,
                    v.__class__.__name__,
                    v.id,
                )
        self.request.session["complex_mapping_choices"] = complex_mapping_choices
        return redirect("artefacts:add-existing-collection")


# Create view Bulk ExistingCollection
@permission_required("artefacts.add_existingcollection", raise_exception=True)
def create_existing_collection(request):
    # Load session data
    headers = request.session.get("headers")
    simple_mapping = request.session.get("simple_mapping")
    complex_mapping_choices = request.session.get("complex_mapping_choices")
    unmapped_headers = request.session.get("unmapped_headers_index")
    # Load file info
    file_path = request.session.get("csv_file_path")
    file_name = request.session.get("csv_file_name")
    delimiter = request.session.get("delimiter")
    encoding = request.session.get("encoding")

    # Create a dict to ease manipulation
    headers_dict = {header["index"]: header["name"] for header in headers}

    # Shorten value to match model field limit
    def clean_value(value):
        if value:
            if len(value) > 225:
                return value[:221] + "..."
        return value

    # Read file
    with open(file_path, encoding=encoding) as csvfile:
        reader = clevercsv.reader(csvfile, delimiter=delimiter)

        # Initialize list for new existing collections
        existing_collections = []

        # Skip header row
        next(reader)

        # Iterate over csv rows
        for i, row in enumerate(reader, start=1):
            try:
                existing_collection = ExistingCollection()
                if simple_mapping:
                    # Map simple data
                    for k, v in simple_mapping.items():
                        value_index = int(v)
                        setattr(
                            existing_collection,
                            k,
                            clean_value(row[value_index] if row[value_index] else None),
                        )

                if complex_mapping_choices:
                    # Map complex data
                    for field, mapping_info in complex_mapping_choices.items():
                        index = int(mapping_info["index"])
                        choices = mapping_info["choices"]

                        # Retrieve cell_value and strip it of any leading/trailing whitespace
                        cell_value = row[index].strip()

                        choice = choices.get(cell_value)
                        if choice:  # Check if choice is not None
                            app_name, model_name, object_id = choice
                            if app_name and model_name and object_id:
                                # Get the model class
                                model = apps.get_model(app_name, model_name)
                                # Get the object instance from the model
                                instance = model.objects.get(pk=object_id)
                                setattr(existing_collection, field, instance)

            except Exception as e:
                logger.error(f"Mapping error at line {i}: {str(e)}")

            try:
                # Add additional_info from unmapped headers
                # Additional info are stored as JSON
                additional_info = []
                for header_index in unmapped_headers:
                    value_index = int(header_index)
                    value = row[value_index] if row[value_index] else None
                    additional_info.append(
                        {"header": headers_dict[value_index], "value": value}
                    )
                # add original filename into addtional_info
                additional_info.append({"header": "Source file", "value": file_name})
                existing_collection.additional_info = additional_info
                existing_collection.user_import = request.user
                existing_collection.timestamp_import = timezone.now()

                # Append new object to list
                existing_collections.append(existing_collection)

            except Exception as e:
                # Handle exceptions: you could return an error message here
                logger.error(f"Mapping additional info error {i}: {str(e)}")
        logger.info("Mapping Done !")
        logger.info("Start the ExistingCollection bulk creation")
        # Create new objects in database
        ExistingCollection.objects.bulk_create(existing_collections)
        logger.info("ExistingCollection bulk creation done !")

    # Clear session data
    for key in [
        "csv_file_name",
        "csv_file_path",
        "delimiter",
        "encoding",
        "headers",
        "header_data",
        "simple_mapping",
        "complex_mapping_choices",
        "unmapped_headers_index",
    ]:
        if key in request.session:
            del request.session[key]

        # Redirect to desired location
        return redirect("artefacts:list-existing-collection")


class ExistingCollectionDetailView(PermissionRequiredMixin, DetailView):
    permission_required = "artefacts.view_existingcollection"
    model = ExistingCollection


class ExistingCollectionListView(PermissionRequiredMixin, ListView):
    permission_required = "artefacts.view_existingcollection"
    model = ExistingCollection


class ExistingCollectionDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = "artefacts.delete_existingcollection"
    model = ExistingCollection

    def get_success_url(self):
        return reverse("artefacts:list-existing-collection")

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                f"The existing collection <strong>{self.object.name}</strong> has been deleted."
            ),
        )
        return response
