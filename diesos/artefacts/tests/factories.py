import random
from unittest import mock

from django.core.files import File
from factory import Faker, Sequence, SubFactory
from factory.django import DjangoModelFactory

from diesos.artefacts.models import (
    Artefact,
    ArtefactImageAsset,
    ArtefactType,
    DamageAssessment,
    DamageGroup,
    ExistingCollection,
    LevelOfDamage,
    PriorityLevel,
    StabilizationMeasure,
    TypeOfDamage,
)
from diesos.treatments.tests.factories import (
    ProcessingLineFactory,
    TreatmentTypeFactory,
)
from diesos.users.tests.factories import UserFactory
from diesos.utils.tests.factories import HazardFactory


class LevelOfDamageFactory(DjangoModelFactory):
    label = Sequence(lambda n: f"Label{n}")
    level = Faker("random_int", min=1, max=4)

    class Meta:
        model = LevelOfDamage
        django_get_or_create = ["label"]


class ArtefactTypeFactory(DjangoModelFactory):
    type_name = Sequence(lambda n: f"{Faker('word')}-{n}")
    priority_order = Faker("random_int", min=0, max=100)

    class Meta:
        model = ArtefactType
        django_get_or_create = ["type_name"]
        django_get_or_create = ["priority_order"]


class ArtefactFactory(DjangoModelFactory):
    inventory_number = Faker("pystr")
    rescue_number = Faker("pystr")
    name = Faker("text", max_nb_chars=20)
    description = Faker("paragraph", nb_sentences=2, variable_nb_sentences=True)
    damage_level = SubFactory(LevelOfDamageFactory)
    artefact_type = SubFactory(ArtefactTypeFactory)

    class Meta:
        model = Artefact
        django_get_or_create = ["rescue_number"]


class ArtefactImageAssetFactory(DjangoModelFactory):
    artefact = SubFactory(ArtefactFactory)
    image = mock.MagicMock(spec=File)
    image.name = "artefact.jpg"
    description = Faker("paragraph", nb_sentences=1)
    default = Faker("pybool")

    class Meta:
        model = ArtefactImageAsset


class DamageGroupFactory(DjangoModelFactory):
    name = Sequence(lambda n: f"{Faker('word')}-{n}")

    class Meta:
        model = DamageGroup
        django_get_or_create = ["name"]


class TypeOfDamageFactory(DjangoModelFactory):
    name = Sequence(lambda n: f"{Faker('word')}-{n}")
    group = SubFactory(DamageGroupFactory)
    icon_class = Faker("word")
    description = Faker("text")

    class Meta:
        model = TypeOfDamage
        django_get_or_create = ["name"]


class DamageAssessmentFactory(DjangoModelFactory):
    artefact = SubFactory(ArtefactFactory)
    damage_type = SubFactory(TypeOfDamageFactory)
    user_add = SubFactory(UserFactory)
    timestamp_add = Faker("date_time_this_year")

    class Meta:
        model = DamageAssessment
        django_get_or_create = ["artefact", "damage_type"]


class PriorityLevelFactory(DjangoModelFactory):
    label = Sequence(lambda n: f"{Faker('word')}-{n}")
    color = "#" + "".join(random.choice("0123456789ABCDEF") for _ in range(6))
    priority_order = Faker("random_int", min=0, max=100)

    class Meta:
        model = PriorityLevel
        django_get_or_create = ["label", "color", "priority_order"]


class StabilizationMeasureFactory(DjangoModelFactory):
    artefact = SubFactory(ArtefactFactory)
    treatment_type = SubFactory(TreatmentTypeFactory)
    processing_line = SubFactory(ProcessingLineFactory)
    priority_order = Sequence(lambda n: n + 1)
    user_add = SubFactory(UserFactory)
    user_remove = SubFactory(UserFactory)

    class Meta:
        model = StabilizationMeasure
        django_get_or_create = ["artefact", "treatment_type"]


class ExistingCollectionFactory(DjangoModelFactory):
    record_id = Faker("pystr")
    inventory_number = Faker("pystr")
    name = Faker("text", max_nb_chars=20)
    description = Faker("paragraph", nb_sentences=2, variable_nb_sentences=True)
    hazard = SubFactory(HazardFactory)
    artefact_type = SubFactory(ArtefactTypeFactory)
    priority = SubFactory(PriorityLevelFactory)
    additional_info = [
        {
            "header": "Header 1",
            "value": "Value 1",
        },
        {
            "header": "Header 2",
            "value": "Value 2",
        },
    ]
    timestamp_import = Faker("date_time_this_year")
    user_import = SubFactory(UserFactory)

    class Meta:
        model = ExistingCollection
