from django.test import TestCase
from django.urls import resolve, reverse

from diesos.artefacts.tests.factories import (
    ArtefactFactory,
    ArtefactTypeFactory,
    DamageGroupFactory,
    ExistingCollectionFactory,
    PriorityLevelFactory,
    TypeOfDamageFactory,
)


class TestUrlsTestCase(TestCase):
    def test_view(self):
        artefact = ArtefactFactory.create()
        assert (
            reverse("artefacts:view", kwargs={"pk": artefact.pk})
            == f"/artefacts/{artefact.pk}"
        )
        assert resolve(f"/artefacts/{artefact.pk}").view_name == "artefacts:view"

    def test_add(self):
        assert reverse("artefacts:add") == "/artefacts/add"
        assert resolve("/artefacts/add").view_name == "artefacts:add"

    def test_list(self):
        assert reverse("artefacts:list") == "/artefacts/"
        assert resolve("/artefacts/").view_name == "artefacts:list"

    def test_scan(self):
        artefact = ArtefactFactory.create()
        assert (
            reverse("artefacts:scan", kwargs={"pk": artefact.pk})
            == f"/artefacts/{artefact.pk}/scan"
        )
        assert resolve(f"/artefacts/{artefact.pk}/scan").view_name == "artefacts:scan"

    def test_change(self):
        artefact = ArtefactFactory.create()
        assert (
            reverse("artefacts:change", kwargs={"pk": artefact.pk})
            == f"/artefacts/{artefact.pk}/change"
        )
        assert (
            resolve(f"/artefacts/{artefact.pk}/change").view_name == "artefacts:change"
        )

    def test_delete(self):
        artefact = ArtefactFactory.create()
        assert (
            reverse("artefacts:delete", kwargs={"pk": artefact.pk})
            == f"/artefacts/{artefact.pk}/delete"
        )
        assert (
            resolve(f"/artefacts/{artefact.pk}/delete").view_name == "artefacts:delete"
        )

    def test_add_type(self):
        assert reverse("artefacts:add-type") == "/artefacts/type/add"
        assert resolve("/artefacts/type/add").view_name == "artefacts:add-type"

    def test_list_type(self):
        assert reverse("artefacts:list-type") == "/artefacts/type"
        assert resolve("/artefacts/type").view_name == "artefacts:list-type"

    def test_change_type(self):
        artefact_type = ArtefactTypeFactory.create()
        assert (
            reverse("artefacts:change-type", kwargs={"pk": artefact_type.pk})
            == f"/artefacts/type/{artefact_type.pk}/change"
        )
        assert (
            resolve(f"/artefacts/type/{artefact_type.pk}/change").view_name
            == "artefacts:change-type"
        )

    def test_delete_type(self):
        artefact_type = ArtefactTypeFactory.create()
        assert (
            reverse("artefacts:delete-type", kwargs={"pk": artefact_type.pk})
            == f"/artefacts/type/{artefact_type.pk}/delete"
        )
        assert (
            resolve(f"/artefacts/type/{artefact_type.pk}/delete").view_name
            == "artefacts:delete-type"
        )

    def test_take_picture(self):
        artefact = ArtefactFactory.create()
        assert (
            reverse("artefacts:take-picture", kwargs={"pk": artefact.pk})
            == f"/artefacts/{artefact.pk}/take-picture"
        )
        assert (
            resolve(f"/artefacts/{artefact.pk}/take-picture").view_name
            == "artefacts:take-picture"
        )

    def test_list_damage_group(self):
        assert reverse("artefacts:list-damage-group") == "/artefacts/damage/group"
        assert (
            resolve("/artefacts/damage/group").view_name
            == "artefacts:list-damage-group"
        )

    def test_add_damage_group(self):
        assert reverse("artefacts:add-damage-group") == "/artefacts/damage/group/add"
        assert (
            resolve("/artefacts/damage/group/add").view_name
            == "artefacts:add-damage-group"
        )

    def test_delete_damage_group(self):
        damage_group = DamageGroupFactory.create()
        assert (
            reverse("artefacts:delete-damage-group", kwargs={"pk": damage_group.pk})
            == f"/artefacts/damage/group/{damage_group.pk}/delete"
        )
        assert (
            resolve(f"/artefacts/damage/group/{damage_group.pk}/delete").view_name
            == "artefacts:delete-damage-group"
        )

    def test_undelete_damage_group(self):
        damage_group = DamageGroupFactory.create()
        assert (
            reverse("artefacts:undelete-damage-group", kwargs={"pk": damage_group.pk})
            == f"/artefacts/damage/group/{damage_group.pk}/undelete"
        )
        assert (
            resolve(f"/artefacts/damage/group/{damage_group.pk}/undelete").view_name
            == "artefacts:undelete-damage-group"
        )

    def test_change_damage_group(self):
        damage_group = DamageGroupFactory.create()
        assert (
            reverse("artefacts:change-damage-group", kwargs={"pk": damage_group.pk})
            == f"/artefacts/damage/group/{damage_group.pk}/change"
        )
        assert (
            resolve(f"/artefacts/damage/group/{damage_group.pk}/change").view_name
            == "artefacts:change-damage-group"
        )

        def test_list_type_of_damage(self):
            assert reverse("artefacts:list-damage-type") == "/artefacts/damage/type"
            assert (
                resolve("/artefacts/damage/type").view_name
                == "artefacts:list-damage-type"
            )

        def test_add_type_of_damage(self):
            assert reverse("artefacts:add-damage-type") == "/artefacts/damage/type/add"
            assert (
                resolve("/artefacts/damage/type/add").view_name
                == "artefacts:add-damage-type"
            )

        def test_delete_type_of_damage(self):
            type_of_damage = TypeOfDamageFactory.create()
            assert (
                reverse(
                    "artefacts:delete-damage-type", kwargs={"pk": type_of_damage.pk}
                )
                == f"/artefacts/damage/type/{damage_group.pk}/delete"
            )
            assert (
                resolve(f"/artefacts/damage/type/{damage_group.pk}/delete").view_name
                == "artefacts:delete-damage-type"
            )

        def test_undelete_type_of_damage(self):
            type_of_damage = TypeOfDamageFactory.create()
            assert (
                reverse(
                    "artefacts:undelete-damage-type", kwargs={"pk": type_of_damage.pk}
                )
                == f"/artefacts/damage/type/{type_of_damage.pk}/undelete"
            )
            assert (
                resolve(
                    f"/artefacts/damage/type/{type_of_damage.pk}/undelete"
                ).view_name
                == "artefacts:undelete-damage-type"
            )

        def test_change_type_of_damage(self):
            type_of_damage = TypeOfDamageFactory.create()
            assert (
                reverse(
                    "artefacts:change-damage-type", kwargs={"pk": type_of_damage.pk}
                )
                == f"/artefacts/damage/type/{type_of_damage.pk}/change"
            )
            assert (
                resolve(f"/artefacts/damage/type/{type_of_damage.pk}/change").view_name
                == "artefacts:change-damage-type"
            )

            def test_change_damage_assessment(self):
                assert (
                    reverse("artefacts:change-damage", kwargs={"pk": 1})
                    == "/artefacts/1/damage-assessment"
                )
                assert (
                    resolve("/artefacts/1/damage-assessment").view_name
                    == "artefacts:change-damage"
                )

            def test_change_stabilization_measure(self):
                artefact = ArtefactFactory.create()
                assert (
                    reverse("change-stabilization-measure", kwargs={"pk": artefact.pk})
                    == f"/{artefact.pk}/stabilization"
                )
                assert (
                    resolve(f"/{artefact.pk}/stabilization").view_name
                    == "change-stabilization-measure"
                )

            def test_delete_treatment_type(self):
                artefact = ArtefactFactory.create()
                treatment_type_id = 1  # or another valid id
                assert (
                    reverse(
                        "delete-treatment-type",
                        kwargs={
                            "pk": artefact.pk,
                            "treatment_type_id": treatment_type_id,
                        },
                    )
                    == f"/{artefact.pk}/stabilization/delete/{treatment_type_id}"
                )
                assert (
                    resolve(
                        f"/{artefact.pk}/stabilization/delete/{treatment_type_id}"
                    ).view_name
                    == "delete-treatment-type"
                )

            def test_save_treatment_type_ordering(self):
                artefact = ArtefactFactory.create()
                assert (
                    reverse("save-treatment-type-ordering", kwargs={"pk": artefact.pk})
                    == f"/{artefact.pk}/stabilization/save-treatment-type-ordering"
                )
                assert (
                    resolve(
                        f"/{artefact.pk}/stabilization/save-treatment-type-ordering"
                    ).view_name
                    == "save-treatment-type-ordering"
                )

    def test_list_priority_level(self):
        assert reverse("artefacts:priority-level") == "/artefacts/priority"
        assert resolve("/artefacts/priority").view_name == "artefacts:priority-level"

    def test_change_priority_level(self):
        priority_level = PriorityLevelFactory.create()
        assert (
            reverse("artefacts:change-priority", kwargs={"pk": priority_level.pk})
            == f"/artefacts/priority/{priority_level.pk}/change"
        )
        assert (
            resolve(f"/artefacts/priority/{priority_level.pk}/change").view_name
            == "artefacts:change-priority"
        )

    def test_import_file(self):
        assert (
            reverse("artefacts:import-file") == "/artefacts/existing-collection/import"
        )
        assert (
            resolve("/artefacts/existing-collection/import").view_name
            == "artefacts:import-file"
        )

    def test_mapping_import_file(self):
        assert (
            reverse("artefacts:map-existing-collection")
            == "/artefacts/existing-collection/map"
        )
        assert (
            resolve("/artefacts/existing-collection/map").view_name
            == "artefacts:map-existing-collection"
        )

    def test_mapping_complex_import_file(self):
        assert (
            reverse("artefacts:map-complex-existing-collection")
            == "/artefacts/existing-collection/map/complex"
        )
        assert (
            resolve("/artefacts/existing-collection/map/complex").view_name
            == "artefacts:map-complex-existing-collection"
        )

    def test_list_existing_collection(self):
        assert (
            reverse("artefacts:list-existing-collection")
            == "/artefacts/existing-collection"
        )
        assert (
            resolve("/artefacts/existing-collection").view_name
            == "artefacts:list-existing-collection"
        )

    def test_add_existing_collection(self):
        assert (
            reverse("artefacts:add-existing-collection")
            == "/artefacts/existing-collection/add"
        )
        assert (
            resolve("/artefacts/existing-collection/add").view_name
            == "artefacts:add-existing-collection"
        )

    def test_view_existing_collection(self):
        existing_collection = ExistingCollectionFactory.create()
        assert (
            reverse(
                "artefacts:view-existing-collection",
                kwargs={"pk": existing_collection.pk},
            )
            == f"/artefacts/existing-collection/{existing_collection.pk}"
        )
        assert (
            resolve(
                f"/artefacts/existing-collection/{existing_collection.pk}"
            ).view_name
            == "artefacts:view-existing-collection"
        )

    def test_change_existing_collection(self):
        existing_collection = ExistingCollectionFactory.create()
        assert (
            reverse(
                "artefacts:delete-existing-collection",
                kwargs={"pk": existing_collection.pk},
            )
            == f"/artefacts/existing-collection/{existing_collection.pk}/delete"
        )
        assert (
            resolve(
                f"/artefacts/existing-collection/{existing_collection.pk}/delete"
            ).view_name
            == "artefacts:delete-existing-collection"
        )
