from urllib.parse import parse_qs, urlparse

from django.test import TestCase
from django.urls import reverse

from diesos.artefacts.models import ArtefactType, StabilizationMeasure
from diesos.artefacts.tests.factories import (
    ArtefactFactory,
    ArtefactImageAssetFactory,
    ArtefactTypeFactory,
    DamageAssessmentFactory,
    DamageGroupFactory,
    ExistingCollectionFactory,
    PriorityLevelFactory,
    StabilizationMeasureFactory,
)
from diesos.treatments.tests.factories import TreatmentTypeFactory
from diesos.users.tests.factories import UserFactory
from diesos.utils.tests.factories import HazardFactory


class TestArtefactModel(TestCase):
    def test_artefact_get_absolute_url(self):
        artefact = ArtefactFactory.create()
        assert artefact.get_absolute_url() == f"/artefacts/{artefact.pk}"

    def test_artefact_str(self):
        artefact = ArtefactFactory.create()
        self.assertEqual(str(artefact), f"{artefact.rescue_number} / {artefact.name}")


class TestArtefactImageAssetModel(TestCase):
    def test_artefactimageasset_str(self):
        artefactimageasset = ArtefactImageAssetFactory.create()
        self.assertEqual(
            str(artefactimageasset),
            f"{artefactimageasset.artefact} / {artefactimageasset.image}",
        )

    def test_artefactimageasset_get_absolute_url(self):
        artefactimageasset = ArtefactImageAssetFactory.create()
        self.assertEqual(
            artefactimageasset.get_absolute_url(),
            artefactimageasset.artefact.get_absolute_url(),
        )

    def test_artefactimageasset_filename(self):
        artefactimageasset = ArtefactImageAssetFactory.create()
        self.assertNotEqual(
            str(artefactimageasset.image.file).split("/")[-1], "artefact.jpg"
        )
        self.assertIn("artefactsassets/", str(artefactimageasset.image.file))

    def test_artefactimageasset_default_no_image(self):
        artefact = ArtefactFactory.create()
        self.assertEqual(artefact.default_picture, False)

    def test_artefactimageasset_default_one_image(self):
        artefact = ArtefactFactory.create()
        artefactimageasset = ArtefactImageAssetFactory.create(
            artefact=artefact, default=True
        )
        self.assertEqual(artefact.default_picture, artefactimageasset.image)

    def test_artefactimageasset_default_two_images(self):
        artefact = ArtefactFactory.create()
        artefactimageasset = ArtefactImageAssetFactory.create(
            artefact=artefact, default=True
        )
        artefactimageasset_2 = ArtefactImageAssetFactory.create(
            artefact=artefact, default=False
        )
        self.assertEqual(artefact.default_picture, artefactimageasset.image)
        self.assertNotEqual(artefact.default_picture, artefactimageasset_2.image)

    def test_artefactimageasset_no_default(self):
        artefact = ArtefactFactory.create()
        artefactimageasset = ArtefactImageAssetFactory.create(
            artefact=artefact, default=False
        )
        self.assertEqual(artefact.default_picture, artefactimageasset.image)

    def test_artefactimageasset_set_default(self):
        artefact = ArtefactFactory.create()
        artefactimageasset = ArtefactImageAssetFactory.create(
            artefact=artefact, default=True
        )
        artefactimageasset_2 = ArtefactImageAssetFactory.create(
            artefact=artefact, default=False
        )
        self.assertEqual(artefactimageasset.default, True)
        self.assertEqual(artefactimageasset_2.default, False)
        artefactimageasset_2.default = True
        artefactimageasset_2.save()
        artefactimageasset.refresh_from_db()
        artefactimageasset_2.refresh_from_db()
        self.assertEqual(artefactimageasset.default, False)
        self.assertEqual(artefactimageasset_2.default, True)


class TestArtefactTypeModel(TestCase):
    def test_artefact_type_str(self):
        artefact_type = ArtefactTypeFactory.create()
        assert str(artefact_type) == artefact_type.type_name

    def test_artefact_type_ordering(self):
        type1 = ArtefactTypeFactory.create(type_name="Type 1", priority_order=1)
        type2 = ArtefactTypeFactory.create(type_name="Type 2", priority_order=2)
        type3 = ArtefactTypeFactory.create(type_name="Type 3", priority_order=3)

        types = ArtefactType.objects.all()

        assert list(types) == [type1, type2, type3]


class TestDamageGroupModel(TestCase):
    def test_damage_group_str(self):
        damage_group = DamageGroupFactory.create()
        self.assertEqual(str(damage_group), damage_group.name)


class TestDamageAssessmentModel(TestCase):
    def test_damage_assessment_str(self):
        damage_assessment = DamageAssessmentFactory.create()
        self.assertEqual(
            str(damage_assessment),
            f"{damage_assessment.artefact} / {damage_assessment.damage_type}",
        )


class TestPriorityLevel(TestCase):
    def test_priority_level_str(self):
        priority_level = DamageGroupFactory.create()
        self.assertEqual(str(priority_level), priority_level.name)


class TestStabilizationMeasureModel(TestCase):
    def test_stabilization_measure_str(self):
        stabilization_measure = StabilizationMeasureFactory.create()
        self.assertEqual(
            str(stabilization_measure),
            f"{stabilization_measure.artefact} / {stabilization_measure.treatment_type}",
        )

    def test_stabilization_measure_save_no_priority_order(self):
        artefact = ArtefactFactory.create()
        treatment_type = TreatmentTypeFactory.create()
        user_add = UserFactory.create()
        stabilization_measure = StabilizationMeasure(
            artefact=artefact,
            treatment_type=treatment_type,
            user_add=user_add,
        )
        stabilization_measure.save()
        self.assertEqual(stabilization_measure.priority_order, 1)

    def test_stabilization_measure_save_with_priority_order(self):
        artefact = ArtefactFactory.create()
        treatment_type = TreatmentTypeFactory.create()
        user_add = UserFactory.create()
        stabilization_measure = StabilizationMeasure(
            artefact=artefact,
            treatment_type=treatment_type,
            user_add=user_add,
            priority_order=3,
        )
        stabilization_measure.save()
        self.assertEqual(stabilization_measure.priority_order, 3)

    def test_stabilization_measure_ordering(self):
        artefact = ArtefactFactory.create()
        treatment_type1 = TreatmentTypeFactory.create()
        treatment_type2 = TreatmentTypeFactory.create()
        user_add = UserFactory.create()
        measure1 = StabilizationMeasureFactory.create(
            artefact=artefact,
            treatment_type=treatment_type1,
            user_add=user_add,
            priority_order=2,
        )
        measure2 = StabilizationMeasureFactory.create(
            artefact=artefact,
            treatment_type=treatment_type2,
            user_add=user_add,
            priority_order=1,
        )

        measures = StabilizationMeasure.objects.all()

        self.assertEqual(list(measures), [measure2, measure1])


class TestExistingCollectionModel(TestCase):
    def setUp(self):
        self.hazard = HazardFactory.create()
        self.artefact_type = ArtefactTypeFactory.create()
        self.priority = PriorityLevelFactory.create()
        self.user_import = UserFactory.create()
        self.collection = ExistingCollectionFactory.create(
            hazard=self.hazard,
            artefact_type=self.artefact_type,
            priority=self.priority,
            user_import=self.user_import,
            additional_info=[
                {
                    "header": "Header 1",
                    "value": "Value 1",
                },
                {
                    "header": "Header 2",
                    "value": "Value 2",
                },
            ],
        )

    def test_existing_collection_str(self):
        self.assertEqual(str(self.collection), self.collection.name)

    def test_get_info_for_popover(self):
        info = self.collection.get_info_for_popover()
        expected_info = (
            f"Inventory Number : {self.collection.inventory_number}<br>"
            f"Name : {self.collection.name}<br>"
            f"Description : {self.collection.description}<br>"
            f"Hazard: {self.collection.hazard}<br>"
            f"Artefact Type : {self.collection.artefact_type}<br>"
            f"Priority : {self.collection.priority}<br>"
            f"Import data : {self.collection.timestamp_import.date()}<br>"
            f"Header 1 : Value 1<br>"
            f"Header 2 : Value 2<br>"
        )

        self.assertEqual(info, expected_info)

    def test_get_absolute_url(self):
        expected_url = reverse(
            "artefacts:view-existing-collection", kwargs={"pk": self.collection.pk}
        )
        self.assertEqual(self.collection.get_absolute_url(), expected_url)

    def test_get_attributes_as_parameters_for_import(self):
        params = self.collection.get_attributes_as_parameters_for_import()
        parsed_params = parse_qs(urlparse("?" + params).query)

        self.assertEqual(parsed_params["record_id"][0], self.collection.record_id)
        self.assertEqual(
            parsed_params["inventory_number"][0], self.collection.inventory_number
        )
        self.assertEqual(parsed_params["name"][0], self.collection.name)
        self.assertEqual(parsed_params["description"][0], self.collection.description)
        self.assertEqual(int(parsed_params["hazard"][0]), self.collection.hazard.id)
        self.assertEqual(
            int(parsed_params["artefact_type"][0]), self.collection.artefact_type.id
        )
        self.assertEqual(int(parsed_params["priority"][0]), self.collection.priority.id)
