import os
from io import BytesIO
from unittest import mock

import clevercsv
import pytest
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import AnonymousUser, Permission
from django.contrib.messages import get_messages
from django.core.exceptions import PermissionDenied
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client, RequestFactory, TestCase
from django.urls import reverse
from PIL import Image

from diesos.artefacts.models import (
    Artefact,
    ArtefactImageAsset,
    ArtefactType,
    DamageAssessment,
    DamageGroup,
    ExistingCollection,
    PriorityLevel,
    StabilizationMeasure,
    TypeOfDamage,
)
from diesos.artefacts.tests.factories import (
    ArtefactFactory,
    ArtefactImageAssetFactory,
    ArtefactTypeFactory,
    DamageAssessmentFactory,
    DamageGroupFactory,
    ExistingCollectionFactory,
    LevelOfDamageFactory,
    PriorityLevelFactory,
    StabilizationMeasureFactory,
    TypeOfDamageFactory,
)
from diesos.artefacts.views import (
    ComplexMapperView,
    UploadCsvView,
    create_existing_collection,
)
from diesos.treatments.tests.factories import (
    ProcessingLineFactory,
    TreatmentTypeFactory,
)
from diesos.users.tests.factories import UserFactory
from diesos.utils.tests.factories import HazardFactory

pytestmark = pytest.mark.django_db


class TestArtefactListView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(Permission.objects.get(codename="view_artefact"))
        self.client.force_login(self.user)
        for i in range(10):
            ArtefactFactory.create()

    def test_authenticated_with_permissions(self):
        response = self.client.get(reverse("artefacts:list"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context["object_list"]), Artefact.objects.count())

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="view_artefact")
        )
        self.client.get(reverse("artefacts:list"))
        self.assertRaises(PermissionDenied)

    def test_anonymous_user(self):
        self.user = AnonymousUser()
        self.client.get(reverse("artefacts:list"))
        self.assertRaises(PermissionDenied)


class TestArtefactDetailView(TestCase):
    fixtures = ["hazards.json"]

    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(Permission.objects.get(codename="view_artefact"))
        self.client.force_login(self.user)
        self.artefact = ArtefactFactory.create()

    def test_authenticated_with_permissions(self):
        response = self.client.get(
            reverse("artefacts:view", kwargs={"pk": self.artefact.pk})
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["object"], self.artefact)
        self.assertTemplateUsed(response, "artefacts/artefact_detail.html")

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="view_artefact")
        )
        response = self.client.get(
            reverse("artefacts:view", kwargs={"pk": self.artefact.pk})
        )
        self.assertRaises(PermissionDenied)
        self.assertEqual(response.status_code, 403)

    def test_not_authenticated(self):
        self.user = AnonymousUser()
        response = self.client.get(
            reverse("artefacts:view", kwargs={"pk": self.artefact.pk})
        )
        self.assertRaises(PermissionDenied)
        self.assertEqual(response.status_code, 200)

    def test_no_hazard(self):
        self.artefact.hazard = None
        self.artefact.save()
        response = self.client.get(
            reverse("artefacts:view", kwargs={"pk": self.artefact.pk})
        )
        html = response.content.decode("utf8")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["object"], self.artefact)
        self.assertTemplateUsed(response, "artefacts/artefact_detail.html")
        self.assertNotIn("Display hazard icon and name", html)

    def test_hazard(self):
        self.artefact.hazard_id = 1
        self.artefact.save()
        response = self.client.get(
            reverse("artefacts:view", kwargs={"pk": self.artefact.pk})
        )
        html = response.content.decode("utf8")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["object"], self.artefact)
        self.assertTemplateUsed(response, "artefacts/artefact_detail.html")
        self.assertIn("Display hazard icon and name", html)

    def test_composed_object_icon(self):
        self.artefact.composed_object = Artefact.COMPOSE
        self.artefact.save()
        icon = "bi " + self.artefact.icon
        response = self.client.get(
            reverse("artefacts:view", kwargs={"pk": self.artefact.pk})
        )
        html = response.content.decode("utf8")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["object"], self.artefact)
        self.assertTemplateUsed(response, "artefacts/artefact_detail.html")
        self.assertIn(icon, html)

    def test_no_image(self):
        response = self.client.get(
            reverse("artefacts:view", kwargs={"pk": self.artefact.pk})
        )
        html = response.content.decode("utf8")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["object"], self.artefact)
        self.assertTemplateUsed(response, "artefacts/artefact_detail.html")
        self.assertNotIn("artefactCarousel", html)

    def test_image(self):
        artefact_image_asset = ArtefactImageAssetFactory.create(artefact=self.artefact)
        response = self.client.get(
            reverse("artefacts:view", kwargs={"pk": self.artefact.pk})
        )
        html = response.content.decode("utf8")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["object"], self.artefact)
        self.assertTemplateUsed(response, "artefacts/artefact_detail.html")
        self.assertIn("artefactCarousel", html)
        self.assertIn(artefact_image_asset.image.url, html)

    def test_display_all_children(self):
        parent = Artefact.objects.create(
            rescue_number="Parent Artefact", name="Parent Artefact"
        )
        child1 = Artefact.objects.create(
            rescue_number="Child 1", name="Child 1", parent=parent
        )
        child2 = Artefact.objects.create(
            rescue_number="Child 2", name="Child 2", parent=parent
        )
        response = self.client.get(reverse("artefacts:view", kwargs={"pk": parent.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["object"], parent)
        self.assertTemplateUsed(response, "artefacts/artefact_detail.html")
        displayed_children = response.context["object"].children.all()
        self.assertIn(child1, displayed_children)
        self.assertIn(child2, displayed_children)

    def test_no_create_part_button_with_parent(self):
        parent_artefact = Artefact.objects.create(
            rescue_number="Parent Artefact", name="Parent Artefact"
        )
        child_artefact = Artefact.objects.create(
            rescue_number="Child 1", name="Child 1", parent=parent_artefact
        )
        response = self.client.get(
            reverse("artefacts:view", kwargs={"pk": child_artefact.pk})
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["object"], child_artefact)
        self.assertTemplateUsed(response, "artefacts/artefact_detail.html")
        self.assertNotContains(response, "create part", html=True)

    def test_existing_collection_not_null(self):
        self.artefact.existing_collection = (
            ExistingCollectionFactory.create()
        )  # assuming you have a factory for ExistingCollection
        self.artefact.save()
        response = self.client.get(
            reverse("artefacts:view", kwargs={"pk": self.artefact.pk})
        )
        html = response.content.decode("utf8")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["object"], self.artefact)
        self.assertTemplateUsed(response, "artefacts/artefact_detail.html")
        self.assertIn("bi-database-up", html)
        self.assertIn(self.artefact.existing_collection.inventory_number, html)

    def test_existing_collection_null(self):
        self.artefact.existing_collection = None
        self.artefact.save()
        response = self.client.get(
            reverse("artefacts:view", kwargs={"pk": self.artefact.pk})
        )
        html = response.content.decode("utf8")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["object"], self.artefact)
        self.assertTemplateUsed(response, "artefacts/artefact_detail.html")
        self.assertNotIn("bi-database-up", html)


class TestArtefactScanView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(Permission.objects.get(codename="view_artefact"))
        self.client.force_login(self.user)
        self.artefact = ArtefactFactory()
        self.treatment_type_1 = TreatmentTypeFactory(name="traitement 1")

        self.stabilization_measure = StabilizationMeasureFactory(
            artefact=self.artefact,
            priority_order=1,
            treatment_type=self.treatment_type_1,
        )
        self.stabilization_measure_2 = StabilizationMeasureFactory(
            artefact=self.artefact,
            priority_order=2,
            treatment_type=self.treatment_type_1,
        )

        self.processing_line = ProcessingLineFactory(
            treatment_type=self.treatment_type_1, running=True
        )

    def test_get_without_unfinished_measures(self):
        self.stabilization_measure.status = StabilizationMeasure.FINISH
        self.stabilization_measure_2.status = StabilizationMeasure.FINISH
        self.stabilization_measure.save()
        self.stabilization_measure_2.save()

        response = self.client.get(
            reverse("artefacts:scan", kwargs={"pk": self.artefact.pk})
        )

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, reverse("artefacts:view", kwargs={"pk": self.artefact.pk})
        )

    def test_get_with_unfinished_measures(self):
        self.stabilization_measure.treatment_type = self.treatment_type_1
        self.stabilization_measure.save()

        session = self.client.session
        session["processing_line"] = self.processing_line.pk
        session.save()

        response = self.client.get(
            reverse("artefacts:scan", kwargs={"pk": self.artefact.pk})
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["object"], self.artefact)
        self.assertTemplateUsed(response, "artefacts/artefact_scan.html")

    def test_finish_stabilization_measures(self):
        response = self.client.get(
            reverse(
                "artefacts:finish-stabilization",
                kwargs={"pk": self.stabilization_measure.pk},
            )
        )

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, self.artefact.get_absolute_url())

        self.stabilization_measure.refresh_from_db()
        self.assertEqual(self.stabilization_measure.status, StabilizationMeasure.FINISH)

    def test_start_stabilization_measures(self):
        response = self.client.get(
            reverse(
                "artefacts:start-stabilization",
                kwargs={
                    "pk": self.stabilization_measure.pk,
                    "processing_line_id": self.processing_line.pk,
                },
            )
        )

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, self.artefact.get_absolute_url())

        self.stabilization_measure.refresh_from_db()
        self.assertEqual(
            self.stabilization_measure.status, StabilizationMeasure.IN_TREATMENT
        )
        self.assertEqual(
            self.stabilization_measure.processing_line, self.processing_line
        )


class TestArtefactCreateView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(Permission.objects.get(codename="add_artefact"))
        self.client.force_login(self.user)
        self.artefact_type = ArtefactTypeFactory.create()
        self.level_of_damage = LevelOfDamageFactory.create()

    def test_get_empty(self):
        response = self.client.get(reverse("artefacts:add"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/artefact_form.html")
        self.assertEqual(response.context["form"].initial, {})

    def test_get_initial(self):
        rescue_number = "ABCD123456"
        response = self.client.get(
            "{}?rescue_number={}".format(reverse("artefacts:add"), rescue_number)
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/artefact_form.html")
        self.assertEqual(
            response.context["form"].initial["rescue_number"], rescue_number
        )

    def test_post(self):
        rescue_number = "ABCD123456"
        response = self.client.post(
            reverse("artefacts:add"),
            data={
                "rescue_number": rescue_number,
                "name": "Test name",
                "damage_level": self.level_of_damage.pk,
                "artefact_type": self.artefact_type.pk,
            },
        )
        new_artefact = Artefact.objects.first()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url, reverse("artefacts:view", kwargs={"pk": new_artefact.pk})
        )
        self.assertEqual(Artefact.objects.count(), 1)
        self.assertEqual(new_artefact.rescue_number, rescue_number)
        self.assertEqual(new_artefact.name, "Test name")

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="view_artefact")
        )
        rescue_number = "ABCD123456"
        self.client.post(
            reverse("artefacts:add"),
            data={
                "rescue_number": rescue_number,
                "name": "Test",
            },
        )
        self.assertRaises(PermissionDenied)

    def test_not_authenticated(self):
        self.user = AnonymousUser()
        rescue_number = "ABCD123456"
        self.client.post(
            reverse("artefacts:add"),
            data={
                "rescue_number": rescue_number,
                "name": "Test",
            },
        )
        self.assertRaises(PermissionDenied)

    def test_create_child_without_compose_option(self):
        parent = Artefact.objects.create(
            rescue_number="Parent", name="Parent", composed_object=Artefact.COMPOSE
        )

        response = self.client.post(
            reverse("artefacts:add") + "?parent_id=" + str(parent.id),
            data={
                "rescue_number": "Child 1",
                "name": "Child 1",
            },
        )
        self.assertEqual(response.status_code, 302)
        child_1 = Artefact.objects.last()

        child_1.refresh_from_db()
        self.assertEqual(parent.composed_object, Artefact.COMPOSE)
        self.assertEqual(child_1.composed_object, Artefact.COMPOSE)

    def test_update_composed_object_after_create(self):
        parent = Artefact.objects.create(
            rescue_number="Parent", name="Parent", composed_object=""
        )

        child_1 = Artefact.objects.create(
            parent=parent, rescue_number="Child 1", name="Child 1", composed_object=""
        )

        response = self.client.post(
            reverse("artefacts:add")
            + "?parent_id="
            + str(parent.id)
            + "&option="
            + Artefact.COMPOSE,
            data={
                "rescue_number": "Child 2",
                "name": "Child 2",
            },
        )
        self.assertEqual(response.status_code, 302)
        child_2 = Artefact.objects.last()

        parent.refresh_from_db()
        child_1.refresh_from_db()
        self.assertEqual(child_2.composed_object, Artefact.COMPOSE)
        self.assertEqual(parent.composed_object, Artefact.COMPOSE)
        self.assertEqual(child_1.composed_object, Artefact.COMPOSE)


class TestArtefactMenus(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()

    def test_menu_view_artefact_hidden(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse("home"))
        html = response.content.decode("utf8")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "pages/home.html")
        self.assertNotIn(reverse("artefacts:list"), html)

    def test_menu_view_artefact_visible(self):
        self.user.user_permissions.add(Permission.objects.get(codename="view_artefact"))
        self.client.force_login(self.user)
        response = self.client.get(reverse("home"))
        html = response.content.decode("utf8")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "pages/home.html")
        self.assertIn(reverse("artefacts:list"), html)

    def test_menu_create_artefact_visible(self):
        self.user.user_permissions.add(Permission.objects.get(codename="view_artefact"))
        self.user.user_permissions.add(Permission.objects.get(codename="add_artefact"))
        self.client.force_login(self.user)
        response = self.client.get(reverse("home"))
        html = response.content.decode("utf8")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "pages/home.html")
        self.assertIn(reverse("artefacts:add"), html)


class TestArtefactTakePicture(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(Permission.objects.get(codename="view_artefact"))
        self.client.force_login(self.user)
        self.artefact = ArtefactFactory()
        self.artefactimageasset = ArtefactImageAssetFactory(artefact=self.artefact)
        f = BytesIO()
        image = Image.new("RGB", (100, 100))
        image.save(f, "jpeg")
        f.seek(0)
        self.test_image = SimpleUploadedFile(
            "test.jpg", f.read(), content_type="image/jpeg"
        )

    def test_get(self):
        response = self.client.get(
            reverse("artefacts:take-picture", kwargs={"pk": self.artefact.pk})
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/artefactimageasset_form.html")
        self.assertEqual(response.context["artefact"], self.artefact)
        self.assertEqual(
            response.context["form"].fields["image"].widget.attrs["capture"],
            "environment",
        )
        self.assertEqual(
            response.context["form"].fields["image"].widget.attrs["accept"], "image/*"
        )
        self.assertEqual(
            response.context["form"].fields["image"].widget.attrs["hidden"], "hidden"
        )
        self.assertIn(
            '<span class="btn btn-outline-primary btn-lg"><i class="bi bi-camera me-2"></i>',
            response.context["form"].fields["image"].label,
        )

    def test_post(self):
        response = self.client.post(
            reverse("artefacts:take-picture", kwargs={"pk": self.artefact.pk}),
            data={
                "image": self.test_image,
                "description": "Test description",
                "default": True,
            },
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url,
            reverse("artefacts:view", kwargs={"pk": self.artefact.pk}),
        )
        self.assertEqual(ArtefactImageAsset.objects.count(), 2)
        self.assertEqual(ArtefactImageAsset.objects.last().artefact, self.artefact)
        self.artefactimageasset.refresh_from_db()
        self.assertEqual(self.artefactimageasset.default, False)

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="view_artefact")
        )
        self.client.post(
            reverse("artefacts:take-picture", kwargs={"pk": self.artefact.pk}),
            data={
                "image": self.test_image,
                "description": "Test description",
                "default": True,
            },
        )
        self.assertRaises(PermissionDenied)

    def test_not_authenticated(self):
        self.user = AnonymousUser()
        self.client.post(
            reverse("artefacts:take-picture", kwargs={"pk": self.artefact.pk}),
            data={
                "image": self.test_image,
                "description": "Test description",
                "default": True,
            },
        )
        self.assertRaises(PermissionDenied)


class TestArtefactTypeListView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="view_artefacttype")
        )
        self.client.force_login(self.user)
        for i in range(5):
            ArtefactTypeFactory.create()

    def test_authenticated_with_permissions(self):
        response = self.client.get(reverse("artefacts:list-type"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            len(response.context["object_list"]), ArtefactType.objects.count()
        )

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="view_artefacttype")
        )
        response = self.client.get(reverse("artefacts:list-type"))
        self.assertEqual(response.status_code, 403)

    def test_anonymous_user(self):
        self.client.logout()
        response = self.client.get(reverse("artefacts:list-type"))
        self.assertEqual(response.status_code, 302)

    def test_add_button_permissions(self):
        self.user.user_permissions.add(
            Permission.objects.get(codename="add_artefacttype")
        )
        response = self.client.get(reverse("artefacts:list-type"))
        self.assertContains(response, 'id="addButton"')

    def test_add_button_no_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="add_artefacttype")
        )
        response = self.client.get(reverse("artefacts:list-type"))
        self.assertNotContains(response, 'id="addButton"')

    def test_reorder_button_permissions(self):
        self.user.user_permissions.add(
            Permission.objects.get(codename="change_artefacttype")
        )
        response = self.client.get(reverse("artefacts:list-type"))
        self.assertContains(response, 'id="reorderModeButton"')

    def test_reorder_button_no_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="change_artefacttype")
        )
        response = self.client.get(reverse("artefacts:list-type"))
        self.assertNotContains(response, 'id="reorderModeButton"')

    def test_cancel_button_permissions(self):
        self.user.user_permissions.add(
            Permission.objects.get(codename="change_artefacttype")
        )
        response = self.client.get(reverse("artefacts:list-type"))
        self.assertContains(response, 'id="cancelButton"')

    def test_cancel_button_no_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="change_artefacttype")
        )
        response = self.client.get(reverse("artefacts:list-type"))
        self.assertNotContains(response, 'id="cancelButton"')

    def test_save_button_permissions(self):
        self.user.user_permissions.add(
            Permission.objects.get(codename="change_artefacttype")
        )
        response = self.client.get(reverse("artefacts:list-type"))
        self.assertContains(response, 'id="saveOrdering"')

    def test_save_button_no_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="change_artefacttype")
        )
        response = self.client.get(reverse("artefacts:list-type"))
        self.assertNotContains(response, 'id="saveOrdering"')


class TestArtefactTypeDetailView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="view_artefacttype")
        )
        self.client.force_login(self.user)
        self.artefact_type = ArtefactTypeFactory.create()


class TestArtefactTypeCreateView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="add_artefacttype")
        )
        self.client.force_login(self.user)

    def test_get(self):
        response = self.client.get(reverse("artefacts:add-type"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/artefacttype_form.html")

    def test_post(self):
        type_name = "Test type"
        priority_order = 1
        response = self.client.post(
            reverse("artefacts:add-type"),
            data={
                "type_name": type_name,
                "priority_order": priority_order,
            },
        )
        new_artefact_type = ArtefactType.objects.first()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url,
            reverse("artefacts:list-type"),
        )
        self.assertEqual(ArtefactType.objects.count(), 1)
        self.assertEqual(new_artefact_type.type_name, type_name)


class TestArtefactTypeUpdateView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="change_artefacttype")
        )
        self.client.force_login(self.user)
        self.artefact_type = ArtefactTypeFactory.create()

    def test_get(self):
        response = self.client.get(
            reverse("artefacts:change-type", kwargs={"pk": self.artefact_type.pk})
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/artefacttype_form.html")

    def test_post(self):
        new_type_name = "Updated type"
        response = self.client.post(
            reverse("artefacts:change-type", kwargs={"pk": self.artefact_type.pk}),
            data={
                "type_name": new_type_name,
                "priority_order": self.artefact_type.priority_order,
            },
        )
        self.artefact_type.refresh_from_db()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url,
            reverse("artefacts:list-type"),
        )
        self.assertEqual(self.artefact_type.type_name, new_type_name)

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="change_artefacttype")
        )
        response = self.client.get(
            reverse("artefacts:change-type", kwargs={"pk": self.artefact_type.pk})
        )
        self.assertEqual(response.status_code, 403)

    def test_not_authenticated(self):
        self.client.logout()
        response = self.client.get(
            reverse("artefacts:change-type", kwargs={"pk": self.artefact_type.pk})
        )
        self.assertEqual(response.status_code, 302)


class TestArtefactTypeDeleteView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="delete_artefacttype")
        )
        self.client.force_login(self.user)
        self.artefact_type = ArtefactTypeFactory.create()

    def test_get(self):
        response = self.client.get(
            reverse("artefacts:delete-type", kwargs={"pk": self.artefact_type.pk})
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/artefacttype_confirm_delete.html")

    def test_post(self):
        response = self.client.post(
            reverse("artefacts:delete-type", kwargs={"pk": self.artefact_type.pk})
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("artefacts:list-type"))
        self.assertEqual(ArtefactType.objects.count(), 0)

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="delete_artefacttype")
        )
        response = self.client.get(
            reverse("artefacts:delete-type", kwargs={"pk": self.artefact_type.pk})
        )
        self.assertEqual(response.status_code, 403)

    def test_not_authenticated(self):
        self.client.logout()
        response = self.client.get(
            reverse("artefacts:delete-type", kwargs={"pk": self.artefact_type.pk})
        )
        self.assertEqual(response.status_code, 302)


class TestArtefactDeleteView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="delete_artefact")
        )
        self.client.force_login(self.user)
        self.artefact = ArtefactFactory.create()

    def test_get(self):
        response = self.client.get(
            reverse("artefacts:delete", kwargs={"pk": self.artefact.pk})
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/artefact_confirm_delete.html")

    def test_post(self):
        response = self.client.post(
            reverse("artefacts:delete", kwargs={"pk": self.artefact.pk})
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("artefacts:list"))
        self.assertEqual(Artefact.objects.count(), 0)

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="delete_artefact")
        )
        response = self.client.get(
            reverse("artefacts:delete", kwargs={"pk": self.artefact.pk})
        )
        self.assertEqual(response.status_code, 403)

    def test_not_authenticated(self):
        self.client.logout()
        response = self.client.get(
            reverse("artefacts:delete", kwargs={"pk": self.artefact.pk})
        )
        self.assertEqual(response.status_code, 302)


class TestArtefactUpdateView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="change_artefact")
        )
        self.client.force_login(self.user)
        self.artefact = ArtefactFactory.create()

    def test_get(self):
        response = self.client.get(
            reverse("artefacts:change", kwargs={"pk": self.artefact.pk})
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/artefact_form.html")

    def test_post(self):
        new_name = "Updated name"
        response = self.client.post(
            reverse("artefacts:change", kwargs={"pk": self.artefact.pk}),
            data={
                "rescue_number": self.artefact.rescue_number,
                "name": new_name,
                "artefact_type": self.artefact.artefact_type.pk,
                "inventory_number": self.artefact.inventory_number,
                "damage_level": self.artefact.damage_level.pk,
                "description": self.artefact.description,
            },
        )
        self.artefact.refresh_from_db()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url,
            reverse("artefacts:view", kwargs={"pk": self.artefact.pk}),
        )
        self.assertEqual(self.artefact.name, new_name)

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="change_artefact")
        )
        response = self.client.get(
            reverse("artefacts:change", kwargs={"pk": self.artefact.pk})
        )
        self.assertEqual(response.status_code, 403)

    def test_not_authenticated(self):
        self.client.logout()
        response = self.client.get(
            reverse("artefacts:change", kwargs={"pk": self.artefact.pk})
        )
        self.assertEqual(response.status_code, 302)


class SaveNewOrderingTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="change_artefacttype")
        )
        self.client.force_login(self.user)

        # Create 3 instances of ArtefactType with different priorities
        self.type1 = ArtefactType.objects.create(type_name="Type 1", priority_order=3)
        self.type2 = ArtefactType.objects.create(type_name="Type 2", priority_order=1)
        self.type3 = ArtefactType.objects.create(type_name="Type 3", priority_order=2)

        self.url = reverse("artefacts:save-type-ordering")

    def test_save_new_ordering(self):
        # Prepare data to be sent with the POST request
        new_order = f"{self.type2.id},{self.type3.id},{self.type1.id}"
        data = {"ordering": new_order}

        # Send the POST request to the save_new_ordering view
        response = self.client.post(self.url, data)

        # Check if the view has returned a redirect to 'artefacts:list-type'
        self.assertRedirects(
            response, reverse("artefacts:list-type"), fetch_redirect_response=False
        )

        # Retrieve the updated instances of ArtefactType
        updated_type1 = ArtefactType.objects.get(pk=self.type1.id)
        updated_type2 = ArtefactType.objects.get(pk=self.type2.id)
        updated_type3 = ArtefactType.objects.get(pk=self.type3.id)

        # Check if the priorities have been updated correctly
        self.assertEqual(updated_type1.priority_order, 3)
        self.assertEqual(updated_type2.priority_order, 1)
        self.assertEqual(updated_type3.priority_order, 2)

    def test_success_message_on_valid_form(self):
        # Prepare data to be sent with the POST request
        new_order = f"{self.type2.id},{self.type3.id},{self.type1.id}"
        data = {"ordering": new_order}

        # Send the POST request to the save_new_ordering view
        response = self.client.post(self.url, data, follow=True)

        # Check if the success message has been added
        messages_list = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages_list), 1)
        self.assertEqual(messages_list[0].level, messages.SUCCESS)


class UndeleteArtefactTypeTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="delete_artefacttype")
        )
        self.client.force_login(self.user)

        self.type = ArtefactType.objects.create(type_name="Type 1", priority_order=1)
        self.type.delete()

    def test_undelete_artefact_type(self):
        self.assertEqual(ArtefactType.objects.count(), 0)

        response = self.client.get(
            reverse("artefacts:undelete-type", args=[self.type.id])
        )

        # Check if the view has returned a redirect to 'artefacts:list-type'
        self.assertRedirects(
            response, reverse("artefacts:list-type"), fetch_redirect_response=False
        )

        self.assertEqual(ArtefactType.objects.count(), 1)
        self.assertEqual(ArtefactType.objects.first().type_name, "Type 1")


class TestDamageGroupCreateView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="add_damagegroup")
        )
        self.client.force_login(self.user)

    def test_get(self):
        response = self.client.get(reverse("artefacts:add-damage-group"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/damagegroup_form.html")

    def test_post(self):
        data = {"name": "New Damage Group"}
        response = self.client.post(reverse("artefacts:add-damage-group"), data=data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("artefacts:list-damage-group"))

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="add_damagegroup")
        )
        response = self.client.get(reverse("artefacts:add-damage-group"))
        self.assertEqual(response.status_code, 403)

    def test_not_authenticated(self):
        self.client.logout()
        response = self.client.get(reverse("artefacts:add-damage-group"))
        self.assertEqual(response.status_code, 302)


class TestDamageGroupUpdateView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="change_damagegroup")
        )
        self.client.force_login(self.user)
        self.damage_group = DamageGroupFactory.create()

    def test_get(self):
        response = self.client.get(
            reverse(
                "artefacts:change-damage-group", kwargs={"pk": self.damage_group.pk}
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/damagegroup_form.html")

    def test_post(self):
        new_name = "Updated Damage Group"
        response = self.client.post(
            reverse(
                "artefacts:change-damage-group", kwargs={"pk": self.damage_group.pk}
            ),
            data={"name": new_name},
        )
        self.damage_group.refresh_from_db()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("artefacts:list-damage-group"))
        self.assertEqual(self.damage_group.name, new_name)

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="change_damagegroup")
        )
        response = self.client.get(
            reverse(
                "artefacts:change-damage-group", kwargs={"pk": self.damage_group.pk}
            )
        )
        self.assertEqual(response.status_code, 403)

    def test_not_authenticated(self):
        self.client.logout()
        response = self.client.get(
            reverse(
                "artefacts:change-damage-group", kwargs={"pk": self.damage_group.pk}
            )
        )
        self.assertEqual(response.status_code, 302)


class TestDamageGroupDeleteView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="delete_damagegroup")
        )
        self.client.force_login(self.user)
        self.damage_group = DamageGroupFactory.create()

    def test_get(self):
        response = self.client.get(
            reverse(
                "artefacts:delete-damage-group", kwargs={"pk": self.damage_group.pk}
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/damagegroup_confirm_delete.html")

        def test_post(self):
            response = self.client.post(
                reverse(
                    "artefacts:delete-damage-group", kwargs={"pk": self.damage_group.pk}
                )
            )
            self.assertEqual(response.status_code, 302)
            self.assertEqual(response.url, reverse("artefacts:list-damage-group"))
            self.assertEqual(DamageGroup.objects.count(), 0)

        def test_authenticated_without_permissions(self):
            self.user.user_permissions.remove(
                Permission.objects.get(codename="delete_damagegroup")
            )
            response = self.client.get(
                reverse(
                    "artefacts:delete-damage-group", kwargs={"pk": self.damage_group.pk}
                )
            )
            self.assertEqual(response.status_code, 403)

        def test_not_authenticated(self):
            self.client.logout()
            response = self.client.get(
                reverse(
                    "artefacts:delete-damage-group", kwargs={"pk": self.damage_group.pk}
                )
            )
            self.assertEqual(response.status_code, 302)


class TestDamageGroupListView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="view_damagegroup")
        )
        self.client.force_login(self.user)
        for i in range(10):
            DamageGroupFactory.create()

    def test_authenticated_with_permissions(self):
        response = self.client.get(reverse("artefacts:list-damage-group"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            len(response.context["object_list"]), DamageGroup.objects.count()
        )

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="view_damagegroup")
        )
        response = self.client.get(reverse("artefacts:list-damage-group"))
        self.assertEqual(response.status_code, 403)

    def test_anonymous_user(self):
        self.client.logout()
        response = self.client.get(reverse("artefacts:list-damage-group"))
        self.assertEqual(response.status_code, 302)


class UndeleteDamageGroupTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="delete_damagegroup")
        )
        self.client.force_login(self.user)

        self.damage_group = DamageGroupFactory()
        self.damage_group.delete()

    def test_undelete_damage_group(self):
        self.assertEqual(DamageGroup.objects.count(), 0)

        response = self.client.get(
            reverse("artefacts:undelete-damage-group", args=[self.damage_group.id])
        )

        # Check if the view has returned a redirect to 'artefacts:list-type'
        self.assertRedirects(
            response,
            reverse("artefacts:list-damage-group"),
            fetch_redirect_response=False,
        )

        self.assertEqual(DamageGroup.objects.count(), 1)
        self.assertEqual(DamageGroup.objects.first().name, self.damage_group.name)


class TypeOfDamageListViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.permission = Permission.objects.get(codename="view_typeofdamage")
        self.user.user_permissions.add(self.permission)
        self.client.force_login(self.user)
        self.type_of_damage = TypeOfDamageFactory()

    def test_authenticated_with_permissions(self):
        response = self.client.get(reverse("artefacts:list-damage-type"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/typeofdamage_list.html")

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(self.permission)
        response = self.client.get(reverse("artefacts:list-damage-type"))
        self.assertEqual(response.status_code, 403)

    def test_anonymous_user(self):
        self.client.logout()
        response = self.client.get(reverse("artefacts:list-damage-type"))
        self.assertEqual(response.status_code, 302)


class TypeOfDamageCreateView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="add_typeofdamage")
        )
        self.client.force_login(self.user)
        self.damage_group = DamageGroupFactory()

    def test_get(self):
        response = self.client.get(reverse("artefacts:add-damage-type"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/typeofdamage_form.html")

    def test_post(self):
        response = self.client.post(
            reverse("artefacts:add-damage-type"),
            data={
                "name": "Test name",
                "group": self.damage_group.pk,
                "description": "Test desc",
            },
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("artefacts:list-damage-type"))

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="add_typeofdamage")
        )
        response = self.client.get(reverse("artefacts:add-damage-type"))
        self.assertEqual(response.status_code, 403)

    def test_authenticated_with_permissions(self):
        response = self.client.get(reverse("artefacts:add-damage-type"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/typeofdamage_form.html")

    def test_not_authenticated(self):
        self.client.logout()
        response = self.client.get(reverse("artefacts:add-damage-type"))
        self.assertEqual(response.status_code, 302)


class TypeOfDamageUpdateViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.permission = Permission.objects.get(codename="change_typeofdamage")
        self.user.user_permissions.add(self.permission)
        self.client.force_login(self.user)
        self.type_of_damage = TypeOfDamageFactory()

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="change_typeofdamage")
        )
        response = self.client.get(
            reverse(
                "artefacts:change-damage-type", kwargs={"pk": self.type_of_damage.pk}
            )
        )
        self.assertEqual(response.status_code, 403)

    def test_authenticated_with_permissions(self):
        response = self.client.get(
            reverse(
                "artefacts:change-damage-type", kwargs={"pk": self.type_of_damage.pk}
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/typeofdamage_form.html")

    def test_not_authenticated(self):
        self.client.logout()
        response = self.client.get(
            reverse(
                "artefacts:change-damage-type", kwargs={"pk": self.type_of_damage.pk}
            )
        )
        self.assertEqual(response.status_code, 302)

    def test_get(self):
        response = self.client.get(
            reverse(
                "artefacts:change-damage-type", kwargs={"pk": self.type_of_damage.pk}
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/typeofdamage_form.html")

    def test_post(self):
        new_name = "Updated name"
        response = self.client.post(
            reverse(
                "artefacts:change-damage-type", kwargs={"pk": self.type_of_damage.pk}
            ),
            data={
                "name": new_name,
                "group": self.type_of_damage.group.pk,
                "description": self.type_of_damage.description,
            },
        )
        self.type_of_damage.refresh_from_db()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("artefacts:list-damage-type"))
        self.assertEqual(self.type_of_damage.name, new_name)


class TestTypeOfDamageDeleteView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="delete_typeofdamage")
        )
        self.client.force_login(self.user)
        self.type_of_damage = TypeOfDamageFactory.create()

    def test_get(self):
        response = self.client.get(
            reverse(
                "artefacts:delete-damage-type", kwargs={"pk": self.type_of_damage.pk}
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/typeofdamage_confirm_delete.html")

    def test_post(self):
        response = self.client.post(
            reverse(
                "artefacts:delete-damage-type", kwargs={"pk": self.type_of_damage.pk}
            )
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("artefacts:list-damage-type"))
        self.assertEqual(TypeOfDamage.objects.count(), 0)

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="delete_typeofdamage")
        )
        response = self.client.get(
            reverse(
                "artefacts:delete-damage-type", kwargs={"pk": self.type_of_damage.pk}
            )
        )
        self.assertEqual(response.status_code, 403)

    def test_authenticated_with_permissions(self):
        response = self.client.get(
            reverse(
                "artefacts:delete-damage-type", kwargs={"pk": self.type_of_damage.pk}
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/typeofdamage_confirm_delete.html")

    def test_not_authenticated(self):
        self.client.logout()
        response = self.client.get(
            reverse(
                "artefacts:delete-damage-type", kwargs={"pk": self.type_of_damage.pk}
            )
        )
        self.assertEqual(response.status_code, 302)


class UndeleteTypeOfDamageTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="delete_typeofdamage")
        )
        self.client.force_login(self.user)

        self.type_of_damage = TypeOfDamageFactory.create()
        self.type_of_damage.delete()

    def test_undelete_type_of_damage(self):
        self.assertEqual(TypeOfDamage.objects.count(), 0)

        response = self.client.get(
            reverse("artefacts:undelete-damage-type", args=[self.type_of_damage.id])
        )

        # Check if the view has returned a redirect to 'artefacts:list-type'
        self.assertRedirects(
            response,
            reverse("artefacts:list-damage-type"),
            fetch_redirect_response=False,
        )

        self.assertEqual(TypeOfDamage.objects.count(), 1)
        self.assertEqual(TypeOfDamage.objects.first().name, self.type_of_damage.name)


class DamageAssessmentUpdateViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.permission = Permission.objects.get(codename="change_damageassessment")
        self.user.user_permissions.add(self.permission)
        self.client.force_login(self.user)
        self.artefact = ArtefactFactory()
        self.damage_level = LevelOfDamageFactory()
        self.artefact.damage_level = self.damage_level
        self.artefact.save()

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(self.permission)
        response = self.client.get(
            reverse(
                "artefacts:change-damage",
                kwargs={"pk": self.artefact.pk},
            )
        )
        self.assertEqual(response.status_code, 403)

    def test_authenticated_with_permissions(self):
        response = self.client.get(
            reverse("artefacts:change-damage", kwargs={"pk": self.artefact.pk})
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/damageassessment_form.html")

    def test_not_authenticated(self):
        self.client.logout()
        response = self.client.get(
            reverse("artefacts:change-damage", kwargs={"pk": self.artefact.pk})
        )
        self.assertEqual(response.status_code, 302)

    def test_create_damage_assessment(self):
        self.assertEqual(
            DamageAssessment.objects.filter(artefact=self.artefact).count(), 0
        )
        new_damage_type = TypeOfDamageFactory()
        response = self.client.post(
            reverse("artefacts:change-damage", kwargs={"pk": self.artefact.pk}),
            data={
                "damage_types": [new_damage_type.pk],
                "damage_level": self.damage_level.pk,
            },
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            DamageAssessment.objects.filter(artefact=self.artefact).count(), 1
        )
        self.assertTrue(
            DamageAssessment.objects.filter(
                artefact=self.artefact, damage_type=new_damage_type
            ).exists()
        )

    def test_update_damage_assessment(self):
        old_damage_assessment = DamageAssessmentFactory(artefact=self.artefact)
        old_damage_type = old_damage_assessment.damage_type
        new_damage_type = TypeOfDamageFactory()
        response = self.client.post(
            reverse("artefacts:change-damage", kwargs={"pk": self.artefact.pk}),
            data={
                "damage_types": [new_damage_type.pk],
                "damage_level": self.damage_level.pk,
            },
        )
        self.assertEqual(response.status_code, 302)
        self.assertFalse(
            DamageAssessment.objects.filter(
                artefact=self.artefact, damage_type=old_damage_type
            ).exists()
        )
        self.assertTrue(
            DamageAssessment.objects.filter(
                artefact=self.artefact, damage_type=new_damage_type
            ).exists()
        )

    def test_delete_damage_assessment(self):
        damage_assessment = DamageAssessmentFactory(artefact=self.artefact)
        response = self.client.post(
            reverse(
                "artefacts:change-damage",
                kwargs={"pk": damage_assessment.artefact.pk},
            ),
            data={"damage_types": []},
        )
        self.assertEqual(response.status_code, 302)
        self.assertFalse(
            DamageAssessment.objects.filter(
                artefact=self.artefact, damage_type=damage_assessment.damage_type
            ).exists()
        )

    def test_update_artefact_damage_level(self):
        new_level_of_damage = LevelOfDamageFactory()
        response = self.client.post(
            reverse("artefacts:change-damage", kwargs={"pk": self.artefact.pk}),
            data={
                "damage_types": [],
                "damage_level": new_level_of_damage.pk,
            },
        )
        self.assertEqual(response.status_code, 302)
        self.artefact.refresh_from_db()
        self.assertEqual(self.artefact.damage_level, new_level_of_damage)

    def test_user_add_on_check_damage_type(self):
        new_damage_type = TypeOfDamageFactory()
        response = self.client.post(
            reverse("artefacts:change-damage", kwargs={"pk": self.artefact.pk}),
            data={
                "damage_types": [new_damage_type.pk],
                "damage_level": self.damage_level.pk,
            },
        )
        self.assertEqual(response.status_code, 302)
        self.assertTrue(
            DamageAssessment.objects.filter(
                artefact=self.artefact, damage_type=new_damage_type
            ).exists()
        )
        added_damage_assessment = DamageAssessment.objects.filter(
            artefact=self.artefact, damage_type=new_damage_type, user_add=self.user
        ).first()
        self.assertIsNotNone(added_damage_assessment)
        self.assertEqual(added_damage_assessment.user_add, self.user)

    def test_add_damage_type_with_null_damage_level(self):
        self.artefact.damage_level = None
        self.artefact.save()

        new_damage_type = TypeOfDamageFactory()
        response = self.client.post(
            reverse("artefacts:change-damage", kwargs={"pk": self.artefact.pk}),
            data={"damage_types": [new_damage_type.pk]},
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(
            DamageAssessment.objects.filter(
                artefact=self.artefact, damage_type=new_damage_type
            ).exists()
        )
        self.assertContains(
            response,
            "A level of damage is required.",
        )

    def test_user_remove_on_uncheck_damage_type(self):
        damage_assessment = DamageAssessmentFactory(artefact=self.artefact)
        old_damage_type = damage_assessment.damage_type
        new_damage_type = TypeOfDamageFactory()
        response = self.client.post(
            reverse("artefacts:change-damage", kwargs={"pk": self.artefact.pk}),
            data={
                "damage_types": [new_damage_type.pk],
                "damage_level": self.damage_level.pk,
            },
        )
        self.assertEqual(response.status_code, 302)
        self.assertFalse(
            DamageAssessment.objects.filter(
                artefact=self.artefact, damage_type=old_damage_type
            ).exists()
        )
        removed_damage_assessment = DamageAssessment.all_objects.filter(
            artefact=self.artefact, damage_type=old_damage_type, user_remove=self.user
        ).first()
        self.assertIsNotNone(removed_damage_assessment)
        self.assertEqual(removed_damage_assessment.user_remove, self.user)

    def test_reinit_user_remove(self):
        damage_assessment = DamageAssessmentFactory(artefact=self.artefact)
        damage_assessment_id = damage_assessment.id
        damage_type_id = damage_assessment.damage_type.id
        response = self.client.post(
            reverse(
                "artefacts:change-damage",
                kwargs={"pk": self.artefact.pk},
            ),
            data={"damage_types": []},
        )
        self.assertEqual(response.status_code, 302)
        self.assertFalse(
            DamageAssessment.objects.filter(
                artefact=self.artefact, damage_type_id=damage_type_id
            ).exists()
        )
        # At this point, the damage assessment is deleted but the user_remove is still set
        self.assertEqual(
            DamageAssessment.all_objects.get(pk=damage_assessment_id).user_remove,
            self.user,
        )
        response = self.client.post(
            reverse("artefacts:change-damage", kwargs={"pk": self.artefact.pk}),
            data={
                "damage_types": [damage_type_id],
                "damage_level": self.damage_level.pk,
            },
        )
        self.assertEqual(response.status_code, 302)
        self.assertTrue(
            DamageAssessment.objects.filter(
                artefact=self.artefact, damage_type_id=damage_type_id
            ).exists()
        )
        new_damage_assessment = DamageAssessment.objects.filter(
            artefact=self.artefact, damage_type_id=damage_type_id
        ).first()
        self.assertTrue(new_damage_assessment.user_remove is None)
        self.assertTrue(new_damage_assessment.timestamp_remove is None)


class TestPriorityLevelListView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="view_prioritylevel")
        )
        self.client.force_login(self.user)
        for i in range(3):
            PriorityLevelFactory.create()

    def test_authenticated_with_permissions(self):
        response = self.client.get(reverse("artefacts:priority-level"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            len(response.context["object_list"]), PriorityLevel.objects.count()
        )

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="view_prioritylevel")
        )
        response = self.client.get(reverse("artefacts:priority-level"))
        self.assertEqual(response.status_code, 403)

    def test_anonymous_user(self):
        self.client.logout()
        response = self.client.get(reverse("artefacts:priority-level"))
        self.assertEqual(response.status_code, 302)


class TestPriorityLevelUpdateView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="change_prioritylevel")
        )
        self.client.force_login(self.user)
        self.priority_level = PriorityLevelFactory.create()

    def test_get(self):
        response = self.client.get(
            reverse("artefacts:change-priority", kwargs={"pk": self.priority_level.pk})
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/prioritylevel_form.html")

    def test_post_label(self):
        new_label = "Updated label"
        response = self.client.post(
            reverse("artefacts:change-priority", kwargs={"pk": self.priority_level.pk}),
            data={
                "label": new_label,
                "color": self.priority_level.color,
                "priority_order": self.priority_level.priority_order,
            },
        )
        self.priority_level.refresh_from_db()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url,
            reverse("artefacts:priority-level"),
        )
        self.assertEqual(self.priority_level.label, new_label)

    def test_post_valid_color(self):
        new_color = "#FF03AB"
        response = self.client.post(
            reverse("artefacts:change-priority", kwargs={"pk": self.priority_level.pk}),
            data={
                "label": self.priority_level.label,
                "color": new_color,
                "priority_order": self.priority_level.priority_order,
            },
        )
        self.priority_level.refresh_from_db()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url,
            reverse("artefacts:priority-level"),
        )
        self.assertEqual(self.priority_level.color, new_color)

    def test_post_invalid_color(self):
        new_color = "#code"
        self.client.post(
            reverse("artefacts:change-priority", kwargs={"pk": self.priority_level.pk}),
            data={
                "label": self.priority_level.label,
                "color": new_color,
                "priority_order": self.priority_level.priority_order,
            },
        )
        self.priority_level.refresh_from_db()
        self.assertNotEqual(self.priority_level.color, new_color)

    def test_authenticated_without_permissions(self):
        self.user.user_permissions.remove(
            Permission.objects.get(codename="change_prioritylevel")
        )
        response = self.client.get(
            reverse("artefacts:change-priority", kwargs={"pk": self.priority_level.pk})
        )
        self.assertEqual(response.status_code, 403)

    def test_not_authenticated(self):
        self.client.logout()
        response = self.client.get(
            reverse("artefacts:change-priority", kwargs={"pk": self.priority_level.pk})
        )
        self.assertEqual(response.status_code, 302)


class TestStabilizationMeasureUpdateView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="change_stabilizationmeasure")
        )
        self.client.force_login(self.user)
        self.artefact = ArtefactFactory.create()
        self.treatment_type = TreatmentTypeFactory.create()

    def test_get(self):
        response = self.client.get(
            reverse(
                "artefacts:change-stabilization-measure",
                kwargs={"pk": self.artefact.pk},
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/stabilizationmeasure_form.html")

    def test_post(self):
        data = {"treatment_type": self.treatment_type.id}
        response = self.client.post(
            reverse(
                "artefacts:change-stabilization-measure",
                kwargs={"pk": self.artefact.pk},
            ),
            data=data,
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url,
            reverse(
                "artefacts:change-stabilization-measure",
                kwargs={"pk": self.artefact.pk},
            ),
        )
        self.assertEqual(StabilizationMeasure.objects.count(), 1)
        self.assertEqual(
            StabilizationMeasure.objects.first().treatment_type, self.treatment_type
        )


class TestSaveTreatmentTypeOrdering(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="change_stabilizationmeasure")
        )
        self.client.force_login(self.user)
        self.artefact = ArtefactFactory.create()
        self.treatment_type1 = TreatmentTypeFactory.create()
        self.treatment_type2 = TreatmentTypeFactory.create()
        self.stabilization_measure1 = StabilizationMeasure.objects.create(
            artefact=self.artefact,
            treatment_type=self.treatment_type1,
            priority_order=2,
        )
        self.stabilization_measure2 = StabilizationMeasure.objects.create(
            artefact=self.artefact,
            treatment_type=self.treatment_type2,
            priority_order=1,
        )

    def test_save_treatment_type_ordering(self):
        data = {"ordering": f"{self.treatment_type2.id},{self.treatment_type1.id}"}
        response = self.client.post(
            reverse(
                "artefacts:save-treatment-type-ordering",
                kwargs={"pk": self.artefact.pk},
            ),
            data=data,
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url,
            reverse(
                "artefacts:change-stabilization-measure",
                kwargs={"pk": self.artefact.pk},
            ),
        )

        self.stabilization_measure1.refresh_from_db()
        self.stabilization_measure2.refresh_from_db()

        self.assertEqual(self.stabilization_measure1.priority_order, 2)
        self.assertEqual(self.stabilization_measure2.priority_order, 1)


class TestDeleteTreatmentType(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="delete_stabilizationmeasure")
        )
        self.client.force_login(self.user)
        self.artefact = ArtefactFactory.create()
        self.treatment_type = TreatmentTypeFactory.create()
        self.stabilization_measure = StabilizationMeasure.objects.create(
            artefact=self.artefact, treatment_type=self.treatment_type
        )

    def test_delete_treatment_type(self):
        response = self.client.get(
            reverse(
                "artefacts:delete-treatment-type",
                kwargs={
                    "pk": self.artefact.pk,
                    "treatment_type_id": self.treatment_type.id,
                },
            )
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url,
            reverse(
                "artefacts:change-stabilization-measure",
                kwargs={"pk": self.artefact.pk},
            ),
        )
        self.assertEqual(StabilizationMeasure.objects.count(), 0)


class TestExistingCollectionListView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="view_existingcollection")
        )
        self.client.force_login(self.user)

    def test_get(self):
        response = self.client.get(reverse("artefacts:list-existing-collection"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/existingcollection_list.html")


class TestExistingCollectionDetailView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="view_existingcollection")
        )
        self.client.force_login(self.user)
        self.existing_collection = ExistingCollectionFactory()

    def test_get(self):
        response = self.client.get(
            reverse(
                "artefacts:view-existing-collection",
                kwargs={"pk": self.existing_collection.pk},
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "artefacts/existingcollection_detail.html")


class TestExistingCollectionDeleteView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="delete_existingcollection")
        )
        self.user.user_permissions.add(
            Permission.objects.get(codename="view_existingcollection")
        )
        self.client.force_login(self.user)
        self.existing_collection = ExistingCollectionFactory.create()

    def test_delete_existing_collection(self):
        response = self.client.post(
            reverse(
                "artefacts:delete-existing-collection",
                kwargs={"pk": self.existing_collection.pk},
            )
        )

        # Teste la réussite de la requête et la redirection
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse("artefacts:list-existing-collection"))

        # Teste que la collection a été correctement supprimée de la base de données
        self.assertEqual(ExistingCollection.objects.count(), 0)

        # Teste le message de succès
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertEqual(
            str(messages[0]),
            f"The existing collection <strong>{self.existing_collection.name}</strong> has been deleted.",
        )


class TestUploadCsvView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="add_existingcollection")
        )
        self.client.force_login(self.user)

        csv_path = os.path.join(
            settings.BASE_DIR,
            "diesos/artefacts/fixtures",
            "input_test_musee_plus_5lines.txt",
        )
        with open(csv_path, "rb") as csv_file:
            self.csv_file = SimpleUploadedFile("test.csv", csv_file.read())

    def test_form_valid(self):
        response = self.client.post(
            reverse("artefacts:import-file"),
            data={"file_csv": self.csv_file},
        )
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, reverse("artefacts:header-edit-existing-collection")
        )

        session = self.client.session

        # Test session data
        self.assertIn("csv_file_name", session)
        self.assertEqual(session["csv_file_name"], "test.csv")
        self.assertIn("csv_file_path", self.client.session)
        self.assertTrue(session["csv_file_path"].startswith(settings.MEDIA_ROOT))
        self.assertTrue(session["csv_file_path"].endswith(".csv"))

        # Test no message ignored line
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertEqual(session["csv_file_name"], "test.csv")
        self.assertEqual(session["encoding"], "utf-8")
        self.assertEqual(session["delimiter"], ";")
        self.assertEqual(session["quotechar"], '"')
        self.assertEqual(len(session["headers"]), 25)
        self.assertEqual(len(session["header_data"]), 25)
        # Test if first header and header_data are as expected
        self.assertEqual(session["headers"][0], {"index": 0, "name": "13"})
        self.assertEqual(
            session["header_data"][0], {"index": 0, "name": "13", "data_example": "15"}
        )

    def test_upload_wrong_extension(self):
        wrong_extension_file = SimpleUploadedFile(
            "test_file.pdf", b"Some content", content_type="application/pdf"
        )

        response = self.client.post(
            reverse("artefacts:import-file"),
            data={"file_csv": wrong_extension_file},
        )

        # Test should fail due to incorect format
        self.assertEqual(response.status_code, 200)

        form = response.context["form"]
        self.assertFalse(form.is_valid())
        self.assertIn("file_csv", form.errors)

    def test_upload_ignored_lines(self):
        csv_content = "number,name,description\n1,Watch,A vintage swiss watch from 1950\n2,painting\n"
        ignored_lines_file = SimpleUploadedFile(
            "test_file.csv", csv_content.encode(), content_type="text/csv"
        )

        response = self.client.post(
            reverse("artefacts:import-file"),
            data={"file_csv": ignored_lines_file},
        )

        # Test succes & redirect
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, reverse("artefacts:header-edit-existing-collection")
        )

        # Test message ignored  line appears
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 2)
        self.assertEqual(
            str(messages[0]),
            "Ignored 1 lines due to incorrect number of columns.",
        )

    @mock.patch("builtins.open", new_callable=mock.mock_open)
    def test_save_csv_file(self, mock_open):
        view = UploadCsvView()
        view.request = mock.Mock()
        view.request.session = {}

        # Input data
        original_filename = "test.csv"
        encoding = "utf-8"
        valid_lines = ["a;b;c", "1;2;3", "4;5;6"]

        view.save_csv_file(original_filename, encoding, valid_lines)

        # Check if the file has been written with the correct content
        mock_open.assert_called_once_with(mock.ANY, "w", encoding=encoding)
        handle = mock_open()
        handle.write.assert_called_once_with("\n".join(valid_lines))

        # Check if the session has been set with the correct keys
        self.assertIn("csv_file_name", view.request.session)
        self.assertIn("csv_file_path", view.request.session)

    def test_csv_format(self):
        view = UploadCsvView()

        # Input data
        delimiter = ";"
        row = ["a", "b", "c"]

        # Expected output
        expected_output = "a;b;c"

        # Call the method and get the result
        result = view.csv_format(delimiter, row)

        # Check if the result matches the expected output
        self.assertEqual(result, expected_output)


class TestCsvMapperView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="add_existingcollection")
        )
        self.client.force_login(self.user)
        self.csv_path = os.path.join(
            settings.BASE_DIR,
            "diesos/artefacts/fixtures",
            "input_test_musee_plus_5lines.txt",
        )
        with open(self.csv_path, "rb") as csv_file:
            self.csv_file = SimpleUploadedFile("test.csv", csv_file.read())

        session = self.client.session
        session["csv_file_name"] = "test.csv"
        session["csv_file_path"] = self.csv_path
        session["encoding"] = "utf-8"
        session["delimiter"] = ";"
        session["quotechar"] = '"'
        session["headers"] = [
            {"index": 0, "name": "record_id"},
            {"index": 1, "name": "inventory_number"},
            {"index": 2, "name": "name"},
            {"index": 3, "name": "description"},
            {"index": 4, "name": "priority"},
            {"index": 5, "name": "artefact_type"},
            {"index": 6, "name": "hazard"},
        ]
        session["header_data"] = [
            {"index": 0, "name": "record_id", "data_example": "1"},
            {"index": 1, "name": "inventory_number", "data_example": "1"},
            {"index": 2, "name": "name", "data_example": "Name 1"},
            {"index": 3, "name": "description", "data_example": "Description 1"},
            {"index": 4, "name": "priority", "data_example": "High"},
            {"index": 5, "name": "artefact_type", "data_example": "Type 1"},
            {"index": 6, "name": "hazard", "data_example": "Hazard 1"},
        ]
        session.save()

    def test_form_valid_simple_mapping(self):
        response = self.client.post(
            reverse("artefacts:map-existing-collection"),
            data={
                "record_id": "0",
                "inventory_number": "1",
                "name": "2",
                "description": "3",
                "priority": "",
                "artefact_type": "",
                "hazard": "",
            },
        )

        self.assertRedirects(
            response,
            reverse("artefacts:add-existing-collection"),
            fetch_redirect_response=False,
        )

    def test_form_valid_complex_mapping(self):
        response = self.client.post(
            reverse("artefacts:map-existing-collection"),
            data={
                "record_id": "0",
                "inventory_number": "1",
                "name": "2",
                "description": "3",
                "priority": "4",
                "artefact_type": "5",
                "hazard": "6",
            },
        )

        self.assertRedirects(
            response,
            reverse("artefacts:map-complex-existing-collection"),
            fetch_redirect_response=False,
        )


class TestComplexMapperView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="add_existingcollection")
        )

        self.priority = PriorityLevelFactory()
        self.artefact_type = ArtefactTypeFactory()
        self.hazard = HazardFactory()

        self.client.force_login(self.user)
        self.csv_path = os.path.join(
            settings.BASE_DIR,
            "diesos/artefacts/fixtures",
            "input_test_musee_plus_5lines.txt",
        )
        with open(self.csv_path, "rb") as csv_file:
            self.csv_file = SimpleUploadedFile("test.csv", csv_file.read())

        session = self.client.session
        session["csv_file_name"] = "test.csv"
        session["csv_file_path"] = self.csv_path
        session["encoding"] = "utf-8"
        session["delimiter"] = ";"
        session["quotechar"] = '"'
        session["complex_mapping"] = {
            "priority": "4",
            "artefact_type": "5",
            "hazard": "6",
        }
        session.save()

    def test_get_distinct_values(self):
        view = ComplexMapperView()
        view.request = mock.Mock()
        view.request.session = {}
        view.request.session["csv_file_path"] = self.csv_path
        view.request.session["encoding"] = "utf-8"
        view.request.session["delimiter"] = ";"
        view.request.session["quotechar"] = '"'
        distinct_values = view.get_distinct_values([4, 5, 6])

        # Check if the function correctly retrieved distinct values
        self.assertEqual(distinct_values[4], {"10002"})
        self.assertEqual(distinct_values[5], {""})
        self.assertEqual(distinct_values[6], {"", "1940"})

    def test_form_valid(self):
        request = self.factory.post(
            reverse("artefacts:map-complex-existing-collection"),
            {
                "priority_:_10002": self.priority.id,
                "artefact_type_:_": self.artefact_type.id,
                "hazard_:_1940": self.hazard.id,
            },
        )
        request.user = self.user
        request.session = self.client.session

        # Create the view and call the post() method with the request
        view = ComplexMapperView.as_view()
        response = view(request)

        self.assertRedirects(
            response,
            reverse("artefacts:add-existing-collection"),
            fetch_redirect_response=False,
        )

        # Check if the choices were correctly stored in session
        session = request.session  # Get the session from the request
        self.assertIn("complex_mapping_choices", session)
        self.assertEqual(
            session["complex_mapping_choices"]["priority"]["choices"],
            {
                "10002": (
                    self.priority._meta.app_label,
                    self.priority.__class__.__name__,
                    self.priority.id,
                )
            },
        )
        self.assertEqual(
            session["complex_mapping_choices"]["artefact_type"]["choices"],
            {
                "": (
                    self.artefact_type._meta.app_label,
                    self.artefact_type.__class__.__name__,
                    self.artefact_type.id,
                )
            },
        )
        self.assertEqual(
            session["complex_mapping_choices"]["hazard"]["choices"],
            {
                "1940": (
                    self.hazard._meta.app_label,
                    self.hazard.__class__.__name__,
                    self.hazard.id,
                )
            },
        )


class TestCreateExistingCollectionView(TestCase):
    @property
    def csv_data(self):
        with open(self.csv_path, encoding="utf-8") as csv_file:
            reader = clevercsv.reader(csv_file, delimiter=";")
            next(reader)  # skip the header
            return list(reader)  # return the remaining rows

    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="add_existingcollection")
        )

        self.priority = PriorityLevelFactory()
        self.artefact_type = ArtefactTypeFactory()
        self.hazard = HazardFactory()
        self.client.force_login(self.user)
        self.csv_path = os.path.join(
            settings.BASE_DIR,
            "diesos/artefacts/fixtures",
            "input_test_musee_plus_5lines.txt",
        )
        with open(self.csv_path, "rb") as csv_file:
            self.csv_file = SimpleUploadedFile("test.csv", csv_file.read())

        session = self.client.session
        session["csv_file_name"] = "test.csv"
        session["csv_file_path"] = self.csv_path
        session["encoding"] = "utf-8"
        session["delimiter"] = ";"
        session["quotechar"] = '"'
        session["headers"] = [
            {"index": 0, "name": "record_id"},
            {"index": 1, "name": "inventory_number"},
            {"index": 2, "name": "name"},
            {"index": 3, "name": "description"},
            {"index": 4, "name": "priority"},
            {"index": 5, "name": "artefact_type"},
            {"index": 6, "name": "hazard"},
        ]
        session["simple_mapping"] = {
            "priority": "4",
            "artefact_type": "5",
            "hazard": "6",
        }
        session["complex_mapping_choices"] = {
            "priority": {
                "index": "4",
                "choices": {
                    "10002": (
                        self.priority._meta.app_label,
                        self.priority.__class__.__name__,
                        self.priority.id,
                    )
                },
            },
            "artefact_type": {
                "index": "5",
                "choices": {
                    "": (
                        self.artefact_type._meta.app_label,
                        self.artefact_type.__class__.__name__,
                        self.artefact_type.id,
                    )
                },
            },
            "hazard": {
                "index": "6",
                "choices": {
                    "1940": (
                        self.hazard._meta.app_label,
                        self.hazard.__class__.__name__,
                        self.hazard.id,
                    )
                },
            },
        }
        session["unmapped_headers_index"] = [
            2,
            3,
        ]
        session.save()

    def test_create_existing_collection(self):
        # Submit the POST request
        request = self.factory.post(
            reverse("artefacts:add-existing-collection"),
        )
        request.user = self.user
        request.session = self.client.session  # Get the session from the client

        # Create the view and call the post() method with the request
        view = create_existing_collection
        response = view(request)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse("artefacts:list-existing-collection"))

        # Check if the objects were correctly created
        self.assertEqual(ExistingCollection.objects.count(), 4)

        # Check if the additional info was correctly stored
        for i, collection in enumerate(ExistingCollection.objects.all()):
            additional_info = collection.additional_info

            # Load the corresponding row from the CSV data
            csv_row = self.csv_data[i]

            # Here we check for both the presence of unmapped headers and their corresponding values
            for header_index, header_name in enumerate(
                ["name", "description"]
            ):  # replace with your actual unmapped headers
                expected_value = csv_row[header_index + 2]
                self.assertTrue(
                    any(
                        info["header"] == header_name
                        and info["value"] == expected_value
                        for info in additional_info
                    )
                )


class FileHeaderEditViewTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()
        self.user = UserFactory()
        self.user.user_permissions.add(
            Permission.objects.get(codename="add_existingcollection")
        )
        self.client.force_login(self.user)

        # Set up some initial session data
        self.session = self.client.session
        self.session["header_data"] = [
            {"name": "header1", "data_example": "data1", "index": 0},
            {"name": "header2", "data_example": "data2", "index": 1},
        ]
        self.session["headers"] = [
            {"name": "header1", "index": 0},
            {"name": "header2", "index": 1},
        ]
        self.session.save()

    def test_get_request(self):
        response = self.client.get(reverse("artefacts:header-edit-existing-collection"))

        # Check that the response has a status code of 200
        self.assertEqual(response.status_code, 200)

        # Check that the correct template was used
        self.assertTemplateUsed(
            response, "artefacts/existingcollection_edit_file_headers.html"
        )

    def test_post_request(self):
        form_data = {"header_0": "new_header1", "header_1": "new_header2"}
        response = self.client.post(
            reverse("artefacts:header-edit-existing-collection"), data=form_data
        )

        # Check that the response has a status code of 302 (redirect)
        self.assertEqual(response.status_code, 302)

        # Check that the user was redirected to the correct url
        self.assertRedirects(response, reverse("artefacts:map-existing-collection"))

        # Check that the header data in the session was updated correctly
        self.assertEqual(
            self.client.session["headers"],
            [{"name": "new_header1", "index": 0}, {"name": "new_header2", "index": 1}],
        )
        self.assertEqual(
            self.client.session["header_data"],
            [
                {"name": "new_header1", "data_example": "data1", "index": 0},
                {"name": "new_header2", "data_example": "data2", "index": 1},
            ],
        )
