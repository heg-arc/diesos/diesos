from django.test import TestCase

from diesos.artefacts.models import Artefact, ArtefactType, DamageGroup, TypeOfDamage

from .factories import (
    ArtefactFactory,
    ArtefactTypeFactory,
    DamageGroupFactory,
    TypeOfDamageFactory,
)


class ArtefactTypeNoDeleteTestCase(TestCase):
    def setUp(self):
        self.artefact_type = ArtefactTypeFactory()

    def test_artefact_type_soft_delete(self):
        self.artefact_type.delete()
        self.assertEqual(ArtefactType.objects.count(), 0)
        self.assertEqual(ArtefactType.objects.all_with_deleted().count(), 1)


class ArtefactTestNoDeleteCase(TestCase):
    def setUp(self):
        self.artefact_type = ArtefactTypeFactory()
        self.artefact = ArtefactFactory(artefact_type=self.artefact_type)

    def test_artefact_soft_delete(self):
        self.artefact.delete()
        self.assertEqual(Artefact.objects.count(), 0)
        self.assertEqual(Artefact.objects.all_with_deleted().count(), 1)


class DamageGroupTestNoDeleteCase(TestCase):
    def setUp(self):
        self.damage_group = DamageGroupFactory()

    def test_damage_group_soft_delete(self):
        self.damage_group.delete()
        self.assertEqual(DamageGroup.objects.count(), 0)
        self.assertEqual(DamageGroup.objects.all_with_deleted().count(), 1)


class TypeOfDamageTestNoDeleteCase(TestCase):
    def setUp(self):
        self.type_of_damage = TypeOfDamageFactory()

    def test_type_of_damage_soft_delete(self):
        self.type_of_damage.delete()
        self.assertEqual(TypeOfDamage.objects.count(), 0)
        self.assertEqual(TypeOfDamage.objects.all_with_deleted().count(), 1)
