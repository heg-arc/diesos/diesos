import re

import magic
from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from django.forms import (
    CharField,
    CheckboxSelectMultiple,
    ChoiceField,
    FileField,
    FileInput,
    Form,
    ModelChoiceField,
    ModelForm,
    ModelMultipleChoiceField,
    Select,
)
from django.utils.translation import gettext_lazy as _

from diesos.treatments.models import TreatmentType
from diesos.utils.models import Hazard

from .models import (
    Artefact,
    ArtefactType,
    DamageAssessment,
    ExistingCollection,
    PriorityLevel,
    StabilizationMeasure,
    TypeOfDamage,
)


class OrderingForm(Form):
    ordering = CharField()


class DamageAssessmentForm(ModelForm):
    damage_types = ModelMultipleChoiceField(
        queryset=TypeOfDamage.objects.all(),
        widget=CheckboxSelectMultiple,
        required=False,
    )

    class Meta:
        model = DamageAssessment
        fields = ["damage_types"]


class ArtefactLevelOfDamageForm(ModelForm):
    class Meta:
        model = Artefact
        fields = ["damage_level"]


class PriorityLevelForm(ModelForm):
    def clean_color(self):
        color = self.cleaned_data.get("color")
        # Example of possible value : #f01f3e / #f01
        if not re.match("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$", color):
            raise ValidationError(_("The hexadecimal color code is not valid"))
        return color

    class Meta:
        model = PriorityLevel
        fields = ["label", "color"]


class StabilizationMeasureForm(ModelForm):
    treatment_type = ModelChoiceField(
        queryset=TreatmentType.objects.none(),
        widget=Select(),
        required=False,
    )

    class Meta:
        model = StabilizationMeasure
        fields = ["treatment_type"]


class CsvForm(Form):
    file_csv = FileField(
        validators=[
            FileExtensionValidator(allowed_extensions=["csv", "txt"])
        ],  # check file extension
        widget=FileInput(
            attrs={"accept": ".csv, .txt"}
        ),  # restrict selection to txt and csv
    )

    # check file content
    def clean_file_csv(self):
        accepted_content = [
            "text/plain",
            "application/csv",
            "text/csv",
        ]
        file = self.cleaned_data.get("file_csv")

        if file:
            # Additional validation with python-magic
            mime = magic.from_buffer(file.read(1024), mime=True)
            print(mime)
            file.seek(0)  # reset file position to the start

            if mime not in accepted_content:
                raise ValidationError(_("Unsupported file format"))

        return file


class FileHeaderForm(Form):
    def __init__(self, *args, **kwargs):
        self.header_data = kwargs.pop("header_data", None)
        super().__init__(*args, **kwargs)

        for i, header in enumerate(self.header_data):
            field_name = f"header_{i}"
            self.fields[field_name] = CharField(
                initial=header["name"],
                label=f"Column {i + 1} - Example data: {header['data_example']}",
            )


class ExistingCollectionMapForm(Form):
    inventory_number = ChoiceField(required=False)
    record_id = ChoiceField(
        required=False, help_text=_("Select the import file technical identifier")
    )
    name = ChoiceField(required=False)
    description = ChoiceField(required=False)
    hazard = ChoiceField(required=False)
    artefact_type = ChoiceField(required=False)
    priority = ChoiceField(required=False)

    def __init__(self, *args, **kwargs):
        # Get CSV headers and their example data
        self.headers = kwargs.pop("headers", None)
        self.header_data = kwargs.pop("header_data", None)
        super().__init__(*args, **kwargs)
        # Create the different choices
        choices = [
            (
                h["index"],
                f"Column header: {h['name']} - Example data: {h['data_example']}",
            )
            for h in self.header_data
        ]
        choices.insert(0, (None, "select data from file"))

        for field_name in self.fields:
            self.fields[field_name].choices = choices

        class Meta:
            model = ExistingCollection
            fields = [
                "inventory_number",
                "record_id",
                "name",
                "description",
                "hazard",
                "priority",
                "artefact_type",
                "additional_info",
            ]


class DynamicComplexMapForm(Form):
    def __init__(self, *args, **kwargs):
        complex_mapping = kwargs.pop("complex_mapping", {})
        csv_distinct_values = kwargs.pop("csv_distinct_values", {})
        super().__init__(*args, **kwargs)

        models_map = {
            "hazard": Hazard,
            "artefact_type": ArtefactType,
            "priority": PriorityLevel,
        }
        display_mapping = {
            "hazard": "Hazard",
            "artefact_type": "Artefact type",
            "priority": "Priority level",
        }

        for field, index in complex_mapping.items():
            if field in models_map:
                model = models_map[field]
                distinct_values = csv_distinct_values.get(int(index), [])

                for value in distinct_values:
                    field_name = f"{field}_:_{value}"
                    label = f"{display_mapping.get(field, field)} : {value}"
                    self.fields[field_name] = ModelChoiceField(
                        queryset=model.objects.all(),
                        label=label,
                        required=False,
                    )
