# Generated by Django 4.0.9 on 2023-03-03 12:54

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Artefact',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('inventory_number', models.CharField(blank=True, default=None, max_length=225, unique=True, verbose_name='Inventory number')),
                ('rescue_number', models.CharField(blank=True, default=None, max_length=225, null=True, unique=True, verbose_name='Rescue number')),
                ('name', models.CharField(default=None, max_length=225, verbose_name='Object name')),
                ('description', models.TextField(blank=True, default=None, verbose_name='Description')),
            ],
        ),
    ]
