# Generated by Django 4.0.10 on 2023-05-30 11:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('artefacts', '0017_stabilizationmeasure'),
    ]

    operations = [
        migrations.AddField(
            model_name='artefact',
            name='composed_object',
            field=models.CharField(blank=True, null=True, choices=[('compose', 'composition of objects'), ('ensemble', 'set of parts')], max_length=10),
        ),
        migrations.AddField(
            model_name='artefact',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='children', to='artefacts.artefact', verbose_name='parent'),
        ),
    ]
