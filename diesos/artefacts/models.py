import os
from urllib.parse import urlencode
from uuid import uuid4

from django.db import models
from django.db.models import Max
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from safedelete.models import SafeDeleteModel

from diesos.treatments.models import ProcessingLine, TreatmentType
from diesos.users.models import User
from diesos.utils.models import Hazard


def path_and_rename(path):
    def wrapper(instance, filename):
        ext = filename.split(".")[-1]
        filename = f"{instance.artefact.rescue_number}/{uuid4().hex}.{ext}"
        return os.path.join(path, filename)

    return wrapper


# Hack to support migrations
image_asset_upload_path = path_and_rename("artefactsassets/")
image_asset_upload_path.__qualname__ = "image_asset_upload_path"


class ArtefactType(SafeDeleteModel):
    type_name = models.CharField(
        verbose_name=_("type name"), max_length=225, unique=True
    )

    description = models.TextField(verbose_name=_("description"), null=True, blank=True)

    priority_order = models.IntegerField(
        verbose_name=_("priority order"), null=True, blank=True
    )

    def __str__(self):
        return f"{self.type_name}"

    def save(self, *args, **kwargs):
        if not self.priority_order:
            max_priority = ArtefactType.objects.aggregate(Max("priority_order"))[
                "priority_order__max"
            ]
            self.priority_order = (max_priority or 0) + 1
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = _("artefact type")
        verbose_name_plural = _("artefact types")
        ordering = ("priority_order", "type_name")
        indexes = [
            models.Index(
                fields=[
                    "type_name",
                ]
            ),
            models.Index(
                fields=[
                    "priority_order",
                ]
            ),
        ]


class LevelOfDamage(models.Model):
    label = models.CharField(verbose_name=_("label"), max_length=225, unique=True)
    level = models.IntegerField(verbose_name=_("level"))

    def __str__(self):
        return f"{self.label}"


class PriorityLevel(models.Model):
    label = models.CharField(verbose_name=_("label"), max_length=50, unique=True)
    color = models.CharField(verbose_name=_("color"), max_length=7)
    priority_order = models.IntegerField(verbose_name=_("priority order"), default=1)

    def __str__(self):
        return f"{self.label}"

    class Meta:
        verbose_name = _("priority level")
        verbose_name_plural = _("priority levels")
        ordering = ["priority_order"]


class ExistingCollection(models.Model):
    record_id = models.CharField(
        verbose_name=_("record id"), max_length=225, null=True, blank=True
    )
    inventory_number = models.CharField(
        verbose_name=_("inventory number"), max_length=225, null=True, blank=True
    )
    name = models.CharField(
        verbose_name=_("designation"), max_length=225, null=True, blank=True
    )

    description = models.TextField(verbose_name=_("description"), null=True, blank=True)

    hazard = models.ForeignKey(
        Hazard,
        on_delete=models.PROTECT,
        verbose_name=_("hazard"),
        null=True,
        blank=True,
    )

    artefact_type = models.ForeignKey(
        ArtefactType,
        on_delete=models.SET_NULL,
        verbose_name=_("type"),
        null=True,
        blank=True,
    )

    priority = models.ForeignKey(
        PriorityLevel,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_("level of priority"),
    )

    additional_info = models.JSONField(
        verbose_name=_("additional information"), null=True, blank=True
    )

    timestamp_import = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_("timestamp import"),
    )

    user_import = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name=_("user import"),
    )

    # TODO Add Images

    def get_attributes_as_parameters_for_import(self):
        fields_to_exclude = ["additional_info", "timestamp_import", "user_import"]
        fields_to_serialize = [
            field.name
            for field in ExistingCollection._meta.fields
            if field.name not in fields_to_exclude
        ]
        # Change name from "id" to "existing_collection" to match Artefact field
        data = {
            field_name
            if field_name != "id"
            else "existing_collection": str(getattr(self, field_name).id)
            if isinstance(getattr(self, field_name), models.Model)
            else getattr(self, field_name)
            for field_name in fields_to_serialize
        }

        return urlencode(data)

    def get_info_for_popover(self):
        info_string = (
            f"Inventory Number : {self.inventory_number}<br>"
            f"Name : {self.name}<br>"
            f"Description : {self.description}<br>"
            f"Hazard: {self.hazard}<br>"
            f"Artefact Type : {self.artefact_type}<br>"
            f"Priority : {self.priority}<br>"
        )

        if self.timestamp_import:
            timestamp_date = self.timestamp_import.date()
            info_string += f"Import data : {timestamp_date}<br>"

        if self.additional_info:
            for info in self.additional_info:
                header = info.get("header")
                value = info.get("value")
                if header != "Unnamed" and value is not None:
                    info_string += f"{header} : {value}<br>"

        return mark_safe(info_string)

    def get_source_file(self):
        if self.additional_info:
            for info in self.additional_info:
                if info.get("header") == "Source file":
                    return info.get("value")
        return None

    def __str__(self):
        return f"{self.name}"

    def get_absolute_url(self):
        return reverse("artefacts:view-existing-collection", kwargs={"pk": self.pk})


class Artefact(SafeDeleteModel):
    COMPOSE = "compose"
    ENSEMBLE = "ensemble"

    COMPOSED_CHOICES = [
        (COMPOSE, _("composition of objects")),
        (ENSEMBLE, _("set of parts")),
    ]

    COMPOSITON_ICONS = {
        COMPOSE: "bi-box2",
        ENSEMBLE: "bi-slash-circle",
    }

    inventory_number = models.CharField(
        verbose_name=_("inventory number"), max_length=225, null=True, blank=True
    )
    rescue_number = models.CharField(
        verbose_name=_("rescue number"), max_length=225, unique=True
    )
    name = models.CharField(verbose_name=_("designation"), max_length=225)
    description = models.TextField(verbose_name=_("description"), null=True, blank=True)
    composed_object = models.CharField(
        max_length=10, choices=COMPOSED_CHOICES, null=True, blank=True
    )
    hazard = models.ForeignKey(Hazard, on_delete=models.PROTECT, null=True, blank=True)
    artefact_type = models.ForeignKey(
        ArtefactType,
        on_delete=models.SET_NULL,
        verbose_name=_("type"),
        null=True,
        blank=True,
    )
    # Linked to a artefactType Issue #11

    damage_level = models.ForeignKey(
        LevelOfDamage,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_("level of damage"),
    )
    # Linked to a DamageLevel Issue #31

    priority = models.ForeignKey(
        PriorityLevel,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_("level of priority"),
    )
    # Linked to a PriorityLevel Issue #20

    parent = models.ForeignKey(
        "self",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="children",
        verbose_name=_("parent"),
    )
    # Linked to a parent Issue #39

    existing_collection = models.ForeignKey(
        ExistingCollection,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_("existing collection"),
    )
    # Linked to a PriorityLevel Issue #37

    class Meta:
        verbose_name = _("artefact")
        verbose_name_plural = _("artefacts")
        ordering = ["rescue_number"]
        indexes = [
            models.Index(
                fields=[
                    "inventory_number",
                ]
            ),
            models.Index(
                fields=[
                    "rescue_number",
                ]
            ),
        ]

    def __str__(self):
        return f"{self.rescue_number} / {self.name}"

    def get_absolute_url(self):
        return reverse("artefacts:view", kwargs={"pk": self.pk})

    @property
    def default_picture(self):
        if self.images.filter(default=True).exists():
            return self.images.filter(default=True).first().image
        elif self.images.exists():
            return self.images.first().image
        else:
            return False

    @property
    def icon(self):
        return self.COMPOSITON_ICONS[self.composed_object]


class ArtefactImageAsset(models.Model):
    artefact = models.ForeignKey(
        Artefact, on_delete=models.CASCADE, related_name="images"
    )
    image = models.ImageField(
        verbose_name=_("image"),
        upload_to=image_asset_upload_path,
        null=True,
        blank=True,
    )
    description = models.TextField(verbose_name=_("description"), null=True, blank=True)
    default = models.BooleanField(verbose_name=_("default"), default=True)

    __original_default = None

    class Meta:
        verbose_name = _("artefact image asset")
        verbose_name_plural = _("artefact image assets")
        ordering = ["artefact", "-default"]
        indexes = [
            models.Index(
                fields=[
                    "artefact",
                ]
            ),
            models.Index(
                fields=[
                    "default",
                ]
            ),
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__original_default = self.default

    def __str__(self):
        return f"{self.artefact} / {self.image}"

    def get_absolute_url(self):
        return reverse("artefacts:view", kwargs={"pk": self.artefact.pk})

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.default:
            image_assets = self.artefact.images.exclude(id=self.id)
            for asset in image_assets:
                asset.default = False
                asset.save()


class DamageGroup(SafeDeleteModel):
    name = models.CharField(verbose_name=_("name"), max_length=225, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("damage group")
        verbose_name_plural = _("damage groups")
        ordering = ["name"]


class TypeOfDamage(SafeDeleteModel):
    name = models.CharField(
        verbose_name=_("name"),
        max_length=225,
        unique=True,
    )

    group = models.ForeignKey(
        DamageGroup,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name=_("group"),
    )

    icon_class = models.CharField(
        verbose_name=_("icon class"), max_length=255, null=True, blank=True
    )

    description = models.TextField(verbose_name=_("description"), null=True, blank=True)

    def __str__(self):
        return f"{self.name} / {self.group}"

    class Meta:
        verbose_name = _("type of damage")
        verbose_name_plural = _("types of damage")
        ordering = ["group", "name"]


class DamageAssessment(SafeDeleteModel):
    artefact = models.ForeignKey(
        Artefact,
        on_delete=models.CASCADE,
        verbose_name=_("artefact"),
        related_name="damages",
    )

    damage_type = models.ForeignKey(
        TypeOfDamage,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name=_("type of damage"),
    )

    user_add = models.ForeignKey(
        User,
        related_name="damage_assessments_removed",
        on_delete=models.SET_NULL,
        null=True,
        verbose_name=_("user add"),
    )

    user_remove = models.ForeignKey(
        User,
        related_name="damage_assessments_added",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_("user remove"),
    )

    timestamp_add = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_("timestamp add"),
    )

    timestamp_remove = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name=_("timestamp remove"),
    )

    class Meta:
        verbose_name = _("damage assessment")
        verbose_name_plural = _("damage assessments")
        ordering = ["timestamp_add"]

    def __str__(self):
        return f"{self.artefact} / {self.damage_type}"


class StabilizationMeasure(SafeDeleteModel):
    FINISH = "finish"
    IN_TREATMENT = "in_treatment"

    STATUS_TREATMENTS = [
        (FINISH, _("DONE")),
        (IN_TREATMENT, _("IN PROGRESS")),
    ]

    artefact = models.ForeignKey(
        Artefact,
        on_delete=models.CASCADE,
        verbose_name=_("artefact"),
        related_name="stabilization_measures",
    )

    treatment_type = models.ForeignKey(
        TreatmentType,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name=_("treatment type"),
    )

    priority_order = models.IntegerField(
        verbose_name=_("priority order"), null=True, blank=True
    )

    status = models.CharField(
        max_length=12, choices=STATUS_TREATMENTS, null=True, blank=True
    )

    processing_line = models.ForeignKey(
        ProcessingLine,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_("processing line"),
    )
    # Linked to a parent Issue #51

    user_add = models.ForeignKey(
        User,
        related_name="treatment_type_added",
        on_delete=models.SET_NULL,
        null=True,
        verbose_name=_("user add"),
    )

    user_remove = models.ForeignKey(
        User,
        related_name="treatment_type_removed",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_("user remove"),
    )

    timestamp_add = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_("timestamp add"),
    )

    timestamp_remove = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name=_("timestamp remove"),
    )

    class Meta:
        verbose_name = _("stabilization measure")
        verbose_name_plural = _("stabilization measures")
        ordering = ["priority_order"]

    def __str__(self):
        return f"{self.artefact} / {self.treatment_type}"

    def save(self, *args, **kwargs):
        if not self.priority_order:
            max_priority = StabilizationMeasure.objects.filter(
                artefact=self.artefact
            ).aggregate(Max("priority_order"))["priority_order__max"]
            self.priority_order = (max_priority or 0) + 1
        super().save(*args, **kwargs)
