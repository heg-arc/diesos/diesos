from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.utils.translation import gettext_lazy as _
from rangefilter.filters import DateRangeFilter

# Register your models here.
from .models import (
    Artefact,
    ArtefactImageAsset,
    ArtefactType,
    DamageAssessment,
    DamageGroup,
    LevelOfDamage,
    PriorityLevel,
    StabilizationMeasure,
    TypeOfDamage,
)


class DamageTypeListFilter(SimpleListFilter):
    title = _("damage type")
    parameter_name = "damages__damage_type"

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        damages = (
            DamageAssessment.objects.filter(artefact__in=qs)
            .select_related("damage_type")
            .distinct()
        )
        return [(d.damage_type.id, d.damage_type) for d in damages]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(damages__damage_type__id=self.value())
        else:
            return queryset


class ArtefactAdmin(admin.ModelAdmin):
    model = Artefact
    search_fields = ("inventory_number", "rescue_number", "name")
    list_filter = ("artefact_type", "damage_level", DamageTypeListFilter)


class ArtefactImageAssetAdmin(admin.ModelAdmin):
    model = ArtefactImageAsset
    search_fields = ("rescue_number",)
    list_filter = ("artefact", "default")


class ArtefactTypeAdmin(admin.ModelAdmin):
    model = ArtefactType
    search_fields = ("type_name",)


class DamageGroupAdmin(admin.ModelAdmin):
    model = DamageGroup
    search_fields = ("name",)


class TypeOfDamageAdmin(admin.ModelAdmin):
    model = TypeOfDamage
    search_fields = ("name",)
    list_filter = ("group",)


class LevelOfDamageAdmin(admin.ModelAdmin):
    model = LevelOfDamage
    search_fields = ("label",)
    list_filter = ("level",)


class ArtefactPriorityAdmin(admin.ModelAdmin):
    model = PriorityLevel
    search_fields = ("label",)


class DamageAssessmentAdmin(admin.ModelAdmin):
    model = DamageAssessment
    search_fields = ("artefact", "damage_type", "user_add", "user_remove")
    list_filter = (
        "artefact",
        "damage_type",
        ("timestamp_add", DateRangeFilter),
        ("timestamp_remove", DateRangeFilter),
        "user_add",
        "user_remove",
    )


class StabilizationMeasureAdmin(admin.ModelAdmin):
    model = StabilizationMeasure
    search_fields = ("artefact",)


admin.site.register(Artefact, ArtefactAdmin)
admin.site.register(ArtefactImageAsset, ArtefactImageAssetAdmin)
admin.site.register(ArtefactType, ArtefactTypeAdmin)
admin.site.register(DamageGroup, DamageGroupAdmin)
admin.site.register(TypeOfDamage, TypeOfDamageAdmin)
admin.site.register(LevelOfDamage, LevelOfDamageAdmin)
admin.site.register(PriorityLevel, ArtefactPriorityAdmin)
admin.site.register(DamageAssessment, DamageAssessmentAdmin)
admin.site.register(StabilizationMeasure, StabilizationMeasureAdmin)
